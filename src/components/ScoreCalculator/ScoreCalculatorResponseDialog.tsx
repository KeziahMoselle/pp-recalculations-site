import React, { useContext } from "react";
import { Dialog, DialogTitle, DialogContent, Grid, Typography, TextField, DialogActions, Button } from "@mui/material";
import { ScoreCalculationResponse } from "./ScoreCalculator";
import { roundValue } from "helpers";
import { AlgorithmContext } from "common/context";

interface IProps {
    open: boolean;
    onClose: () => void;
    score?: ScoreCalculationResponse;
    isSaved?: boolean;
};

const ScoreCalculatorResponseDialog: React.FC<IProps> = (props) => {
    const { open, onClose, score, isSaved } = props;
    const { algorithm } = useContext(AlgorithmContext);

    const hitStatsWidth = algorithm?.gamemode === 3 ? 2 : 3;

    return (
        <Dialog open={open} onClose={onClose} fullWidth maxWidth="md">
            <DialogTitle>Score calculator response</DialogTitle>
            <DialogContent>
                <Grid container item spacing={2} xs={12}>
                    <Grid item xs={12}>
                        <Typography variant="body1">Recalculated score</Typography>
                        {isSaved && <Typography variant="subtitle2" color="primary" >This score has been saved</Typography>}
                    </Grid>
                    <Grid item xs={12} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.map_name} label="Map name" fullWidth disabled />
                    </Grid>

                    <Grid item xs={12} sm={6} md={4} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.mods} label="Mods" fullWidth disabled />
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.max_combo} label="Max combo" fullWidth disabled />
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.accuracy} label="Accuracy" fullWidth disabled />
                    </Grid>

                    {algorithm?.gamemode === 3 ? (
                        <Grid item xs={12} sm={6} md={hitStatsWidth} justifyContent="flex-start">
                            <TextField variant="standard" value={score?.perfect} label="Max count" fullWidth disabled />
                        </Grid>
                    ) : null}
                    <Grid item xs={12} sm={6} md={hitStatsWidth} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.great} label="300 count" fullWidth disabled />
                    </Grid>
                    {algorithm?.gamemode === 3 ? (
                        <Grid item xs={12} sm={6} md={hitStatsWidth} justifyContent="flex-start">
                            <TextField variant="standard" value={score?.good} label="200 count" fullWidth disabled />
                        </Grid>
                    ) : null}
                    <Grid item xs={12} sm={6} md={hitStatsWidth} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.ok} label="100 count" fullWidth disabled />
                    </Grid>
                    <Grid item xs={12} sm={6} md={hitStatsWidth} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.meh} label="50 count" fullWidth disabled />
                    </Grid>
                    <Grid item xs={12} sm={6} md={hitStatsWidth} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.miss} label="Miss count" fullWidth disabled />
                    </Grid>

                    <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                        <TextField variant="standard" value={roundValue(score?.local_pp ?? 0, 2)} label="Total pp" fullWidth disabled />
                    </Grid>

                    {score?.aim_pp ? (
                        <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                            <TextField variant="standard" value={roundValue(score?.aim_pp ?? 0, 2)} label="Aim pp" fullWidth disabled />
                        </Grid>
                    ) : null}
                    {score?.tap_pp ? (
                        <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                            <TextField variant="standard" value={roundValue(score?.tap_pp ?? 0, 2)} label="Tap pp" fullWidth disabled />
                        </Grid>
                    ) : null}
                    {score?.acc_pp ? (
                        <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                            <TextField variant="standard" value={roundValue(score?.acc_pp ?? 0, 2)} label="Acc pp" fullWidth disabled />
                        </Grid>
                    ) : null}
                    {score?.fl_pp ? (
                        <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                            <TextField variant="standard" value={roundValue(score?.fl_pp ?? 0, 2)} label="FL pp" fullWidth disabled />
                        </Grid>
                    ) : null}

                    {score?.visual_pp ? (
                        <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                            <TextField variant="standard" value={roundValue(score?.visual_pp ?? 0, 2)} label="Visual pp" fullWidth disabled />
                        </Grid>
                    ) : null}

                    {score?.cognition_pp ? (
                        <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                            <TextField variant="standard" value={roundValue(score?.cognition_pp ?? 0, 2)} label="Cognition pp" fullWidth disabled />
                        </Grid>
                    ) : null}

                    {score?.reading_pp ? (
                        <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                            <TextField variant="standard" value={roundValue(score?.reading_pp ?? 0, 2)} label="Reading pp" fullWidth disabled />
                        </Grid>
                    ) : null}

                    <Grid item xs={12} sm={6} md={2} justifyContent="flex-start">
                        <TextField variant="standard" value={score?.newSR} label="Star rating" fullWidth disabled />
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button variant="text" onClick={onClose}>Close</Button>
            </DialogActions>
        </Dialog>
    );
};

export default ScoreCalculatorResponseDialog;