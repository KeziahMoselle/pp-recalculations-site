import React, { useEffect, useState } from "react";
import { Paper, Grid } from "@mui/material";
import { styled } from "@mui/material/styles";
import { useContext } from "react";
import { AlgorithmContext } from "../../common/context";
import { apiPatchRequest, apiPostRequest } from "../../helpers";
import ScoreCalculatorForm from "./ScoreCalculatorForm";
import ScoreCalculatorResponseDialog from "./ScoreCalculatorResponseDialog";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import ForbiddenError from "common/components/Errors/ForbiddenError";

interface IProps { };
export type Fields = "mapId" | "count300" | "count200" | "count100" | "count50" | "countMiss" | "combo" | "scoreId";

export interface ScoreCalculationResponse {
    map_name: string,
    accuracy: number,
    max_combo: number,
    perfect?: number,
    great: number,
    good: number,
    ok?: number,
    meh: number,
    miss: number,
    mods: string,
    local_pp: number,
    aim_pp: number,
    tap_pp: number,
    acc_pp: number,
    fl_pp: number,
    visual_pp: number,
    cognition_pp: number,
    reading_pp: number,
    newSR: number,
    isSaved?: boolean
};

const StyledPaperContent = styled("div")(({ theme }) => ({
    padding: theme.spacing(2),
    display: "flex",
    justifyContent: "flex-start"
}));

const ScoreCalculator: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);

    const [score, setScore] = useState<ScoreCalculationResponse>();
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [isScoreSaved, setIsScoreSaved] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);

    const [showForbiddenError, setShowForbiddenError] = useState<boolean>(false);

    useEffect(() => {
        if (algorithm?.code && oauth?.user?.id) {
            if (algorithm.is_private && !oauth.is_hyper_admin && !oauth.rework_permissions.some(p => p.rework_id === algorithm.id))
                setShowForbiddenError(true);
        }
    }, [algorithm, oauth]);

    const calculate = async (body: any) => {
        setLoading(true);
        const { response, status } = await apiPatchRequest<ScoreCalculationResponse>("calculate-score", { ...body, rework: algorithm?.code }, oauth.token);
        if (status === 403)
            setShowForbiddenError(true);
        else if (response?.map_name) {
            setScore(response);
            setDialogOpen(true);
            setIsScoreSaved(false);
        }
        setLoading(false);
    };

    const calculateAndSave = async (body: any) => {
        setLoading(true);
        const { response, status } = await apiPostRequest<ScoreCalculationResponse>("scores/add", body, oauth.token, algorithm?.code);
        if (status === 403)
            setShowForbiddenError(true);
        else if (response?.map_name) {
            setScore(response);
            setDialogOpen(true);
            setIsScoreSaved(response.isSaved ?? false);
        }
        setLoading(false);
    };

    const closeDialog = () => setDialogOpen(false);

    if (showForbiddenError) return <ForbiddenError />;

    return (
        <Paper>
            <StyledPaperContent>
                <Grid container>
                    <Grid container item xs={12} sm={6}>
                        <ScoreCalculatorForm calculate={calculate} calculateAndSave={calculateAndSave} loading={loading} />
                    </Grid>
                </Grid>
            </StyledPaperContent>

            <ScoreCalculatorResponseDialog open={dialogOpen} onClose={closeDialog} score={score} isSaved={isScoreSaved} />
        </Paper>
    );
};

export default ScoreCalculator;