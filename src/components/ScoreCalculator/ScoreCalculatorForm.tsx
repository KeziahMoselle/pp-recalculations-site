import React, { ChangeEvent, useContext, useState } from "react";
import { Grid, Typography, TextField, Hidden, Button, FormControl, InputLabel, Select, Input, MenuItem, Checkbox, ListItemText, Divider } from "@mui/material";
import { Fields } from "./ScoreCalculator";
import { SxStyles } from "../../helpers";
import LoadingIcon from "../../common/components/LoadingIcon/LoadingIcon";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import BetterTooltip from "common/components/BetterTooltip/BetterTooltip";
import { AlgorithmContext } from "common/context";

interface IProps {
    calculate: (body: any) => Promise<void>;
    calculateAndSave?: (body: any) => Promise<void>;
    loading: boolean;
    message?: string;
};

const sx: SxStyles = {
    formControl: {
        width: "100%",
        textAlign: "left"
    }
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
    getContentAnchorEl: () => null as unknown as Element // lol
};

const ScoreCalculatorForm: React.FC<IProps> = (props) => {
    const { calculate, calculateAndSave, loading, message } = props;
    const { oauth } = useSelector((state: IApplicationState) => state);
    const { algorithm } = useContext(AlgorithmContext);
    const [mapId, setMapId] = useState<number>();
    const [mods, setMods] = useState<string[]>([]);
    const [count300, setCount300] = useState<number>();
    const [count200, setCount200] = useState<number>();
    const [count100, setCount100] = useState<number>();
    const [count50, setCount50] = useState<number>();
    const [countMiss, setCountMiss] = useState<number>();
    const [combo, setCombo] = useState<number>();

    const [scoreId, setScoreId] = useState<number>();

    const handleNumberFieldChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, field: Fields) => {
        let value: number | undefined;
        if (!event.target.value?.length) value = undefined;
        else {
            value = +event.target.value;
            if (isNaN(value)) return;
        }

        switch (field) {
            case "mapId": setMapId(value); break;
            case "count300": setCount300(value); break;
            case "count200": setCount200(value); break;
            case "count100": setCount100(value); break;
            case "count50": setCount50(value); break;
            case "countMiss": setCountMiss(value); break;
            case "combo": setCombo(value); break;
            case "scoreId": setScoreId(value); break;
            default: console.log("wtf");
        };
    };

    const handleSelectFieldChange = (event: any, field: any) => {
        setMods(event?.target?.value as string[]);
    };

    const onClick = () => {
        const body = {
            map_id: mapId,
            mods,
            great: count300,
            good: algorithm?.gamemode === 3 ? count200 : count100,
            ok: count100,
            meh: count50,
            miss: countMiss,
            combo
        };

        if (!body.map_id || loading) return;
        calculate(body);
    };

    const onClickSave = () => {
        if (!calculateAndSave) return;
        const body = {
            score_id: scoreId
        };

        if (!body.score_id || loading) return;
        calculateAndSave(body);
    };

    const modOptions: string[] = ["EZ", "NF", "HT", "HD", "HR", "DT", "FL", "SO"];
    if (algorithm?.gamemode === 3)
        modOptions.push(...["4K", "5K", "6K", "7K", "8K", "9K", "FI", "MR"]);

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Typography align="left" variant="body1">{message ?? "Use this page to manually recalculate a score"}</Typography>
                <Typography align="left" variant="subtitle2">Leaving max combo empty will default to full combo. Leaving 100/50/miss count empty will default to 0</Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="flex-start">
                <TextField variant="standard" value={mapId} label="Map ID" fullWidth onChange={e => handleNumberFieldChange(e, "mapId")} />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="flex-start">
                <FormControl sx={sx.formControl}>
                    <InputLabel variant="standard">Mods</InputLabel>
                    <Select variant="standard" multiple value={mods} onChange={handleSelectFieldChange} input={<Input />} renderValue={s => (s as string[]).join(", ")} MenuProps={MenuProps}>
                        {modOptions.map(mod => (
                            <MenuItem key={mod} value={mod}>
                                <Checkbox color="primary" checked={mods.indexOf(mod) > -1} />
                                <ListItemText primary={mod} />
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Grid>

            {algorithm?.gamemode !== 3 && (
                <>
                    <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="flex-start">
                        <TextField variant="standard" value={combo} label="Max combo" fullWidth onChange={e => handleNumberFieldChange(e, "combo")} />
                    </Grid>
                    <Hidden mdDown>
                        <Grid item lg={3} />
                    </Hidden>
                </>
            )}

            {algorithm?.gamemode === 3 && (
                <>
                    <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="flex-start">
                        <TextField variant="standard" value={count300} label="300 count" fullWidth onChange={e => handleNumberFieldChange(e, "count300")} />
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="flex-start">
                        <TextField variant="standard" value={count200} label="200 count" fullWidth onChange={e => handleNumberFieldChange(e, "count200")} />
                    </Grid>
                </>
            )}

            <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="flex-start">
                <TextField variant="standard" value={count100} label="100 count" fullWidth onChange={e => handleNumberFieldChange(e, "count100")} />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="flex-start">
                <TextField variant="standard" value={count50} label="50 count" fullWidth onChange={e => handleNumberFieldChange(e, "count50")} />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="flex-start">
                <TextField variant="standard" value={countMiss} label="Miss count" fullWidth onChange={e => handleNumberFieldChange(e, "countMiss")} />
            </Grid>

            <Hidden mdDown>
                <Grid item lg={3} />
            </Hidden>

            <Grid container item xs={12} justifyContent="flex-start">
                <Button variant="contained" color="primary" onClick={onClick} disabled={loading}>Calculate</Button>
            </Grid>

            <Grid item xs={12}>
                <Divider />
            </Grid>

            <Grid item xs={12}>
                <Typography align="left" variant="body1">Or enter a score id to calculate and save that score instead</Typography>
            </Grid>

            <Grid item xs={12} lg={6}>
                <BetterTooltip title={!oauth?.user?.id || oauth.user.id < 0 ? ["Please login to use this feature"] : []}>
                    <TextField variant="standard" label="Score ID" value={scoreId} fullWidth onChange={e => handleNumberFieldChange(e, "scoreId")} disabled={!oauth?.token} />
                </BetterTooltip>
            </Grid>

            <Grid container item xs={12}>
                <BetterTooltip title={!oauth?.user?.id || oauth.user.id < 0 ? ["Please login to use this feature"] : []}>
                    <Button variant="contained" color="primary" onClick={onClickSave} disabled={loading || !oauth?.user?.id || oauth?.user?.id < 0 || !scoreId}>Calculate and save</Button>
                </BetterTooltip>
            </Grid>

            <Grid container item xs={12}>
                {loading && <div style={{ width: 50, height: 50 }}><LoadingIcon /></div>}
            </Grid>
        </Grid>
    );
};

export default ScoreCalculatorForm;