import React, { useContext } from "react";
import { Divider, Paper, Typography } from "@mui/material";
import Link from "../../common/components/Link/Link";
import { AlgorithmContext } from "../../common/context";
import { SxStyles } from "../../helpers";

interface IProps { };

const styles: SxStyles = {
    paper: {
        padding: 2
    },
    text: {
        color: "white"
    },
    divider: {
        mb: 2
    }
};

const RankingsInfo: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);

    const getReworkInfo = () => {
        switch (algorithm?.code) {
            case "live":
                return (
                    <>
                        <Typography align="left" variant="body1" paragraph>
                            Although not visible on the osu! website, the total pp for a score consists of four components: aim pp, tap pp, acc pp and flashlight pp. These four are the crucial values that form the total pp of a score.
                            In the past, a website run by Tom94 would allow anyone to view these values for your top scores, as well as rankings of the top players based on these values. Unfortunately it has been outdated for years and unavailable as of recently.
                            This section (requested by <Link url="https://osu.ppy.sh/u/3521482" text="Willy" />) serves as a replacement for Tom94's website.

                            Data is calculated as follows:
                            <ul>
                                <li>For players in the top 10k (roughly), the 500 best scores are retrieved and recalculated according to the currently live pp algorithm. The values for aim, tap, acc pp and flashlight pp are saved for every score and visible on your player details page.</li>
                                <li>For players outside the top 10k, the same is done for the best 100 scores.</li>
                                <li>For each player, total aim, tap, acc pp and flashlight pp is calculated using the same weighted method as for total pp.</li>
                                <li>Bonus pp is calculated. This is accurate for top 10k players, and an estimate for everyone else.</li>
                                <li>If you have multiple scores on the same map, the best one will be counted according to the currently calculated value. So if you have two scores on a map of which the first has higher aim pp, but the second has higher tap pp, then the first one will be used for total aim pp and the second one for total tap pp.</li>
                                <li>Bonus pp is not included in total aim, tap, acc or flashlight pp. It is only calculated to show it seperately.</li>
                            </ul>
                        </Typography>
                        <Typography align="left" variant="body1" paragraph>
                            To get your data calculated, you can add yourself to the queue. If you're already on the list, adding yourself to the queue will calculated any newly detected scores.
                        </Typography>
                        <Typography align="left" variant="body1" paragraph>
                            For questions, requests, bug reports or cool pictures of your cat, you can find me <Link url="https://osu.ppy.sh/u/2330619" text="here" />
                        </Typography>
                    </>
                );
            case "master":
                return (
                    <>
                        <Typography align="left" variant="body1" paragraph>
                            This section shows the recalculations of the osu! rankings on the current version of the master branch. The master branch contains all new, confirmed changes that have not deployed to live yet.   
                        </Typography>
                    </>
                );
            case "pp_2016":
                return (
                    <>
                        <Typography align="left" variant="body1" paragraph>
                            This section shows what the pp rankings would like if we were still using the original ppv2 system from 2016. Thanks to <Link url="https://osu.ppy.sh/users/2773526" text="Xexxar" /> for finding and updating the source code. The values are not 100% accurate, but should be pretty close.
                        </Typography>
                    </>
                );
            case "xexxar_skills":
                return (
                    <>
                        <Typography align="left" variant="body1" paragraph>
                            This section shows the recalculations of the osu! rankings after the pp changes set for deployment on November 8, 2021. Additional info can be found here:
                            <ul>
                                <li><Link url="https://osu.ppy.sh/home/news/2021-11-09-performance-points-star-rating-updates" text="osu! newspost" /></li>
                                <li><Link url="https://www.reddit.com/r/osugame/comments/qp8j0b/november_8_2021_ppsr_update_faq_megathread/" text="Reddit thread" /></li>
                                <li><Link url={algorithm.url} text="Github page" /></li>
                            </ul>
                        </Typography>
                    </>
                );
            default:
                return (
                    <>
                        <Typography align="left" variant="body1" paragraph>
                            This section shows the recalculations of the osu! rankings using {algorithm?.name}, which is still work in progress. You can find the GitHub page <Link url={algorithm?.url ?? ""} text="here" />
                        </Typography>
                    </>
                );
        }
    }

    return (
        <Paper sx={styles.paper}>
            <Typography sx={styles.text} variant="subtitle2" align="left" paragraph>
                {algorithm?.url && algorithm.branch && <><Link url={algorithm?.url ?? ""} text={`Branch: ${algorithm?.branch}`} /><br /></>}
                {algorithm?.commit && `Commit: ${algorithm?.commit}`}
            </Typography>

            {!["live", "master"].includes(algorithm?.code ?? "") && (
                <Typography sx={styles.text} variant="body2" align="left" color="warn">
                    Reworks usually undergo frequent updates; therefore this site will not be up to date all the time. Please check for yourself what the latest status of a rework is <span style={{ fontWeight: "bold" }}>before</span> getting angry on Twitter.<br /><br />
                </Typography>
            )}

            <Typography sx={styles.text} variant="body2" align="left">
                Please add your user id to the queue if your data is outdated, or if you're not on the list at all.<br />
                All values shown are estimates, especially your rank! For any bugs/questions, you can find me on <Link url="https://osu.ppy.sh/u/2330619" text="osu!" /> or Discord (Mr HeliX#0073)
                <br /><br />
            </Typography>

            <Divider sx={styles.divider} />

            {getReworkInfo()}
        </Paper>
    );
};

export default RankingsInfo;