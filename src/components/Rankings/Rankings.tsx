import React, { useContext } from "react";
import { styled } from "@mui/material/styles";
import ToolBar from "../../common/components/ToolBar/ToolBar";
import RankingsNavigation from "../PlayersList/RankingsNavigation";
import PlayersList from "../PlayersList/PlayersList";
import TopScores from "../TopScores/TopScores";
import Queue from "../Queue/Queue";
import { AlgorithmContext } from "../../common/context";
import LoadingIcon from "../../common/components/LoadingIcon/LoadingIcon";
import ScoreCalculator from "../ScoreCalculator/ScoreCalculator";
import { Redirect, Route, Switch } from "react-router";
import RankingsInfo from "./RankingsInfo";
import RankingsData from "./RankingsData";
import ReworkAdmin from "components/ReworkAdmin/ReworkAdmin";
import { Paper, Typography, useMediaQuery, useTheme } from "@mui/material";
import Link from "common/components/Link/Link";
import { SxStyles } from "helpers";

interface IProps { };

const sx: SxStyles = {
    banner: {
        ml: 1,
        color: "#da4949"
    }
};

const StyledRankingsContainer = styled("div")(({ theme }) => ({
    backgroundColor: theme.palette.background.default
}));

const Rankings: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);

    const isSmallScreen = useMediaQuery(useTheme().breakpoints.down("sm"));

    const getTitle = () => {
        return algorithm?.name ?? "Gaming";
    };

    return (
        <StyledRankingsContainer>
            <ToolBar shortTitle={"PP Rankings"} longTitle={`PP Rankings - ${getTitle()}`} />
            {algorithm
                ? (
                    <>
                        {algorithm?.banner_text && algorithm?.banner_text?.length > 0 && (
                            <Paper>
                                {algorithm?.code === "xexxar_skills" && (
                                    <Typography sx={sx.banner} align={isSmallScreen ? "left" : "center"} variant={isSmallScreen ? "body2" : "body1"} paragraph={isSmallScreen}>
                                        Deployment started on November 9 and will take until at least November 16. Read <Link text="the newspost" url="https://osu.ppy.sh/home/news/2021-11-09-performance-points-star-rating-updates" color="#da4949" /> for more details.
                                    </Typography>
                                )}
                                <Typography sx={sx.banner} align={"left"} variant={isSmallScreen ? "body2" : "body1"} paragraph={isSmallScreen}>
                                    {algorithm.banner_text?.split("\n").map((line, i) => <React.Fragment key={i}>{line}<br /></React.Fragment>)}
                                </Typography>
                            </Paper>
                        )}
                        <div style={{ minHeight: isSmallScreen ? "calc(100vh - 56px)" : "calc(100vh - 64px)" }}>
                            <RankingsNavigation />
                            <Switch>
                                <Route path="/rankings/info" component={RankingsInfo} />
                                <Route path="/rankings/players" component={PlayersList} />
                                <Route path="/rankings/topscores" component={TopScores} />
                                <Route path="/rankings/queue" component={Queue} />
                                <Route path="/rankings/score-calculator" component={ScoreCalculator} />
                                <Route path="/rankings/data" component={RankingsData} />
                                <Route path="/rankings/admin" component={ReworkAdmin} />
                                <Route><Redirect to="/rankings/players/live" /></Route>
                            </Switch>
                        </div>
                    </>
                )
                : <LoadingIcon />
            }
        </StyledRankingsContainer>
    );
};

export default Rankings;