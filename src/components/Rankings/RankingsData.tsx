import { Grid } from "@mui/material";
import React, { useContext, useState } from "react";
import { SxStyles } from "helpers";
import TopPlayersGraphsContainer from "./RankingsGraphs/TopPlayers/TopPlayersGraphsContainer";
import TopScoresGraphsContainer from "./RankingsGraphs/TopScores/TopScoresGraphsContainer";
import { AlgorithmContext } from "common/context";
import ForbiddenError from "common/components/Errors/ForbiddenError";

interface IProps { };

const styles: SxStyles = {
    grid: {
        mt: 0 // ???
    }
};

const RankingsData: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);
    const [showForbiddenError, setShowForbiddenError] = useState<boolean>(false);

    if (algorithm?.code === "live") return null;
    if (showForbiddenError) return <ForbiddenError />;

    return (
        <Grid container spacing={2} sx={styles.grid}>
            <TopPlayersGraphsContainer setShowForbiddenError={setShowForbiddenError} />
            <TopScoresGraphsContainer setShowForbiddenError={setShowForbiddenError} />
        </Grid>
    );
};

export default RankingsData;