import React, { useContext } from "react";
import { Typography, Card, CardContent } from "@mui/material";
import { AlgorithmContext } from "../../common/context";
import Link from "../../common/components/Link/Link";

interface IProps { };

const RankingsHelp: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);

    const getAlgorithmName = (): string => {
        return algorithm?.name ?? "";
    };

    const getAlgorithmGithub = (): string => {
        return algorithm?.url ?? "";
    };

    const getCommit = (): string | null => {
        return algorithm?.commit ?? null;
    };

    const getHelpTextLive = () => (
        <>
            <Typography align="left" variant="body1" paragraph>
                Although not visible on the osu! website, the total pp for a score consists of three important components: aim pp, tap pp and acc pp. These three are the crucial values that form the total pp of a score.
                In the past, a website run by Tom94 would allow anyone to view these values for your top scores, as well as rankings of the top players based on these values. Unfortunately it has been outdated for years and unavailable as of recently.
                This section (requested by <Link url="https://osu.ppy.sh/u/3521482" text="Willy" />) serves as a replacement for Tom94's website.

                Data is calculated as follows:
                <ul>
                    <li>For players in the top 10k (roughly), the 500 best scores are retrieved and recalculated according to the currently live pp algorithm. The values for aim, tap and acc pp are saved for every score and visible on your player details page.</li>
                    <li>For players outside the top 10k, the same is done for the best 100 scores.</li>
                    <li>For each player, total aim, tap and acc pp is calculated using the same weighted method as for total pp.</li>
                    <li>Bonus pp is calculated. This is accurate for top 10k players, and an estimate for everyone else.</li>
                    <li>If you have multiple scores on the same map, the best one will be counted according to the currently calculated value. So if you have two scores on a map of which the first has higher aim pp, but the second has higher tap pp, then the first one will be used for total aim pp and the second one for total tap pp.</li>
                    <li>Bonus pp is not included in total aim, tap or acc pp. It is only calculated to show it seperately.</li>
                </ul>
            </Typography>
            <Typography align="left" variant="body1" paragraph>
                To get your data calculated, you can add yourself to the queue. If you're already on the list, adding yourself to the queue will calculated any newly detected scores.
            </Typography>
            <Typography align="left" variant="body1" paragraph>
                For questions, requests, bug reports or cool pictures of your cat, you can find me <Link url="https://osu.ppy.sh/u/2330619" text="here" />
            </Typography>
        </>
    );

    const getHelpTextRecalc = () => {
        const name = getAlgorithmName();
        const github = getAlgorithmGithub();
        const commit = getCommit();

        return (
            <>
                <Typography align="left" variant="body1" paragraph>
                    This sections shows the recalculations of the osu! rankings using {name}, which is still work in progress. You can find the GitHub page <Link url={github} text="here" /> {!!commit ? `(commit: ${commit})` : ""}
                </Typography>
                <Typography align="left" variant="body1" paragraph>
                    If your scores are outdated, or if you're not on the list at all, you can add yourself to the queue for a recalculation.
                </Typography>
                <Typography align="left" variant="body1" paragraph>
                    All values shown are estimates, especially your rank! For any important stuff you want to share, you can find me <Link url="https://osu.ppy.sh/u/2330619" text="here" />
                </Typography>
            </>
        );
    };

    const getHelpTextOld = () => (
        <>
            <Typography align="left" variant="body1" paragraph>
                On this page you can recalculate your scores using the old pp algorithm for the Taiko gamemode (before September 2020)
            </Typography>
        </>
    );

    const getHelpText = () => {
        switch (algorithm?.code) {
            case "live": return getHelpTextLive();
            case "taiko_old": return getHelpTextOld();
            default: return getHelpTextRecalc();
        };
    };

    return (
        <Card>
            <CardContent>
                {getHelpText()}
            </CardContent>
        </Card>
    );
};

export default RankingsHelp;