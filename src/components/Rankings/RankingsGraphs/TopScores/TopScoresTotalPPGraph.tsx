import React, { useEffect, useState } from "react";
import { enforceModOrder, roundValue, SxStyles } from "helpers";
import { RankingsDataTopScores } from "common/interfaces/interfaces";
import { ChartOptions } from "chart.js";
import { Button, Card, CardContent, CardHeader, Checkbox, FormControl, InputLabel, ListItemText, MenuItem, Select, SelectChangeEvent } from "@mui/material";
import BaseChart from "common/components/BaseChart/BaseChart";
import _ from "lodash";
import { styled } from "@mui/styles";

interface IProps {
    data: RankingsDataTopScores;
    appliedMods: string[];
    applyMods: (mods: string[]) => void;
};

const styles: SxStyles = {
    cardHeader: {
        display: "flex",
        flexDirection: {
            xs: "column",
            sm: "row"
        },
        alignItems: {
            xs: "flex-start"
        }
    }
};

const StyledCardActions = styled("div")(({ theme }) => ({
    display: "flex",
    flexDirection: "row",
    alignItems: "end"
}));

const TopScoresTotalPPGraph: React.FC<IProps> = (props) => {
    const { data, appliedMods, applyMods } = props;
    const [mods, setMods] = useState<string[]>([]);
    const modsOptions = ["None", "EZ", "HT", "HD", "HR", "DT", "FL"];
    const [modsChanged, setModsChanged] = useState<boolean>(false);

    useEffect(() => {
        if (mods && appliedMods) {
            const isChanged = !_.isEqual(enforceModOrder(mods.join("")), enforceModOrder(appliedMods.join("")));
            if (isChanged !== modsChanged)
                setModsChanged(isChanged);
        }
    }, [mods, appliedMods, modsChanged]);

    const chartData = {
        labels: Array.from(Array(data.new.length)).map((_, i) => i + 1),
        datasets: [
            { label: "Live", data: data.old, borderColor: "#F47A1F", pointRadius: 0 },
            { label: "Local", data: data.new, borderColor: "#007CC3", pointRadius: 0 }
        ]
    };

    const chartOptions: ChartOptions = {
        plugins: {
            tooltip: {
                intersect: false,
                mode: "index",
                callbacks: {
                    title: (items) => {
                        return `Rank #${items[0].label}`;
                    }
                }
            }
        }
    };

    const handleModsChange = (event: SelectChangeEvent<typeof mods>) => {
        const { target: { value } } = event;
        let newMods: string[];
        if (typeof value !== "string" && value.length > mods.length && value.length > 1) {
            const newMod = value[value.length - 1];
            switch (newMod) {
                case "DT":
                    newMods = [...mods.filter(m => !["None", "NC", "HT"].includes(m)), newMod];
                    break;
                case "NC":
                    newMods = [...mods.filter(m => !["None", "DT", "HT"].includes(m)), newMod];
                    break;
                case "HT":
                    newMods = [...mods.filter(m => !["None", "NC", "DT"].includes(m)), newMod];
                    break;
                case "EZ":
                    newMods = [...mods.filter(m => !["None", "HR"].includes(m)), newMod];
                    break;
                case "HR":
                    newMods = [...mods.filter(m => !["None", "EZ"].includes(m)), newMod];
                    break;
                case "None":
                    newMods = [newMod];
                    break;
                default:
                    newMods = [...mods.filter(m => !["None"].includes(m)), newMod];
            };
        }
        else
            newMods = typeof value !== "string" ? value : [value];

        setMods(newMods);
    };

    return (
        <Card>
            <CardHeader
                sx={styles.cardHeader}
                title={`Top 500 scores: total pp ${appliedMods.length ? `(${appliedMods.join(", ")})` : ""}`}
                subheader={`Avg. pp change: ${roundValue(data.avgChange, 1)}`}
                action={
                    <StyledCardActions>
                        <FormControl variant="standard">
                            <InputLabel>Mods filter (for both graphs)</InputLabel>
                            <Select
                                value={mods}
                                onChange={handleModsChange}
                                multiple
                                sx={{ minWidth: 250, mr: 1 }}
                                renderValue={(selected) => selected.join(", ")}
                            >
                                {modsOptions.map(m => (
                                    <MenuItem key={m} value={m}>
                                        <Checkbox checked={mods.indexOf(m) > -1} />
                                        <ListItemText primary={m} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                            <Button color="primary" variant="contained" onClick={() => applyMods(mods)} disabled={!modsChanged}>Apply</Button>
                    </StyledCardActions>
                }
            />
            <CardContent>
                <BaseChart data={chartData} type="line" options={chartOptions} />
            </CardContent>
        </Card>
    );
};

export default TopScoresTotalPPGraph;