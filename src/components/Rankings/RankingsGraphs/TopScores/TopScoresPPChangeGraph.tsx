import React from "react";
import { RankingsDataTopScores } from "common/interfaces/interfaces";
import { ChartOptions } from "chart.js";
import { Card, CardContent, CardHeader } from "@mui/material";
import BaseChart from "common/components/BaseChart/BaseChart";

interface IProps {
    data: RankingsDataTopScores;
};

const TopScoresPPChangeGraph: React.FC<IProps> = (props) => {
    const { data } = props;

    const lowestValue = Math.min(...data.diff);
    const highestValue = Math.max(...data.diff);

    let stepValue = 20;
    let bottomBound = Math.floor(lowestValue / stepValue) * stepValue;
    let topBound = Math.ceil(highestValue / stepValue) * stepValue;
    const difference = topBound - bottomBound;

    if (difference >= 1000) {
        stepValue = 50;
        bottomBound = Math.floor(lowestValue / stepValue) * stepValue;
        topBound = Math.ceil(highestValue / stepValue) * stepValue;
    };

    const labels = [];
    for (let i = bottomBound; i < topBound; i += stepValue) {
        labels.push(i);
    };

    const dataset = labels.map(l => {
        const matches = data.diff.filter(d => d >= l && d < l + stepValue);
        return matches.length;
    });

    const colors = labels.map(l => l < 0 ? "#ba2f2f" : "#7AC142");

    const chartData = {
        labels,
        datasets: [{
            label: "pp change",
            data: dataset,
            backgroundColor: colors,
        }]
    };

    const chartOptions: ChartOptions = {
        plugins: {
            tooltip: {
                intersect: false,
                mode: "index",
                callbacks: {
                    title: (items) => {
                        return `PP change between ${items[0].label} and ${+items[0].label + stepValue}`;
                    },
                    label: (context) => {
                        return `Number of scores: ${context.formattedValue}`
                    }
                }
            },
            legend: {
                display: false
            }
        }
    };

    return (
        <Card>
            <CardHeader title="Top 500 scores: pp change" subheader={`Each bar displays a range of ${stepValue}pp, values displayed on x-axis are lower bounds`} />
            <CardContent>
                <BaseChart data={chartData} type="bar" options={chartOptions} />
            </CardContent>
        </Card>
    );
};

export default TopScoresPPChangeGraph;