import React, { Dispatch, SetStateAction, useContext, useEffect, useState } from "react";
import { apiGetRequest } from "helpers";
import { AlgorithmContext } from "common/context";
import { RankingsDataTopScores } from "common/interfaces/interfaces";
import LoadingIcon from "common/components/LoadingIcon/LoadingIcon";
import { Grid } from "@mui/material";
import TopScoresTotalPPGraph from "./TopScoresTotalPPGraph";
import TopScoresPPChangeGraph from "./TopScoresPPChangeGraph";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";

interface IProps {
    setShowForbiddenError: Dispatch<SetStateAction<boolean>>
};

const TopScoresGraphsContainer: React.FC<IProps> = (props) => {
    const { setShowForbiddenError } = props;
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);
    const [appliedMods, setAppliedMods] = useState<string[]>([]);
    const [data, setData] = useState<RankingsDataTopScores>();

    useEffect(() => {
        const fetchData = async () => {
            const modsParams = appliedMods.length ? appliedMods : "all";
            const { response, status } = await apiGetRequest<RankingsDataTopScores>(`statistics/topscores/${algorithm?.id}/${modsParams}`, undefined, oauth.token);
            if (status === 403) setShowForbiddenError(true);
            else setData(response);
        };

        if (oauth?.user?.id && algorithm?.id)
            fetchData();
    }, [oauth, algorithm, appliedMods, setShowForbiddenError]);

    const applyMods = (mods: string[]) => {
        setAppliedMods(mods);
    }

    if (!data) return <LoadingIcon />;

    return (
        <>
            <Grid item xs={12} lg={6}>
                <TopScoresTotalPPGraph data={data} appliedMods={appliedMods} applyMods={applyMods} />
            </Grid>
            <Grid item xs={12} lg={6}>
                <TopScoresPPChangeGraph data={data} />
            </Grid>
        </>
    );
};

export default TopScoresGraphsContainer;