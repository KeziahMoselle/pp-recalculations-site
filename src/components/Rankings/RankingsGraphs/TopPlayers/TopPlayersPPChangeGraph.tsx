import React from "react";
import { RankingsDataTopPlayers } from "common/interfaces/interfaces";
import { ChartOptions } from "chart.js";
import { Card, CardContent, CardHeader } from "@mui/material";
import BaseChart from "common/components/BaseChart/BaseChart";

interface IProps {
    data: RankingsDataTopPlayers;
};

const TopPlayersPPChangeGraph: React.FC<IProps> = (props) => {
    const { data } = props;

    const lowestValue = Math.min(...data.diff);
    const highestValue = Math.max(...data.diff);

    let bottomBound = Math.floor(lowestValue / 100) * 100;
    let topBound = Math.ceil(highestValue / 100) * 100;
    let stepValue = 100;
    const difference = topBound - bottomBound;

    if (difference >= 2500) {
        bottomBound = Math.floor(lowestValue / 250) * 250;
        topBound = Math.ceil(highestValue / 250) * 250;
        stepValue = 250;
    };

    const labels = [];
    for (let i = bottomBound; i < topBound; i += stepValue) {
        labels.push(i);
    };

    const dataset = labels.map(l => {
        const matches = data.diff.filter(d => d >= l && d < l + stepValue);
        return matches.length;
    });

    const colors = labels.map(l => l < 0 ? "#ba2f2f" : "#7AC142");

    const chartData = {
        labels,
        datasets: [{
            label: "pp change",
            data: dataset,
            backgroundColor: colors,
        }]
    };

    const chartOptions: ChartOptions = {
        plugins: {
            tooltip: {
                intersect: false,
                mode: "index",
                callbacks: {
                    title: (items) => {
                        return `PP change between ${items[0].label} and ${+items[0].label + stepValue}`;
                    },
                    label: (context) => {
                        return `Number of players: ${context.formattedValue}`
                    }
                }
            },
            legend: {
                display: false
            }
        }
    };

    return (
        <Card>
            <CardHeader title="Top 500 players: pp change" subheader={`Each bar displays a range of ${stepValue}pp; values displayed on x-axis are lower bounds`} />
            <CardContent>
                <BaseChart data={chartData} type="bar" options={chartOptions} />
            </CardContent>
        </Card>
    );
};

export default TopPlayersPPChangeGraph;