import React from "react";
import { roundValue } from "helpers";
import { RankingsDataTopPlayers } from "common/interfaces/interfaces";
import { ChartOptions } from "chart.js";
import { Card, CardContent, CardHeader } from "@mui/material";
import BaseChart from "common/components/BaseChart/BaseChart";

interface IProps {
    data: RankingsDataTopPlayers;
};

const TopPlayersTotalPPGraph: React.FC<IProps> = (props) => {
    const { data } = props;

    const chartData = {
        labels: Array.from(Array(data.new.length)).map((_, i) => i + 1),
        datasets: [
            { label: "Live", data: data.old, borderColor: "#F47A1F", pointRadius: 0 },
            { label: "Local", data: data.new, borderColor: "#007CC3", pointRadius: 0 }
        ]
    };

    const chartOptions: ChartOptions = {
        plugins: {
            tooltip: {
                intersect: false,
                mode: "index",
                callbacks: {
                    title: (items) => {
                        return `Rank #${items[0].label}`;
                    },
                }
            }
        }
    };

    return (
        <Card>
            <CardHeader title="Top 500 players: total pp" subheader={`Avg. pp change: ${roundValue(data.avgChange, 1)}`} />
            <CardContent>
                <BaseChart data={chartData} type="line" options={chartOptions} />
            </CardContent>
        </Card>
    );
};

export default TopPlayersTotalPPGraph;