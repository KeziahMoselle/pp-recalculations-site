import React, { Dispatch, SetStateAction, useContext, useEffect, useState } from "react";
import { AlgorithmContext } from "../../../../common/context";
import { RankingsDataTopPlayers } from "../../../../common/interfaces/interfaces";
import { apiGetRequest } from "../../../../helpers";
import LoadingIcon from "../../../../common/components/LoadingIcon/LoadingIcon";
import { Grid } from "@mui/material";
import TopPlayersTotalPPGraph from "./TopPlayersTotalPPGraph";
import TopPlayersPPChangeGraph from "./TopPlayersPPChangeGraph";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";

interface IProps {
    setShowForbiddenError: Dispatch<SetStateAction<boolean>>
};

const TopPlayersGraphsContainer: React.FC<IProps> = (props) => {
    const { setShowForbiddenError } = props;
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);
    const [data, setData] = useState<RankingsDataTopPlayers>();

    useEffect(() => {
        const fetchData = async () => {
            const { response, status } = await apiGetRequest<RankingsDataTopPlayers>(`statistics/topplayers/${algorithm?.id}`, undefined, oauth.token);
            if (status === 403) setShowForbiddenError(true);
            else setData(response);
        };

        if (oauth?.user?.id && algorithm?.id)
            fetchData();
    }, [oauth, algorithm, setShowForbiddenError]);

    if (!data) return <LoadingIcon />;

    return (
        <>
            <Grid item xs={12} lg={6}>
                <TopPlayersTotalPPGraph data={data} />
            </Grid>
            <Grid item xs={12} lg={6}>
                <TopPlayersPPChangeGraph data={data} />
            </Grid>
        </>
    );
};

export default TopPlayersGraphsContainer;