import React, { SyntheticEvent, useContext, useEffect, useState } from 'react';
import { Button, Snackbar } from '@mui/material';
import { styled } from "@mui/material/styles";
import { Player, ReworkTypeCode } from '../../common/interfaces/interfaces';
import StatisticsPanel from '../StatisticsPanel/StatisticsPanel';
import ToolBar from '../../common/components/ToolBar/ToolBar';
import StyledName from '../../common/components/StyledName/StyledName';
import { AlgorithmContext } from '../../common/context';
import { apiGetRequest, apiPatchRequest, roundValue, SxStyles } from '../../helpers';
import { useLocation, useHistory } from 'react-router';
import MuiAlert from "@mui/lab/Alert";
import PlayerTopRanks from './PlayerTopRanks';
import LoadingIcon from 'common/components/LoadingIcon/LoadingIcon';
import { useSelector } from 'react-redux';
import { IApplicationState } from 'store';
import ForbiddenError from 'common/components/Errors/ForbiddenError';

interface IProps { }

const styles: SxStyles = {
    csvButton: {
        height: "75%",
        fontSize: "12px",
        ml: 1,
        mt: 1
    }
};

export const StyledOptionsContainer = styled("div")(({ theme }) => ({
    display: "flex",
    justifyContent: "flex-start",
}));

const PlayerDetails: React.FC<IProps> = (props) => {
    const location = useLocation();
    const { oauth } = useSelector((state: IApplicationState) => state);
    const playerId = +(location.pathname.split('/')[2] || -1);
    const { algorithm } = useContext(AlgorithmContext);

    const [playerData, setPlayerData] = useState<Player>();
    const [version, setVersion] = useState<{ algorithm_version: number }>();

    const [snackBarOpen, setSnackBarOpen] = useState<boolean>(false);
    const [errorSnackBarOpen, setErrorSnackBarOpen] = useState<boolean>(false);
    const [showForbiddenError, setShowForbiddenError] = useState<boolean>(false);

    const history = useHistory();

    useEffect(() => {
        const fetchData = async () => {
            const [playerResponse] = await Promise.all([
                apiGetRequest<Player>(`player/userdata/${playerId}/${algorithm?.id}`, undefined, oauth.token),
            ]);

            if (playerResponse.status === 403) {
                setShowForbiddenError(true);
                setPlayerData({ user_id: -1, name: "" } as Player);
            }
            else setPlayerData(playerResponse.response);

            setVersion({ algorithm_version: algorithm?.algorithm_version ?? -1 });
        };

        if (oauth?.user?.id && algorithm && playerId)
            fetchData();
    }, [oauth, algorithm, playerId]);

    if (!version || !playerData) return <LoadingIcon />;

    const getTiles = () => {
        if (!playerData) return [];
        if (algorithm?.rework_type_code === ReworkTypeCode.LIVE)
            return [
                { label: "Weighted pp", value: roundValue(playerData.old_pp, 1) },
                { label: "Aim pp", value: roundValue(playerData.weighted_aim_pp, 1) },
                { label: "Tap pp", value: roundValue(playerData.weighted_tap_pp, 1) },
                { label: "Acc pp", value: roundValue(playerData.weighted_acc_pp, 1) },
                { label: "Bonus pp", value: roundValue(playerData.new_pp_incl_bonus - playerData.new_pp_excl_bonus, 3) }
            ];
        else
            return [
                { label: algorithm?.compare_with_master ? "Master pp" : "Old pp", value: roundValue(playerData.old_pp, 1) },
                { label: "New pp (excl. bonus)", value: roundValue(playerData.new_pp_excl_bonus, 1) },
                { label: "New pp (incl. bonus)", value: roundValue(playerData.new_pp_incl_bonus, 1) },
                { label: "Difference", value: roundValue((playerData.new_pp_incl_bonus - playerData.old_pp), 2) }
            ];
    };

    const tiles = getTiles();

    const goBack = () => {
        history.push(`/rankings/players/${algorithm?.code}`);
    };

    const openOsuProfile = () => {
        window.open(`https://osu.ppy.sh/u/${playerData.user_id}`);
    };

    const addPlayerToQueue = async () => {
        if (!playerData.user_id || isNaN(playerData.user_id)) return;
        const { error } = await apiPatchRequest(`queue/add-to-queue`, { user_id: playerData.user_id, rework: algorithm?.id }, oauth.token);
        if (error) setErrorSnackBarOpen(true);
        else setSnackBarOpen(true);
    };

    const handleSnackbarClose = (_: SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') return;
        setErrorSnackBarOpen(false);
        setSnackBarOpen(false);
    };

    return (
        <>
            <ToolBar shortTitle={playerData.name} longTitle={`Recalculated top ranks: ${playerData.name}`} />
            {showForbiddenError
                ? <ForbiddenError />
                : (
                    <>
                        <StatisticsPanel xs={1} sm={1} md={2} lg={2} xl={1} mainLabel={<StyledName name={playerData.name} country={playerData.country} />} tiles={tiles} />
                        <StyledOptionsContainer>
                            <Button sx={styles.csvButton} variant="outlined" color="primary" onClick={goBack} disabled={history.length <= 1}>Back</Button>
                            <Button sx={styles.csvButton} variant="contained" color="primary" onClick={openOsuProfile}>Open osu! profile</Button>
                            <Button sx={styles.csvButton} variant="contained" color="primary" onClick={addPlayerToQueue} disabled={!algorithm?.queue_enabled}>Add to queue</Button>
                        </StyledOptionsContainer>

                        <PlayerTopRanks player_id={playerData.user_id} />

                        <Snackbar open={errorSnackBarOpen} autoHideDuration={1500} onClose={handleSnackbarClose}>
                            <MuiAlert elevation={6} variant="filled" onClose={handleSnackbarClose} severity="error">
                                {`Invalid user id (it should be a number)`}
                            </MuiAlert>
                        </Snackbar>

                        <Snackbar open={snackBarOpen} autoHideDuration={1500} onClose={handleSnackbarClose}>
                            <MuiAlert elevation={6} variant="filled" onClose={handleSnackbarClose} severity="success">
                                {`${playerData.name} was added to the queue!`}
                            </MuiAlert>
                        </Snackbar>
                    </>
                )
            }
        </>
    );
};

export default PlayerDetails;