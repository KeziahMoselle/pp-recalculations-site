import React, { useContext, useEffect, useState } from "react";
import { apiGetRequest, convertDateFormat, enforceModOrder, isDefined, SxStyles } from "helpers";
import { RecalculatedPlayerData, ReworkTypeCode } from "common/interfaces/interfaces";
import { AlgorithmContext } from "common/context";
import { Button, Checkbox, FormControlLabel, Typography, useMediaQuery, useTheme } from "@mui/material";
import { FieldsOrder } from "common/enums/FieldsOrder";
import LoadingIcon from "common/components/LoadingIcon/LoadingIcon";
import moment from "moment";
import BaseTable from "common/components/BaseTable/BaseTable";
import { CSVLink } from "react-csv";
import { styled } from "@mui/material/styles";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import Searchbar from "common/components/Searchbar/Searchbar";
import PPDifferenceIndicator from "common/components/PPDifferenceIndicator/PPDifferenceIndicator";
import BetterTooltip from "common/components/BetterTooltip/BetterTooltip";

interface IProps {
    player_id: number;
};

const styles: SxStyles = {
    whiteText: {
        color: "text.primary"
    },
    csvButton: {
        height: "75%",
        fontSize: "12px"
    }
};

export const StyledOptionsContainer = styled("div")(({ theme }) => ({
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("md")]: {
        flexDirection: "column",
        alignItems: "flex-start"
    },
    marginLeft: theme.spacing(1),
    alignItems: "center"
}));

const PlayerTopRanks: React.FC<IProps> = (props) => {
    const { player_id } = props;
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth, settings } = useSelector((state: IApplicationState) => state);
    const [scores, setScores] = useState<RecalculatedPlayerData>();
    const [filteredScores, setFilteredScores] = useState<RecalculatedPlayerData>();

    const [topranksFilter, setTopranksFilter] = useState(true);
    const [showPPDetails, setShowPPDetails] = useState(false);
    const [searchValue, setSearchValue] = useState<string>("");

    const isSmallScreen = useMediaQuery(useTheme().breakpoints.down("sm"));

    useEffect(() => {
        const fetchData = async () => {
            const { response } = await apiGetRequest<RecalculatedPlayerData>(`player/topscores/${player_id}/${algorithm?.id}`, undefined, oauth.token);
            setScores(response);
        };

        if (oauth?.user?.id && player_id && algorithm)
            fetchData();
    }, [oauth, player_id, algorithm]);

    useEffect(() => {
        if (scores) {
            const topRanks = topranksFilter
                ? scores.filter(s => s.new_rank !== null)
                : scores;

            setFilteredScores(topRanks.filter(s => {
                const map_name = (s.artist.concat(s.title).concat(s.diff_name).concat(s.creator_name)).toLowerCase();
                return !searchValue.length || map_name.includes(searchValue);
            }));
        }
    }, [searchValue, topranksFilter, scores]);

    const getHeaders = () => {
        if (algorithm?.rework_type_code === ReworkTypeCode.LIVE)
            return [
                { key: "old_rank", value: "Rank" },
                { key: "map_name", value: "Map" },
                { key: "live_pp", value: isSmallScreen ? "PP" : "Weighted pp", isBoldColumn: true, tooltip: "The current pp value for this score" },
                { key: "aim_pp", value: "Aim", tooltip: "Aim is one of the skills used to calculate total pp. The aim pp values of each score are listed here." },
                { key: "tap_pp", value: "Tap", tooltip: "Tapping is one of the skills used to calculate total pp. The tap pp values of each score are listed here." },
                { key: "acc_pp", value: "Acc", tooltip: "Accuracy is one of the skills used to calculate total pp. The accuracy pp values of each score are listed here." },
                { key: "fl_pp", value: "FL", tooltip: "Flashlight is one of the skills used to calculate total pp. The flashlight pp values of each score are listed here." },
                { key: "mods", value: "Mods" },
                { key: "accuracy", value: "Accuracy" },
                { key: "max_combo", value: "Combo" },
                { key: "good", value: "100", maxWidth: "1%" },
                { key: "meh", value: "50", maxWidth: "1%" },
                { key: "miss", value: "Miss", maxWidth: "1%" },
                { key: "score_date", value: "Date" },
            ];
        else
            return [
                { key: "rank", value: "Rank", maxWidth: "1%" },
                { key: "map_name", value: "Map" },
                !algorithm?.compare_with_master ? { key: "live_pp", value: isSmallScreen ? "Live" : "Live", isBoldColumn: true, tooltip: "The current pp value for this score" } : null,
                algorithm?.compare_with_master ? { key: "master_pp", value: "Master", isBoldColumn: true, tooltip: "The pp value for this score according to the master branch. The master branch contains new, confirmed changes that have not been deployed to live yet." } : null,
                { key: "local_pp", value: isSmallScreen ? "Local" : "Local", isBoldColumn: true, tooltip: "The new pp value for this score according to this rework." },
                algorithm?.parsed_pp_skills.includes("aim") ? { key: "aim_pp", value: "Aim", tooltip: "Aim is one of the skills used to calculate total pp. The aim pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("tap") ? { key: "tap_pp", value: "Tap", tooltip: "Tapping is one of the skills used to calculate total pp. The tap pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("accuracy") ? { key: "acc_pp", value: "Acc", tooltip: "Accuracy is one of the skills used to calculate total pp. The accuracy pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("flashlight") ? { key: "fl_pp", value: "FL", tooltip: "Flashlight is one of the skills used to calculate total pp. The flashlight pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("visual") ? { key: "visual_pp", value: "Visual", tooltip: "Vision is one of the skills used to calculate total pp. The visual pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("cognition") ? { key: "cognition_pp", value: "Cognition", tooltip: "Cognition is one of the skills used to calculate total pp. The cognition pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("reading") ? { key: "reading_pp", value: "Reading", tooltip: "Reading is one of the skills used to calculate total pp. The reading pp values of each score are listed here." } : null,
                { key: "difference", value: "Diff." },
                { key: "mods", value: "Mods", maxWidth: "1%" },
                { key: "accuracy", value: "Accuracy" },
                { key: "max_combo", value: "Combo" },
                algorithm?.gamemode === 3 ? { key: "perfect", value: "Epic", maxWidth: "1%" } : null,
                algorithm?.gamemode === 3 ? { key: "great", value: "300", maxWidth: "1%" } : null,
                algorithm?.gamemode === 3 ? { key: "good", value: "200", maxWidth: "1%" } : null,
                algorithm?.gamemode !== 3 ? { key: "good", value: "100", maxWidth: "1%" } : null,
                algorithm?.gamemode === 3 ? { key: "ok", value: "100", maxWidth: "1%" } : null,
                { key: "meh", value: "50", maxWidth: "1%" },
                { key: "miss", value: "Miss", maxWidth: "1%" },
                { key: "score_date", value: "Date" },
            ]
                .filter(isDefined)
                .filter(h => {
                    return (showPPDetails || !(h.key === "aim_pp" || h.key === "tap_pp" || h.key === "acc_pp" || h.key === "fl_pp" || h.key === "visual_pp" || h.key === "cognition_pp" || h.key === "reading_pp"))
                });
    };

    const headers = getHeaders().map(h => ({ ...h, sortAllowed: true }));

    const spliceIndex = headers.findIndex(h => h.key === "max_combo");
    if (isSmallScreen && spliceIndex >= 0) headers.splice(spliceIndex, 5);

    if (!scores || !filteredScores) return <LoadingIcon />;
    if (!scores.length) return <Typography color="white">No scores found</Typography>;

    const rows = filteredScores.map((row, index: number) => {
        const difference = +(row.local_pp - (algorithm?.compare_with_master ? +row.master_pp : +row.live_pp)).toFixed(1);
        const rankDifference = row.new_rank && row.old_rank ? row.old_rank - row.new_rank : 0;
        const rankDiffLabel = rankDifference > 0
            ? `+${rankDifference}`
            : rankDifference < 0
                ? rankDifference
                : "-";

        return {
            key: index,
            values: {
                ...row,
                rank: {
                    value: rankDifference,
                    element: <span>
                        {row.new_rank ?? "-"}
                        <Typography variant="caption" sx={{ fontSize: "11px", color: rankDifference < 0 ? "#da4949" : (rankDifference > 0 ? "#49da4c" : "#dea916") }}> {rankDiffLabel}</Typography>
                    </span>
                },
                old_rank: row.old_rank ?? "-",
                local_pp: { element: +(row.local_pp ?? 0).toFixed(1), value: row.local_pp ?? 0 },
                live_pp: { element: +(+row.live_pp).toFixed(1), value: row.live_pp },
                master_pp: { element: +(row.master_pp ?? 0).toFixed(1), value: row.master_pp ?? 0 },
                difference: { value: difference || 0, element: <PPDifferenceIndicator value={difference} /> },
                aim_pp: { element: +(row.aim_pp ?? 0).toFixed(1), value: row.aim_pp },
                tap_pp: { element: +(row.tap_pp ?? 0).toFixed(1), value: row.tap_pp },
                acc_pp: { element: +(row.acc_pp ?? 0).toFixed(1), value: row.acc_pp },
                fl_pp: { element: +(row.fl_pp ?? 0).toFixed(1), value: row.fl_pp },
                visual_pp: { element: +(row.visual_pp ?? 0).toFixed(1), value: row.visual_pp },
                cognition_pp: { element: +(row.cognition_pp ?? 0).toFixed(1), value: row.cognition_pp },
                reading_pp: { element: +(row.reading_pp ?? 0).toFixed(1), value: row.reading_pp },
                accuracy: +row.accuracy.toFixed(2),
                mods: enforceModOrder(row.mods.replace(/, /g, '')),
                score_date: moment(row.score_date).format(convertDateFormat(settings.dateTimeFormat)),
                map_name: `${row.artist} - ${row.title} (${row.creator_name}) [${row.diff_name}]`
            },
            isGreenBackground: settings.highlightNewTop100Scores && row.new_rank <= 100 && row.old_rank > 100,
            isOrangeBackground: row.new_rank === null
        };
    });

    const fields = Object.keys(scores[0]);
    fields.sort((a, b) => (FieldsOrder as any)[a] - (FieldsOrder as any)[b]);
    const excludedFields = ["_id", "new_top_rank"];
    const filteredFields = fields.filter(f => !excludedFields.includes(f));
    const csvRows = scores.map(row => filteredFields.map(field => (row as any)[field]));
    const csvData = [filteredFields].concat(csvRows);

    const handleRowClick = (row: any, mouseButton?: number) => {
        if (mouseButton === 2) return;
        window.open(`https://osu.ppy.sh/b/${row.beatmap_id}`);
    };

    const exportToCsv = () => {
        document.getElementById('csv-link')?.click();
    };

    return (
        <>
            <CSVLink id="csv-link" style={{ display: "none" }} data={csvData} separator=";" title={`${player_id}`} filename={`${player_id}_${algorithm?.code}.csv`}>Export to CSV</CSVLink>
            <StyledOptionsContainer>
                <div style={{ display: "flex", alignItems: "center" }}>
                    <BetterTooltip title={["If you have multiple scores on the same map with different mod combinations, then only the best score will count towards your topranks.", "Disabling this toggle will show them in the list below too, highlighted in orange."]}>
                        <FormControlLabel control={
                            <Checkbox name="topranksFilterChecked" checked={topranksFilter} onChange={() => setTopranksFilter(!topranksFilter)} color="primary" />
                        } sx={styles.whiteText} label="Filter to topranks" style={{ textDecoration: "underline dotted", textUnderlineOffset: 4 }} />
                    </BetterTooltip>
                    {algorithm?.rework_type_code !== ReworkTypeCode.LIVE && algorithm?.parsed_pp_skills?.length ? (
                        <FormControlLabel control={
                            <Checkbox name="showPPDetailChecked" checked={showPPDetails} onChange={() => setShowPPDetails(!showPPDetails)} color="primary" />
                        } sx={styles.whiteText} label="Show separate pp values" />
                    ) : null}
                    {!isSmallScreen && (
                        <BetterTooltip title={["Make sure to set your decimal separator to . and thousands separator to , if you plan on opening this in Excel"]}>
                            <Button sx={styles.csvButton} variant="contained" color="primary" onClick={exportToCsv}>Export to CSV</Button>
                        </BetterTooltip>
                    )}
                </div>
                <Searchbar label="Search map" value={searchValue} update={setSearchValue} delay={500} styling={{ mb: 2 }} />
            </StyledOptionsContainer>

            <BaseTable
                headers={headers}
                rows={rows}
                handleClick={handleRowClick}
                defaultSort="desc"
                defaultSortField={algorithm?.rework_type_code === ReworkTypeCode.LIVE ? "live_pp" : "local_pp"}
                containerStyle={{ maxHeight: isSmallScreen ? "100%" : "calc(100vh - 259px)" }}
            />
        </>
    );
};

export default PlayerTopRanks;