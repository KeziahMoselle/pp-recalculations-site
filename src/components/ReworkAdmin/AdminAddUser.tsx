import React, { ChangeEvent, useContext, useState } from "react";
import { apiPatchRequest, SxStyles } from "helpers";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, Grid, InputLabel, MenuItem, Select, SelectChangeEvent, TextField, Typography } from "@mui/material";
import { ReworkPermissionEnum, UsersWithAccess } from "common/interfaces/OAuth";
import { AlgorithmContext } from "common/context";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import ForbiddenSnackbar from "common/components/Errors/ForbiddenSnackbar";

interface IProps {
    open: boolean,
    onClose: (newUsers?: UsersWithAccess) => void
};

const styles: SxStyles = {
    dialogContent: {
        p: 1
    },
    textField: {
        minWidth: 200
    }
};

const AdminAddUserDialog: React.FC<IProps> = (props) => {
    const { open, onClose } = props;
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);
    const [userId, setUserId] = useState<number>();
    const [role, setRole] = useState<ReworkPermissionEnum>();
    const [showErrorSnackbar, setShowErrorSnackbar] = useState<boolean>(false);
    const [errorText, setErrorText] = useState<string>();

    const save = async () => {
        if (!userId || isNaN(userId) || !role || !algorithm?.id || !oauth?.user?.id) return;
        const { response, error } = await apiPatchRequest<UsersWithAccess>(`users/add`, { userId, role, rework: algorithm?.id }, oauth.token);
        if (error) {
            setErrorText(error);
            setShowErrorSnackbar(true);
        }
        else onClose(response);
    };

    const cancel = async () => {
        onClose();
    };

    const handleUserIdChange = (event: ChangeEvent<HTMLInputElement>) => {
        const value = +event.target.value;
        if (!isNaN(value))
            setUserId(value);
    };

    const handleRoleChange = (event: SelectChangeEvent) => {
        setRole(+event.target.value as ReworkPermissionEnum);
    };

    return (
        <>
            <Dialog open={open} disableEscapeKeyDown maxWidth="sm" fullWidth>
                <DialogTitle>Add user</DialogTitle>
                <DialogContent>
                    <Grid container spacing={2} sx={{ mt: 1 }}>
                        <Grid item xs={12}>
                            <TextField sx={styles.textField} type="number" fullWidth label="User ID" value={userId} onChange={handleUserIdChange} />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl fullWidth>
                                <InputLabel>Role</InputLabel>
                                <Select value={role?.toString()} onChange={handleRoleChange} label="Role">
                                    <MenuItem value={ReworkPermissionEnum.ADMIN}>Admin</MenuItem>
                                    <MenuItem value={ReworkPermissionEnum.READER}>Reader</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>

                        <Grid item xs={12}>
                            {role === ReworkPermissionEnum.ADMIN && (
                                <Typography variant="body1" sx={{ color: "white" }}>Admins can view all data for this rework and are also able to add/remove other users. Admins can only be removed by hyperadmins.</Typography>
                            )}
                            {role === ReworkPermissionEnum.READER && (
                                <Typography variant="body1" sx={{ color: "white" }}>Readers can view all data for this rework, but have no access to this user panel. They can be removed by admins at any time.</Typography>
                            )}
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button variant="text" onClick={cancel}>Cancel</Button>
                    <Button variant="contained" onClick={save} disabled={!userId || !role}>Save</Button>
                </DialogActions>
            </Dialog>

            <ForbiddenSnackbar open={showErrorSnackbar} onClose={setShowErrorSnackbar} text={errorText} />
        </>
    );
};

export default AdminAddUserDialog;