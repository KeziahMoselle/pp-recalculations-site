import React, { ChangeEvent, useContext, useState } from "react";
import { apiPatchRequest, getReworkTypeNameByCode, SxStyles } from "helpers";
import { AlgorithmContext } from "common/context";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import { Algorithm, ReworkTypeCode } from "common/interfaces/interfaces";
import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, FormControlLabel, Grid, InputLabel, MenuItem, Select, SelectChangeEvent, TextField } from "@mui/material";
import ForbiddenSnackbar from "common/components/Errors/ForbiddenSnackbar";

interface IProps {
    open: boolean,
    onClose: () => void
};

const styles: SxStyles = {};

const AdminChangeReworkSettingsDialog: React.FC<IProps> = (props) => {
    const { open, onClose } = props;
    const { algorithm, setAlgorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);

    const [name, setName] = useState<string>(algorithm?.name ?? "");
    const [banner_text, setBannerText] = useState<string>(algorithm?.banner_text ?? "");
    const [rework_type, setReworkType] = useState<ReworkTypeCode>(algorithm?.rework_type_code ?? ReworkTypeCode.REWORK_PRIVATE_ACTIVE);
    const [for_onions, setForOnions] = useState<boolean>(false);

    const [showErrorSnackbar, setShowErrorSnackbar] = useState<boolean>(false);
    const [errorText, setErrorText] = useState<string>();

    const save = async () => {
        const { response, error } = await apiPatchRequest<Algorithm>(`reworks/update`, { ...algorithm, name, banner_text, rework_type_code: rework_type, for_onions }, oauth?.token);
        if (error) {
            setErrorText(error);
            setShowErrorSnackbar(true);
        }
        else {
            setAlgorithm(response);
            onClose();
        }
    };

    const handleNameChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setName(event.target.value);
    };

    const handleBannerChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setBannerText(event.target.value);
    };

    const handleReworkTypeChange = (event: SelectChangeEvent) => {
        setReworkType(event.target.value as ReworkTypeCode);
    };

    const { LIVE, MASTER, REWORK_PUBLIC_ACTIVE, REWORK_PUBLIC_INACTIVE, REWORK_PRIVATE_ACTIVE, REWORK_PRIVATE_INACTIVE, HISTORIC } = ReworkTypeCode;
    const reworkTypesOptions = [LIVE, MASTER, REWORK_PUBLIC_ACTIVE, REWORK_PUBLIC_INACTIVE, REWORK_PRIVATE_ACTIVE, REWORK_PRIVATE_INACTIVE, HISTORIC];

    return (
        <>
            <Dialog open={open} disableEscapeKeyDown maxWidth="sm" fullWidth>
                <DialogTitle>Update rework settings</DialogTitle>
                <DialogContent>
                    <Grid container spacing={2} sx={{ mt: 1 }}>
                        <Grid item xs={12}>
                            <TextField sx={styles.textField} fullWidth label="Name" value={name} onChange={handleNameChange} />
                        </Grid>

                        <Grid item xs={12}>
                            <FormControl fullWidth>
                                <InputLabel>Rework type</InputLabel>
                                <Select value={rework_type} onChange={handleReworkTypeChange} label="Rework type">
                                    {reworkTypesOptions.map(o => (
                                        <MenuItem key={o} value={o}>{getReworkTypeNameByCode(o)}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>

                        <Grid item xs={12}>
                            <TextField fullWidth label="Banner" value={banner_text} onChange={handleBannerChange} multiline />
                        </Grid>

                        <Grid item xs={12}>
                            <FormControlLabel
                                control={<Checkbox checked={for_onions} onChange={() => setForOnions(!for_onions)} color="primary" />}
                                label="Grant access to onions"
                                sx={{ color: "white" }}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button variant="text" onClick={onClose}>Cancel</Button>
                    <Button variant="contained" color="primary" onClick={save} disabled={!name?.length}>Save</Button>
                </DialogActions>
            </Dialog>

            <ForbiddenSnackbar open={showErrorSnackbar} onClose={setShowErrorSnackbar} text={errorText} />
        </>
    );
};

export default AdminChangeReworkSettingsDialog;