import React, { ChangeEvent, useContext, useEffect, useState } from "react";
import { apiGetRequest } from "helpers";
import { AlgorithmContext } from "common/context";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import ForbiddenError from "common/components/Errors/ForbiddenError";
import LoadingIcon from "common/components/LoadingIcon/LoadingIcon";
import { FormControl, FormControlLabel, FormLabel, Grid, Radio, RadioGroup, TextField, Typography } from "@mui/material";
import { BranchRadio } from "./UpdateReworkWizard";

interface IProps {
    radioValue: BranchRadio,
    newBranch?: string,
    onChange: (radio: BranchRadio, branch: string) => void
};

interface ReworkVersion {
    branch: string,
    commit: string
};

const SelectVersionStep: React.FC<IProps> = (props) => {
    const { radioValue, newBranch, onChange } = props;
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);
    const [currentVersion, setCurrentVersion] = useState<ReworkVersion>();
    const [showForbiddenError, setShowForbiddenError] = useState<boolean>(false);

    useEffect(() => {
        const fetchData = async () => {
            const { status, response } = await apiGetRequest<ReworkVersion>(`releases/current`, undefined, oauth.token, algorithm?.code);
            if (status === 403) setShowForbiddenError(true);
            else {
                setCurrentVersion(response);
                onChange("same", response.branch);
            }
        };

        if (oauth?.user?.id && algorithm?.code)
            fetchData();
        //eslint-disable-next-line
    }, [oauth, algorithm]);

    const handleRadioChange = (event: ChangeEvent<HTMLInputElement>) => {
        const newValue = (event.target as HTMLInputElement).value as BranchRadio;
        onChange(newValue, newValue === "other" ? "" : currentVersion?.branch ?? "");
    };

    const handleNewBranchChange = (event: ChangeEvent<HTMLInputElement>) => {
        onChange(radioValue, event.target.value);
    };

    if (showForbiddenError) return <ForbiddenError />;
    if (!currentVersion) return <LoadingIcon />;

    return (
        <Grid container spacing={2} sx={{ mt: 1 }}>
            <Grid item xs={12} sm={6}>
                <TextField label="Current branch" value={currentVersion.branch} fullWidth disabled />
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField label="Current commit" value={currentVersion.commit} fullWidth disabled />
            </Grid>

            <Grid item xs={12}>
                <FormControl component="fieldset">
                    <FormLabel component="legend">Select the branch to use</FormLabel>
                    <RadioGroup defaultValue="same" value={radioValue} onChange={handleRadioChange}>
                        <FormControlLabel value="same" control={<Radio />} label="Use same branch" />
                        <FormControlLabel value="other" control={<Radio />} label="Use different branch" />
                    </RadioGroup>
                </FormControl>
            </Grid>

            {radioValue === "other" && (
                <Grid item xs={12}>
                    <Typography variant="body1">Enter the new branch name. Be sure to not make any typos</Typography>
                    <TextField label="New branch" value={newBranch} onChange={handleNewBranchChange} fullWidth sx={{ mt: 1 }} />
                </Grid>
            )}
        </Grid>
    );
};

export default SelectVersionStep;