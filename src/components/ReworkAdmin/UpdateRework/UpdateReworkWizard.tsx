import React, { useContext, useState } from "react";
import { apiPatchRequest } from "helpers";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Step, StepLabel, Stepper } from "@mui/material";
import SelectVersionStep from "./SelectVersionStep";
import PrepareReleaseStep from "./PrepareReleaseStep";
import TestReleaseStep from "./TestReleaseStep";
import ConfirmReleaseStep from "./ConfirmReleaseStep";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import { AlgorithmContext } from "common/context";
import ForbiddenSnackbar from "common/components/Errors/ForbiddenSnackbar";
import LoadingIcon from "common/components/LoadingIcon/LoadingIcon";

interface IProps {
    open: boolean,
    onClose: () => void
};

export type BranchRadio = "same" | "other";

const UpdateReworkWizard: React.FC<IProps> = (props) => {
    const { open, onClose } = props;
    const [activeStep, setActiveStep] = useState<number>(0);
    const { oauth } = useSelector((state: IApplicationState) => state);
    const { algorithm } = useContext(AlgorithmContext);

    const [branchRadio, setBranchRadio] = useState<BranchRadio>("same");
    const [newBranch, setNewBranch] = useState<string>("");
    const [isReleasePrepared, setIsReleasePrepared] = useState<boolean>(false);
    const [isConfirmed, setIsConfirmed] = useState<boolean>(false);

    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [showError, setShowError] = useState<boolean>(false);
    const [errorText, setErrorText] = useState<string>();

    const steps = [
        "Select branch",
        "Prepare new release",
        "Testing",
        "Confirmation"
    ];

    const cancel = () => {
        setActiveStep(0);
        onClose();
    };

    const confirm = async () => {
        setIsLoading(true);
        const { error } = await apiPatchRequest("releases/confirm-release", branchRadio === "other" ? { branch: newBranch } : undefined, oauth?.token, algorithm?.code);
        if (error) {
            setErrorText(error);
            setShowError(true);
        }
        else
            onClose();
        setIsLoading(false);
    };

    const updateBranchInfo = (branchRadioValue: BranchRadio, newBranchValue: string) => {
        if (branchRadioValue !== branchRadio) setBranchRadio(branchRadioValue);
        if (newBranchValue !== newBranch) setNewBranch(newBranchValue);
    };

    const nextStep = () => setActiveStep(s => Math.min(s + 1, steps.length - 1));
    // const previousStep = () => setActiveStep(s => Math.max(0, s - 1));

    const isInputValid = (): boolean => {
        if (isLoading) return false;
        switch (activeStep) {
            case 0: return branchRadio === "same" || (newBranch ?? "").length > 0;
            case 1: return isReleasePrepared;
            case 2: return true;
            case 3: return isConfirmed;
            default: return false;
        };
    };

    return (
        <>
            <Dialog open={open} disableEscapeKeyDown maxWidth="md" fullWidth>
                <DialogTitle>Update rework</DialogTitle>
                <DialogContent>
                    <Stepper activeStep={activeStep} alternativeLabel>
                        {steps.map(step => (
                            <Step key={step}>
                                <StepLabel>{step}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    {activeStep === 0 && <SelectVersionStep radioValue={branchRadio} newBranch={newBranch} onChange={updateBranchInfo} />}
                    {activeStep === 1 && <PrepareReleaseStep newBranch={newBranch} isDifferentBranch={branchRadio === "other"} onSuccess={() => setIsReleasePrepared(true)} setLoading={setIsLoading} />}
                    {activeStep === 2 && <TestReleaseStep />}
                    {activeStep === 3 && <ConfirmReleaseStep isConfirmed={isConfirmed} setIsConfirmed={setIsConfirmed} />}
                    {isLoading && <LoadingIcon />}
                </DialogContent>
                <DialogActions>
                    <Button variant="text" onClick={cancel} disabled={isLoading}>Cancel</Button>
                    {/* {activeStep > 0 && <Button variant="outlined" color="primary" onClick={previousStep}>Previous</Button>} */}
                    {activeStep < steps.length - 1 && <Button variant="contained" color="primary" onClick={nextStep} disabled={!isInputValid()}>Next</Button>}
                    {activeStep === steps.length - 1 && <Button variant="contained" color="primary" disabled={!isInputValid()} onClick={confirm}>Confirm release</Button>}
                </DialogActions>
            </Dialog>
            <ForbiddenSnackbar text={errorText} open={showError} onClose={setShowError} />
        </>
    );
};

export default UpdateReworkWizard;