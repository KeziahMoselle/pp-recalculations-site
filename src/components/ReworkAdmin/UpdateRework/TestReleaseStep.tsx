import React, { useContext, useState } from "react";
import { apiPatchRequest } from "helpers";
import { Grid, Typography } from "@mui/material";
import ScoreCalculatorForm from "components/ScoreCalculator/ScoreCalculatorForm";
import { ScoreCalculationResponse } from "components/ScoreCalculator/ScoreCalculator";
import { AlgorithmContext } from "common/context";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import ScoreCalculatorResponseDialog from "components/ScoreCalculator/ScoreCalculatorResponseDialog";

interface IProps { };

const TestReleaseStep: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [responseDialogOpen, setResponseDialogOpen] = useState<boolean>(false);
    const [testResponse, setTestResponse] = useState<ScoreCalculationResponse>();

    const testRework = async (body: any) => {
        setIsLoading(true);
        const { response } = await apiPatchRequest<ScoreCalculationResponse>("calculate-score", { ...body, rework: algorithm?.code, useTestRelease: true }, oauth?.token);
        if (response?.map_name) {
            setTestResponse(response);
            setResponseDialogOpen(true);
        }
        setIsLoading(false);
    };

    return (
        <>
            <Grid container spacing={2} sx={{ mt: 1 }}>
                <Grid item xs={12}>
                    <ScoreCalculatorForm loading={isLoading} calculate={testRework} message="Please use the calculator below to test the new release" />
                </Grid>
            </Grid>

            <Grid item xs={12} sx={{ mt: 2 }}>
                <Typography variant="body1">If everything looks good, continue to the final step</Typography>
            </Grid>

            <ScoreCalculatorResponseDialog open={responseDialogOpen} onClose={() => setResponseDialogOpen(false)} score={testResponse} />
        </>
    );
};

export default TestReleaseStep;