import React, { useContext, useState } from "react";
import { apiPatchRequest } from "helpers";
import { Grid, Typography, Button } from "@mui/material";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import { AlgorithmContext } from "common/context";
import LoadingIcon from "common/components/LoadingIcon/LoadingIcon";

interface IProps {
    newBranch: string,
    isDifferentBranch: boolean,
    onSuccess: () => void;
    setLoading: (loading: boolean) => void;
};

const PrepareReleaseStep: React.FC<IProps> = (props) => {
    const { newBranch, isDifferentBranch, onSuccess, setLoading } = props;
    const { oauth } = useSelector((state: IApplicationState) => state);
    const { algorithm } = useContext(AlgorithmContext);

    const [showError, setShowError] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isPreparingRelease, setIsPreparingRelease] = useState<boolean>(false);

    const [repoUpdateResponse, setRepoUpdateResponse] = useState<string>("");
    const [newCommit, setNewCommit] = useState<string>();

    const [createReleaseResponse, setCreateReleaseResponse] = useState<string>("");

    const updateRepository = async () => {
        setIsLoading(true);
        const { error, response } = await apiPatchRequest<{ message: string, commit: string }>(`releases/update`, isDifferentBranch ? { branch: newBranch } : undefined, oauth?.token, algorithm?.code);
        if (error) {
            setRepoUpdateResponse(error);
            setShowError(true);
        }
        else {
            setNewCommit(response.commit);
            setShowError(false);
            setRepoUpdateResponse(response.message);
        }
        setIsLoading(false);
    };

    const createRelease = async () => {
        if (showError) return;

        setIsPreparingRelease(true);
        setLoading(true);

        const { error, response } = await apiPatchRequest<{ message: string }>(`releases/create-release`, undefined, oauth?.token, algorithm?.code);
        if (error) {
            setCreateReleaseResponse(error);
            setShowError(true);
        }
        else {
            setShowError(false);
            setCreateReleaseResponse(response.message);
            onSuccess();
        }

        setIsPreparingRelease(false);
        setLoading(false);
    };

    const renderRepoUpdateResponseText = () => {
        switch (repoUpdateResponse) {
            case "Updated": return `Branch ${newBranch} has been updated to commit ${newCommit}. If this is correct, click the "Prepare release" button to continue`;
            case "Nothing changed": return isDifferentBranch
                ? `The branch was switched to ${newBranch} (commit ${newCommit}). If this is correct, click the "Prepare release" button to continue`
                : `The repository already is on branch ${newBranch} (commit ${newCommit}). Creating a new release should not make any difference. Are you sure this is what you want?`;
            case "idk": return `The repo seems to have succesfully updated to branch ${newBranch} (commit ${newCommit}) but with an unknown status. Ideally you'd let Mr HeliX know that this is pretty weird, but if you like to live dangerously I will allow you to continue`;
            case "Local changes detected": return `Local changes were detected; please contact Mr HeliX so he can fix it`;
            case "Conflicts detected": return `Merge conflicts were detected; please contact Mr HeliX so he can fix it`;
            default: return "I have no clue what just happened";
        };
    };

    return (
        <>
            <Grid container spacing={2} sx={{ mt: 1 }}>
                <Grid item xs={12}>
                    <Typography variant="body1">
                        {isDifferentBranch
                            ? `By clicking update, the repository will switch to branch "${newBranch}" and pull it to the latest commit`
                            : `By clicking update, the repository will stay on branch "${newBranch}" and pull it to the latest commit`
                        }
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" onClick={updateRepository} disabled={isLoading || showError || repoUpdateResponse?.length > 0}>Update</Button>
                </Grid>
                {repoUpdateResponse?.length > 0 && (
                    <>
                        <Grid item xs={12}>
                            <Typography variant="body1" sx={showError ? { color: "#da4949" } : {}}>
                                {renderRepoUpdateResponseText()}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" color="primary" onClick={createRelease} disabled={isPreparingRelease || showError || createReleaseResponse?.length > 0}>Prepare release</Button>
                        </Grid>
                    </>
                )}

                {createReleaseResponse?.length > 0 && (
                    <Grid item xs={12}>
                        <Typography variant="body1" sx={showError ? { color: "#da4949" } : {}}>
                            {showError
                                ? `Something went wrong when trying to prepare a new release. Please contact Mr HeliX so he can fix it for you`
                                : `A new release has succesfully been prepared. Please continue to the next step`
                            }
                        </Typography>
                    </Grid>
                )}

                {(isLoading || isPreparingRelease) && (
                    <Grid item xs={12} sx={{ mt: 3, textAlign: "center" }}>
                        {isPreparingRelease && <Typography variant="body2">This might take a few minutes, please wait patiently..</Typography>}
                        {isLoading && <LoadingIcon />}
                    </Grid>
                )}
            </Grid>
        </>
    );
};

export default PrepareReleaseStep;