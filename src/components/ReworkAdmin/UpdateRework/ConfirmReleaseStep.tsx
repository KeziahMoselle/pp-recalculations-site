import React from "react";
import { Checkbox, FormControlLabel, Grid, Typography } from "@mui/material";

interface IProps {
    isConfirmed: boolean,
    setIsConfirmed: (confirmed: boolean) => void
};

const ConfirmReleaseStep: React.FC<IProps> = (props) => {
    const { isConfirmed, setIsConfirmed } = props;

    const handleCheckboxChange = (event: any) => {
        setIsConfirmed(event.target.checked);
    };

    return (
        <Grid container spacing={2} sx={{ mt: 1 }}>
            <Grid item xs={12}>
                <Typography variant="body1">
                    By clicking the "Confirm release" button, the following will happen:
                </Typography>
                <Typography variant="body1">
                    <ul>
                        <li>The current release will be overwritten with the release you just prepared</li>
                        <li>All current existing data will be set to outdated</li>
                        <li>The calculators will automatically start updating all players, removing the outdated data in the process</li>
                    </ul>
                </Typography>
                <Typography variant="body1">
                    This process cannot be reversed. If this rework is public and/or used a lot at the moment, please think twice before confirming a new release.
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <FormControlLabel
                    control={<Checkbox checked={isConfirmed} onChange={handleCheckboxChange} color="primary" />}
                    label="I promise I will not be an idiot"
                    sx={{ color: "white" }}
                />
            </Grid>
        </Grid>
    );
};

export default ConfirmReleaseStep;