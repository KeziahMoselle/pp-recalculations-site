import React, { useContext, useEffect, useState } from "react";
import { apiDeleteRequest, apiGetRequest, SxStyles, totalPPReworks } from "helpers";
import { UsersWithAccess } from "common/interfaces/OAuth";
import { AlgorithmContext } from "common/context";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import ForbiddenError from "../../common/components/Errors/ForbiddenError";
import { Button, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import LoadingIcon from "common/components/LoadingIcon/LoadingIcon";
import { styled } from "@mui/material/styles";
import AdminAddUserDialog from "./AdminAddUser";
import ForbiddenSnackbar from "common/components/Errors/ForbiddenSnackbar";
import UpdateReworkWizard from "./UpdateRework/UpdateReworkWizard";
import AdminChangeReworkSettingsDialog from "./AdminChangeReworkSettings";

interface IProps { };

const StyledOptionsBar = styled("div")(({ theme }) => ({
    display: "flex",
    justifyContent: "flex-start",
    [theme.breakpoints.down("sm")]: {
        flexDirection: "column",
        alignItems: "flex-start"
    },
    padding: theme.spacing(1)
}));

const styles: SxStyles = {
    button: {
        mr: 1,
        mt: {
            xs: 1,
            md: 0
        }
    }
};

const ReworkAdmin: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);
    const [reworkUsers, setReworkUsers] = useState<UsersWithAccess>();
    const [showForbiddenError, setShowForbiddenError] = useState<boolean>(false);

    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [changeNameDialogOpen, setChangeNameDialogOpen] = useState<boolean>(false);
    const [wizardOpen, setWizardOpen] = useState<boolean>(false);
    const [showForbiddenSnackbar, setShowForbiddenSnackbar] = useState<boolean>(false);
    const [errorText, setErrorText] = useState<string>();

    useEffect(() => {
        const fetchData = async () => {
            const { response, status } = await apiGetRequest<UsersWithAccess>(`users/list/${algorithm?.id}`, undefined, oauth.token);
            if (status === 403) setShowForbiddenError(true);
            else setReworkUsers(response);
        };

        if (algorithm?.id && oauth?.user?.id)
            fetchData();
    }, [oauth, algorithm]);

    if (showForbiddenError) return <ForbiddenError />;
    if (!reworkUsers) return <LoadingIcon />;

    const openAddUserDialog = () => {
        setDialogOpen(true);
    };

    const openUpdateReworkWizard = () => {
        setWizardOpen(true);
    };

    const handleCloseDialog = (newUsers?: UsersWithAccess) => {
        if (newUsers)
            setReworkUsers(newUsers);
        setDialogOpen(false);
    };

    const handleCloseWizard = () => {
        setWizardOpen(false);
    };

    const removeUser = async (userId: number) => {
        if (isNaN(userId)) return;
        const { error, response } = await apiDeleteRequest<UsersWithAccess>(`users/delete`, { userId, rework: algorithm?.id }, oauth.token);
        if (error) {
            setErrorText(error);
            setShowForbiddenSnackbar(true);
        }
        else setReworkUsers(response);
    };

    return (
        <>
            <StyledOptionsBar>
                <Button variant="contained" onClick={openAddUserDialog} sx={styles.button}>Add user</Button>
                <Button variant="contained" onClick={() => setChangeNameDialogOpen(true)} sx={styles.button}>Change rework settings</Button>
                <Button variant="contained" onClick={openUpdateReworkWizard} sx={styles.button} disabled={totalPPReworks.includes(algorithm?.code ?? "")}>Update rework</Button>
            </StyledOptionsBar>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>User ID</TableCell>
                        <TableCell>Username</TableCell>
                        <TableCell>Role</TableCell>
                        <TableCell />
                    </TableRow>
                </TableHead>
                <TableBody>
                    {[...reworkUsers.hyperAdmins, ...reworkUsers.users].map(u => (
                        <TableRow key={u.user_id}>
                            <TableCell>{u.user_id}</TableCell>
                            <TableCell>{u.username ?? "???"}</TableCell>
                            <TableCell>{"role" in u ? u.role : "Hyperadmin"}</TableCell>
                            <TableCell>
                                {"role" in u && <Button variant="contained" onClick={() => removeUser(u.user_id)} disabled={!oauth.is_hyper_admin && u.rework_permission_id === 1}>Remove</Button>}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>

            <AdminChangeReworkSettingsDialog open={changeNameDialogOpen} onClose={() => setChangeNameDialogOpen(false)} />

            <AdminAddUserDialog open={dialogOpen} onClose={handleCloseDialog} />
            <UpdateReworkWizard open={wizardOpen} onClose={handleCloseWizard} />
            <ForbiddenSnackbar open={showForbiddenSnackbar} onClose={setShowForbiddenSnackbar} text={errorText} />
        </>
    );
};

export default ReworkAdmin;