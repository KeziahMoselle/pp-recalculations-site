import React, { useContext, useEffect, useState } from "react";
import { List, ListItem, ListItemText, Paper, Divider, Typography } from "@mui/material";
import LoadingIcon from "../../common/components/LoadingIcon/LoadingIcon";
import RequestRecalculation from "../../common/components/RequestRecalculation/RequestRecalculation";
import { AlgorithmContext } from "../../common/context";
import { apiGetRequest, SxStyles } from "../../helpers";
import { QueueResponse } from "common/interfaces/interfaces";
import moment from "moment";

interface IProps { };

const sx: SxStyles = {
    root: {
        mt: 1
    },
    list: {
        pt: 0,
        pb: 0
    },
    listItem: {
        height: 40,
        width: 300
    },
    listItemHeader: {
        mb: 1
    }
};

const Queue: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);
    const [queueResponse, setQueue] = useState<QueueResponse>();
    const [queueTime, setQueueTime] = useState<number>();

    useEffect(() => {
        const fetchData = async () => {
            const { response: queueResponse } = await apiGetRequest<QueueResponse>(`queue/list?rework=${algorithm?.id}`);
            const { response: queueTimeResponse } = await apiGetRequest<number>(`queue/waiting-time`);
            setQueue(queueResponse ?? []);
            setQueueTime(queueTimeResponse);
        };

        if (algorithm)
            fetchData();
    }, [algorithm]);

    const queue = queueResponse?.queue;
    if (!queue) return <LoadingIcon />;

    const onQueueUpdate = (newQueue: QueueResponse) => {
        setQueue(newQueue);
    };

    const activeRequestCount = queue.filter(q => q.requested_by_you).length;
    // const waitingTime = queueResponse?.oldestTime ? moment.utc().diff(moment.utc(queueResponse.oldestTime), "minutes") + 1 : 1;
    // const waitingTime = Math.ceil((queue.length * 45 + 45) / 60 / 6);

    return (
        <>
            <RequestRecalculation onQueueUpdate={onQueueUpdate} activeRequestCount={activeRequestCount} />
            {queueTime && <Typography align="left" sx={{ ml: 1 }} variant="body2">Estimated waiting time: {queueTime} minutes</Typography>}
            <Paper sx={sx.root}>
                <List sx={sx.list}>
                    {!queue.length && (
                        <ListItem>
                            <ListItemText primary="No users currently in queue" />
                        </ListItem>
                    )}
                    {queue.length > 0 && (
                        <ListItem sx={sx.listItemHeader}>
                            <ListItemText primary="Current users in queue:" />
                        </ListItem>
                    )}
                    {queue.map(user => {
                        const rowText = user.name ? user.name : user.user_id.toString();
                        const rowTextTime = `Added on ${moment.utc(user.date_added).format("HH:mm")} UTC`;

                        return (
                            <React.Fragment key={user.user_id}>
                                <ListItem sx={sx.listItem} key={user.user_id}>
                                    <ListItemText primary={rowText} />
                                    <Typography variant="caption">{rowTextTime}</Typography>
                                </ListItem>
                                <Divider />
                            </React.Fragment>
                        );
                    })}
                </List>
            </Paper>
        </>
    );
};

export default Queue;