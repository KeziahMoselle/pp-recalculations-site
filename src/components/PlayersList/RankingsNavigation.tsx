import { AlgorithmContext } from "common/context";
import { isDefined } from "helpers";
import React, { useContext, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import LinkTabs from "../../common/components/LinkTabs/LinkTabs";

interface IProps {};

interface NavigationItem {
    id: string,
    label: string
};

const RankingsNavigation: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);
    const { oauth } = useSelector((state: IApplicationState) => state);
    const [navItems, setNavItems] = useState<NavigationItem[]>([]);

    useEffect(() => {
        const defaultNavItems = [
            { id: "info", label: "Info" },
            { id: "players", label: "Player rankings" },
            { id: "topscores", label: "Score rankings" },
            { id: "queue", label: "Queue" },
            { id: "score-calculator", label: "Score calculator" },
        ];

        const isAdmin = oauth?.is_hyper_admin || !!oauth?.rework_permissions?.find(p => p.rework_id === algorithm?.id && p.permission === 1);
        setNavItems([
            ...defaultNavItems,
            algorithm?.code !== "live" ? { id: "data", label: "Data" } : null,
            isAdmin ? { id: "admin", label: "Admin" } : null
        ].filter(isDefined));
    }, [oauth, algorithm]);

    return (
        <LinkTabs parentRoute={`/rankings`} tabs={navItems} defaultValue={window.location.href.split("/").pop() || "rankings/players"} />
    );
};

export default RankingsNavigation;