import React, { useState, useEffect, useContext, useCallback } from 'react';
import { useTheme, useMediaQuery, Typography, Button } from '@mui/material';
import { styled } from "@mui/material/styles";
import BaseTable from '../../common/components/BaseTable/BaseTable';
import BaseTableFilters from '../../common/components/BaseTableFilters/BaseTableFilters';
import StyledName from '../../common/components/StyledName/StyledName';
import { AlgorithmContext } from '../../common/context';
import { useHistory } from 'react-router';
import { apiGetRequest, isDefined, roundValue, totalPPReworks } from '../../helpers';
import localforage from 'localforage';
import { IApplicationState } from 'store';
import { useSelector } from "react-redux";
import { isArray } from 'lodash';
import ForbiddenError from 'common/components/Errors/ForbiddenError';
import PPDifferenceIndicator from 'common/components/PPDifferenceIndicator/PPDifferenceIndicator';

interface IProps { };

interface RankingsParams {
    rework?: number,
    countryFilter?: string,
    sortingField?: string,
    sortOrder: "asc" | "desc",
    searchValue: string,
    onlyUpdatedPlayers: boolean,
    hideUnrankedPlayers: boolean
};

const StyledPlayersListContainer = styled("div")(({ theme }) => ({
    backgroundColor: theme.palette.background.default
}));

const PlayersList: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);
    const [roundValues, setRoundValues] = useState<boolean>(true);

    const [players, setPlayers] = useState<any>();
    const [version, setVersion] = useState<{ algorithm_version: number }>();
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [showForbiddenError, setShowForbiddenError] = useState<boolean>(false);

    const [rankingsParams, setRankingsParams] = useState<RankingsParams>({
        rework: algorithm?.id,
        countryFilter: undefined,
        sortingField: undefined,
        sortOrder: "desc",
        searchValue: "",
        onlyUpdatedPlayers: false,
        hideUnrankedPlayers: true
    });

    const [parsedRows, setParsedRows] = useState<any>();

    const theme = useTheme();
    const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));
    const history = useHistory();
    const { oauth, countries } = useSelector((state: IApplicationState) => state);

    const handleRounding = useCallback((value: number) => {
        return roundValues ? Math.round(value) : roundValue(value, 1);
    }, [roundValues]);

    useEffect(() => {
        const fetchData = async () => {
            setVersion({ algorithm_version: algorithm?.algorithm_version ?? -1 });
        };

        if (algorithm?.code)
            fetchData();
    }, [algorithm]);

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            const { rework, countryFilter, searchValue, sortingField, sortOrder, onlyUpdatedPlayers, hideUnrankedPlayers } = rankingsParams;
            const { response, status } = await apiGetRequest<any>(`rankings/players/${rework}?country=${countryFilter}&search=${searchValue}&sort=${sortingField}&order=${sortOrder}&onlyUpToDate=${onlyUpdatedPlayers}&hideUnranked=${hideUnrankedPlayers}`, undefined, oauth.token);
            if (status === 403 || !isArray(response)) {
                setShowForbiddenError(true);
                setPlayers([]);
            }
            else
                setPlayers(response);
            setIsLoading(false);
        };

        if (oauth?.user?.id && rankingsParams.rework && rankingsParams.countryFilter !== undefined && rankingsParams.sortingField)
            fetchData();
    }, [rankingsParams, oauth]);

    useEffect(() => {
        const getSavedCountryFilter = async () => {
            const savedCountry = await localforage.getItem<string>("country");
            setRankingsParams(old => ({
                ...old,
                countryFilter: savedCountry?.length ? savedCountry : ""
            }));
        };

        getSavedCountryFilter();
    }, []);

    useEffect(() => {
        if (algorithm) {
            setRankingsParams(old => ({
                ...old,
                rework: algorithm.id,
                sortingField: algorithm.id === 1 ? "old_pp" : "new_pp_incl_bonus"
            }));
        }
    }, [algorithm]);

    useEffect(() => {
        if (players && version) {
            const newRows = players.map((row: any, index: number) => {
                return {
                    key: index,
                    values: {
                        ...row,
                        raw_name: row.name,
                        name: <StyledName name={version.algorithm_version > row.pp_version ? `${row.name} (outdated)` : row.name} country={row.country || ""} />,
                        rank: (rankingsParams.countryFilter?.length ? row.country_rank_real : row.global_rank_real) || "-",
                        rank_sort_field: (rankingsParams.countryFilter?.length ? row.country_rank_sort_field : row.global_rank_sort_field) || "-",
                        rank_global: row.global_rank_real || "-",
                        pp_change: <PPDifferenceIndicator value={row.pp_change} />,
                        old_pp: handleRounding(row.old_pp),
                        new_pp_excl_bonus: handleRounding(row.new_pp_excl_bonus),
                        new_pp_incl_bonus: handleRounding(row.new_pp_incl_bonus),
                        weighted_aim_pp: handleRounding(row.weighted_aim_pp) ?? 0,
                        weighted_tap_pp: handleRounding(row.weighted_tap_pp) ?? 0,
                        weighted_acc_pp: handleRounding(row.weighted_acc_pp) ?? 0,
                        weighted_fl_pp: handleRounding(row.weighted_fl_pp) ?? 0,
                        weighted_visual_pp: handleRounding(row.weighted_visual_pp) ?? 0,
                        weighted_cognition_pp: handleRounding(row.weighted_cognition_pp) ?? 0,
                        weighted_reading_pp: handleRounding(row.weighted_reading_pp) ?? 0,
                        bonus_pp: row.bonus_pp ?? 0
                    },
                    isOutdated: version.algorithm_version > row.pp_version
                }
            });

            setParsedRows(newRows);
        }
    }, [players, version, rankingsParams.countryFilter, handleRounding, roundValues]);

    if (!version) return <Typography color="white">Loading current version...</Typography>;
    if (!players) return <Typography color="white">Loading players...</Typography>;

    const getHeaders = (): ({ key: string, value: string, isBoldColumn?: boolean, sortAllowed?: boolean, tooltip?: string } | null)[] => {
        const { countryFilter } = rankingsParams;
        if (algorithm?.code === "live")
            return [
                { key: "rank", value: "Rank" },
                countryFilter?.length && !isSmallScreen ? { key: "rank_global", value: "Global rank" } : null,
                { key: "name", value: isSmallScreen ? "Name" : "Player name" },
                { key: "old_pp", value: isSmallScreen ? "PP" : "Weighted pp", sortAllowed: true, tooltip: "The player's current total pp" },
                { key: "weighted_aim_pp", value: isSmallScreen ? "Aim" : "Aim pp", sortAllowed: true, tooltip: "Aim pp is calculated in the same way as total pp, but only uses the aim pp of the player's scores." },
                { key: "weighted_tap_pp", value: isSmallScreen ? "Tap" : "Tap pp", sortAllowed: true, tooltip: "Tap pp is calculated in the same way as total pp, but only uses the tap pp of the player's scores." },
                { key: "weighted_acc_pp", value: isSmallScreen ? "Acc" : "Acc pp", sortAllowed: true, tooltip: "Accuracy pp is calculated in the same way as total pp, but only uses the acc pp of the player's scores." },
                { key: "weighted_fl_pp", value: isSmallScreen ? "FL" : "FL pp", sortAllowed: true, tooltip: "Flashlight pp is calculated in the same way as total pp, but only uses the flashlight pp of the player's scores." },
                { key: "bonus_pp", value: isSmallScreen ? "Bonus" : "Bonus pp", sortAllowed: true, tooltip: "Bonus pp is an additional amount of pp based on how many unique scores the player has." }
            ];
        else
            return [
                { key: "rank", value: "Rank" },
                countryFilter?.length && !isSmallScreen ? { key: "rank_global", value: "Global rank" } : null,
                { key: "name", value: isSmallScreen ? "Name" : "Player name" },
                !algorithm?.compare_with_master ? { key: "old_pp", value: isSmallScreen ? "Old" : "Old pp", sortAllowed: true, tooltip: "The player's current total pp" } : null,
                algorithm?.compare_with_master ? { key: "old_pp", value: isSmallScreen ? "Master" : "Master pp", tooltip: "Master pp is the player's estimated total pp according to the master branch, which contains new, confirmed changes that have not been deployed to live yet." } : null,
                { key: "new_pp_incl_bonus", value: isSmallScreen ? "New" : "New pp", isBoldColumn: true, sortAllowed: true, tooltip: "This is an estimate of the player's new total pp." },
                { key: "pp_change", value: "Difference", sortAllowed: !algorithm?.compare_with_master },
                algorithm?.parsed_pp_skills.includes("aim") ? { key: "weighted_aim_pp", value: isSmallScreen ? "Aim" : "Aim pp", sortAllowed: true, tooltip: "Aim pp is calculated in the same way as total pp, but only uses the aim pp of the player's scores." } : null,
                algorithm?.parsed_pp_skills.includes("tap") ? { key: "weighted_tap_pp", value: isSmallScreen ? "Tap" : "Tap pp", sortAllowed: true, tooltip: "Tap pp is calculated in the same way as total pp, but only uses the tap pp of the player's scores." } : null,
                algorithm?.parsed_pp_skills.includes("accuracy") ? { key: "weighted_acc_pp", value: isSmallScreen ? "Acc" : "Acc pp", sortAllowed: true, tooltip: "Accuracy pp is calculated in the same way as total pp, but only uses the acc pp of the player's scores." } : null,
                algorithm?.parsed_pp_skills.includes("flashlight") ? { key: "weighted_fl_pp", value: isSmallScreen ? "FL" : "FL pp", sortAllowed: true, tooltip: "Flashlight pp is calculated in the same way as total pp, but only uses the flashlight pp of the player's scores." } : null,
                algorithm?.parsed_pp_skills.includes("visual") ? { key: "weighted_visual_pp", value: isSmallScreen ? "Visual" : "Visual pp", sortAllowed: true, tooltip: "Visual pp is calculated in the same way as total pp, but only uses the visual pp of the player's scores." } : null,
                algorithm?.parsed_pp_skills.includes("cognition") ? { key: "weighted_cognition_pp", value: isSmallScreen ? "Cognition" : "Cognition pp", sortAllowed: true, tooltip: "Cognition pp is calculated in the same way as total pp, but only uses the cognition pp of the player's scores." } : null,
                algorithm?.parsed_pp_skills.includes("reading") ? { key: "weighted_reading_pp", value: isSmallScreen ? "Reading" : "Reading pp", sortAllowed: true, tooltip: "Reading pp is calculated in the same way as total pp, but only uses the reading pp of the player's scores." } : null,
                !totalPPReworks.includes(algorithm?.code ?? "") ? { key: "bonus_pp", value: isSmallScreen ? "Bonus" : "Bonus pp", sortAllowed: true, tooltip: "Bonus pp is an additional amount of pp based on how many unique scores the player has." } : null,
                !isSmallScreen ? { key: "new_pp_excl_bonus", value: totalPPReworks.includes(algorithm?.code ?? "") ? "Weighted pp" : "New pp (excl. bonus)", sortAllowed: true, tooltip: totalPPReworks.includes(algorithm?.code ?? "") ? "This is the player's weighted pp" : "This is the player's new total pp without bonus pp applied." } : null,
            ];
    };

    const headers = getHeaders().filter(isDefined);

    const handleSearch = (newSearchValue: string, newCountryFilter: string, newRoundValues: boolean, newOnlyUpdatedPlayers: boolean, newHideUnrankedPlayers: boolean) => {
        if (newRoundValues === roundValues) {
            localforage.setItem("country", newCountryFilter);
            setRankingsParams(old => ({
                ...old,
                searchValue: newSearchValue,
                countryFilter: newCountryFilter,
                onlyUpdatedPlayers: newOnlyUpdatedPlayers,
                hideUnrankedPlayers: newHideUnrankedPlayers
            }));
        }
        else setRoundValues(newRoundValues);
    };

    const handleRowClick = (row: any, mouseButton?: number) => {
        if (mouseButton === 1)
            window.open(`/player/${row.user_id}/${algorithm?.code}`);
        else if (mouseButton === 2)
            return;
        else
            history.push(`/player/${row.user_id}/${algorithm?.code}`);
    };

    const handleSortChange = (newSortingField: string) => {
        setRankingsParams(old => ({
            ...old,
            sortingField: newSortingField,
            sortOrder: newSortingField === old.sortingField
                ? old.sortOrder === "asc" ? "desc" : "asc"
                : old.sortOrder
        }));
    };

    const navigateToQueue = () => {
        history.push(`/rankings/queue/${algorithm?.code}`);
    };

    if (showForbiddenError) {
        return (
            <StyledPlayersListContainer>
                <ForbiddenError />
            </StyledPlayersListContainer>
        );
    }

    const contextItems = [
        { label: "Open player in new tab", onClick: (row: any) => handleRowClick(row, 1) }
    ];

    return (
        <StyledPlayersListContainer>
            {rankingsParams.countryFilter !== undefined && <BaseTableFilters update={handleSearch} countries={countries} searchDelay={500} initialCountry={rankingsParams.countryFilter} noShowModStarRatingsOption />}
            <BaseTable
                headers={headers}
                rows={parsedRows}
                handleClick={handleRowClick}
                defaultSort="desc"
                defaultSortField={rankingsParams.rework === 1 ? "old_pp" : "new_pp_incl_bonus"}
                sortingHandler={handleSortChange}
                sortBy={rankingsParams.sortingField}
                sortOrder={rankingsParams.sortOrder}
                loading={isLoading}
                containerStyle={{ maxHeight: isSmallScreen ? "100%" : "calc(100vh - 160px)" }}
                contextMenuItems={contextItems}
            />
            {parsedRows?.length < 50 && (
                <>
                    <Typography variant="h6" color="primary" sx={{ mt: 3 }}>Didn't find who you're looking for?</Typography>
                    <Button variant="outlined" color="primary" sx={{ mt: 1 }} onClick={navigateToQueue}>Request a recalculation</Button>
                </>
            )}
        </StyledPlayersListContainer>
    );
};

export default PlayersList;