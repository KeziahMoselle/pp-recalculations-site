import React from "react";
import { SxStyles } from "helpers";
import { Divider, FormControl, InputLabel, List, ListItem, ListItemText, MenuItem, Select, SelectChangeEvent, Switch, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { IApplicationState } from "store";
import { SettingsActions } from "store/settings";
import { Settings } from "common/interfaces/interfaces";
import ToolBar from "common/components/ToolBar/ToolBar";

interface IProps { };

const sx: SxStyles = {
    header: {
        mt: 2,
        mb: 1,
        ml: 2
    },
    divider: {
        width: "98%"
    },
    list: {
        mt: 2,
        width: {
            xs: "100%",
            md: "75%",
            lg: "50%"
        }
    },
    formControl: {
        width: {
            xs: "50%",
            md: 300
        }
    }
};

const SettingsComponent: React.FC<IProps> = (props) => {
    const { settings } = useSelector((state: IApplicationState) => state);
    const dispatch = useDispatch();

    const dateFormats = [{ code: "ymd", name: "YYYY-MM-DD" }, { code: "dmy", name: "DD-MM-YYYY" }, { code: "mdy", name: "MM-DD-YYYY (the wrong one)" }];

    const updateSettings = (newSettings: Settings) => {
        dispatch(SettingsActions.updateSettings(newSettings));
    };

    const settingSelectChanged = (field: keyof Settings, event: SelectChangeEvent<string | null>) => {
        updateSettings({ ...settings, [field]: event.target.value });
    };

    const settingToggleChanged = (field: keyof Settings) => {
        updateSettings({ ...settings, [field]: !settings[field] });
    };

    return (
        <>
            <ToolBar longTitle="Settings" shortTitle="Settings" />
            <Typography variant="body1" align="left" sx={sx.header}>Settings are saved in the local storage of your browser</Typography>
            <div style={{ display: "flex", justifyContent: "center" }}>
                <Divider sx={sx.divider} />
            </div>
            <List sx={sx.list}>
                <ListItem>
                    <ListItemText primary="Datetime format" color="white" />
                    <FormControl sx={sx.formControl}>
                        <InputLabel>Format</InputLabel>
                        <Select label="Format" value={settings.dateTimeFormat} onChange={event => settingSelectChanged("dateTimeFormat", event)}>
                            {dateFormats.map(format => (
                                <MenuItem key={format.code} value={format.code}>{format.name}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </ListItem>
                <ListItem>
                    <ListItemText primary="Use gradient color for pp difference indicator" />
                    <FormControl sx={sx.formControl}>
                        <Switch checked={settings.useGradientForPPDifference} onChange={() => settingToggleChanged("useGradientForPPDifference")} color="primary" />
                    </FormControl>
                </ListItem>
                <ListItem>
                    <ListItemText primary="Highlight new top 100 scores in green" />
                    <FormControl sx={sx.formControl}>
                        <Switch checked={settings.highlightNewTop100Scores} onChange={() => settingToggleChanged("highlightNewTop100Scores")} color="primary" />
                    </FormControl>
                </ListItem>
            </List>
        </>
    );
};

export default SettingsComponent;