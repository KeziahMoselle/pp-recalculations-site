import React, { useContext, useEffect, useState } from "react";
import { useTheme, useMediaQuery, Typography } from "@mui/material";
import BaseTable from "../../common/components/BaseTable/BaseTable";
import StyledName from "../../common/components/StyledName/StyledName";
import { AlgorithmContext } from "../../common/context";
import moment from "moment";
import { apiGetRequest, convertDateFormat, enforceModOrder, getGamemodeFromNumber, isDefined, roundValue } from "../../helpers";
import { isArray } from "lodash";
import { IApplicationState } from "store";
import { useSelector } from "react-redux";
import ForbiddenError from "common/components/Errors/ForbiddenError";
import PPDifferenceIndicator from "common/components/PPDifferenceIndicator/PPDifferenceIndicator";

interface IProps { };

interface TopScoresParams {
    rework?: number,
    sortingField?: string,
    sortOrder: "asc" | "desc"
};

const TopScores: React.FC<IProps> = (props) => {
    const { algorithm } = useContext(AlgorithmContext);
    const theme = useTheme();
    const { oauth, settings } = useSelector((state: IApplicationState) => state);
    const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));
    const [topScoresParams, setTopScoresParams] = useState<TopScoresParams>({
        rework: undefined,
        sortingField: undefined,
        sortOrder: "desc"
    });

    const [topScores, setTopScores] = useState<any>();
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [showForbiddenError, setShowForbiddenError] = useState<boolean>(false);

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            const { rework, sortingField, sortOrder } = topScoresParams;
            const { response, status } = await apiGetRequest<any>(`rankings/topscores/${rework}?sort=${sortingField}&order=${sortOrder}`, undefined, oauth.token);
            if (status === 403 || !isArray(response)) {
                setShowForbiddenError(true);
                setTopScores([]);
            }
            else setTopScores(response);
            setIsLoading(false);
        };

        if (oauth?.user?.id && topScoresParams.rework && topScoresParams.sortingField)
            fetchData();
    }, [oauth, topScoresParams]);

    useEffect(() => {
        if (algorithm) {
            setTopScoresParams(old => ({
                ...old,
                rework: algorithm.id,
                sortingField: algorithm.id === 1 ? "live_pp" : "local_pp"
            }));
        }
    }, [algorithm]);

    if (!topScores) return <Typography color="white">Loading scores...</Typography>;

    const getHeaders = (): { key: string, value: string, isBoldColumn?: boolean, maxWidth?: string, sortAllowed?: boolean, tooltip?: string }[] => {
        if (algorithm?.code === "live")
            return [
                { key: "rank", value: "Rank" },
                { key: "username", value: "Player" },
                { key: "map_name", value: "Map" },
                { key: "live_pp", value: "Live", isBoldColumn: true, sortAllowed: true, tooltip: "The current pp value for this score" },
                { key: "aim_pp", value: "Aim", sortAllowed: true, tooltip: "Aim is one of the skills used to calculate total pp. The aim pp values of each score are listed here." },
                { key: "tap_pp", value: "Tap", sortAllowed: true, tooltip: "Tapping is one of the skills used to calculate total pp. The tap pp values of each score are listed here." },
                { key: "acc_pp", value: "Acc", sortAllowed: true, tooltip: "Accuracy is one of the skills used to calculate total pp. The accuracy pp values of each score are listed here." },
                { key: "fl_pp", value: "FL", sortAllowed: true, tooltip: "Flashlight is one of the skills used to calculate total pp. The flashlight pp values of each score are listed here." },
                { key: "mods", value: "Mods" },
                { key: "accuracy", value: "Accuracy" },
                { key: "max_combo", value: "Combo" },
                { key: "good", value: "100", maxWidth: "1%" },
                { key: "meh", value: "50", maxWidth: "1%" },
                { key: "miss", value: "Miss", maxWidth: "1%" },
                { key: "score_date", value: "Date" },
            ];
        else
            return [
                { key: "rank", value: "Rank" },
                { key: "username", value: "Player" },
                { key: "map_name", value: "Map", maxWidth: "40%" },
                !algorithm?.compare_with_master ? { key: "live_pp", value: isSmallScreen ? "Live" : "Live", isBoldColumn: true, sortAllowed: true, tooltip: "The current pp value for this score" } : null,
                algorithm?.compare_with_master ? { key: "master_pp", value: "Master", isBoldColumn: true, tooltip: "The pp value for this score according to the master branch. The master branch contains new, confirmed changes that have not been deployed to live yet." } : null,
                { key: "local_pp", value: isSmallScreen ? "Local" : "Local", isBoldColumn: true, sortAllowed: true, tooltip: "The new pp value for this score according to this rework." },
                { key: "pp_diff", value: "Diff.", sortAllowed: !algorithm?.compare_with_master },
                algorithm?.parsed_pp_skills.includes("aim") ? { key: "aim_pp", value: isSmallScreen ? "Aim" : "Aim", sortAllowed: true, tooltip: "Aim is one of the skills used to calculate total pp. The aim pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("tap") ? { key: "tap_pp", value: isSmallScreen ? "Tap" : "Tap", sortAllowed: true, tooltip: "Tapping is one of the skills used to calculate total pp. The tap pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("accuracy") ? { key: "acc_pp", value: isSmallScreen ? "Acc" : "Acc", sortAllowed: true, tooltip: "Accuracy is one of the skills used to calculate total pp. The accuracy pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("flashlight") ? { key: "fl_pp", value: "FL", sortAllowed: true, tooltip: "Flashlight is one of the skills used to calculate total pp. The flashlight pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("visual") ? { key: "visual_pp", value: "Visual", sortAllowed: true, tooltip: "Vision is one of the skills used to calculate total pp. The visual pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("cognition") ? { key: "cognition_pp", value: "Cognition", sortAllowed: true, tooltip: "Cognition is one of the skills used to calculate total pp. The cognition pp values of each score are listed here." } : null,
                algorithm?.parsed_pp_skills.includes("reading") ? { key: "reading_pp", value: "Reading", sortAllowed: true, tooltip: "Reading is one of the skills used to calculate total pp. The reading pp values of each score are listed here." } : null,
                { key: "mods", value: "Mods" },
                { key: "accuracy", value: "Accuracy" },
                { key: "max_combo", value: isSmallScreen ? "Combo" : "Combo" },
                algorithm?.gamemode === 3 ? { key: "perfect", value: "Epic", maxWidth: "1%" } : null,
                algorithm?.gamemode === 3 ? { key: "great", value: "300", maxWidth: "1%" } : null,
                algorithm?.gamemode === 3 ? { key: "good", value: "200", maxWidth: "1%" } : null,
                algorithm?.gamemode === 3 ? { key: "ok", value: "100", maxWidth: "1%" } : null,
                algorithm?.gamemode === 3 ? { key: "meh", value: "50", maxWidth: "1%" } : null,
                algorithm?.gamemode === 3 ? { key: "miss", value: "0", maxWidth: "1%" } : null,
                algorithm?.gamemode !== 3 ? { key: "hit_counts", value: "100/50/0", maxWidth: "1%" } : null,
                { key: "score_date", value: "Date" },
            ].filter(isDefined);
    };

    const headers = getHeaders();

    const spliceIndex = headers.findIndex(h => h.key === "max_combo");
    if (isSmallScreen && spliceIndex >= 0) headers.splice(spliceIndex, 4);

    const rows = topScores?.map((row: any, index: number) => {
        const difference = +(isDefined(row.pp_diff) ? +row.pp_diff : +row.local_pp - +row.live_pp).toFixed(3);
        const isOutdated = row.version !== algorithm?.algorithm_version;
        return {
            key: index,
            values: {
                ...row,
                rank: index + 1,
                username: <StyledName name={row.username} country={row.country || ""} />,
                local_pp: roundValue(row.local_pp ?? 0, 1),
                live_pp: roundValue(row.live_pp, 1),
                master_pp: roundValue(row.master_pp, 1),
                pp_diff: { value: difference || 0, element: <PPDifferenceIndicator value={difference} /> },
                accuracy: roundValue(row.accuracy, 2),
                mods: enforceModOrder(row.mods.replace(/, /g, '')),
                score_date: moment.utc(row.score_date).format(convertDateFormat(settings.dateTimeFormat)),
                aim_pp: roundValue(row.aim_pp, 1),
                tap_pp: roundValue(row.tap_pp, 1),
                acc_pp: roundValue(row.acc_pp, 1),
                fl_pp: roundValue(row.fl_pp ?? 0, 1),
                cognition_pp: roundValue(row.cognition_pp ?? 0, 1),
                visual_pp: roundValue(row.visual_pp ?? 0, 1),
                hit_counts: `${row.good}/${row.meh}/${row.miss}`,
                map_name: `${row.artist} - ${row.title} (${row.creator_name}) [${row.diff_name}]${isOutdated ? " (OUTDATED)" : ""}`
            },
            isOutdated: row.version !== algorithm?.algorithm_version
        };
    });

    const handleRowClick = (row: any, mouseButton?: number) => {
        const url = `https://osu.ppy.sh/scores/${getGamemodeFromNumber(algorithm?.gamemode ?? 0)}/${row.score_id}`;
        if (mouseButton === 2) return;
        else window.open(url);
    };

    const navigateToPlayer = (row: any) => {
        window.open(`/player/${row.user_id}/${algorithm?.code}`);
    };

    const navigateToScore = (row: any) => {
        window.open(`https://osu.ppy.sh/scores/${getGamemodeFromNumber(algorithm?.gamemode ?? 0)}/${row.score_id}`);
    };

    const handleSortChange = (newSortingField: string) => {
        setTopScoresParams(old => ({
            ...old,
            sortingField: newSortingField,
            sortOrder: newSortingField === old.sortingField
                ? old.sortOrder === "asc" ? "desc" : "asc"
                : old.sortOrder
        }));
    };

    if (showForbiddenError) return <ForbiddenError />;

    const contextItems = [
        { label: "Open player in new tab", onClick: navigateToPlayer },
        { label: "Open score in new tab", onClick: navigateToScore }
    ];

    return (
        <BaseTable
            headers={headers}
            rows={rows}
            handleClick={handleRowClick}
            defaultSort="desc"
            defaultSortField={topScoresParams.rework === 1 ? "live_pp" : "local_pp"}
            sortingHandler={handleSortChange}
            sortBy={topScoresParams.sortingField}
            sortOrder={topScoresParams.sortOrder}
            loading={isLoading}
            containerStyle={{ maxHeight: isSmallScreen ? "100%" : "calc(100vh - 112px)" }}
            contextMenuItems={contextItems}
        />
    );
};

export default TopScores;