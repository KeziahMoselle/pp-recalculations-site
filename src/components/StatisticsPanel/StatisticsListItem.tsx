import React from 'react';
import { ListItem, Typography } from '@mui/material';
import { SxStyles } from '../../helpers';

interface IProps {
    label: string;
    value: string;
    addition?: string;
}

const sx: SxStyles = {
    label: {
        fontSize: "16px",
        color: "grey"
    },
    value: {
        ml: 1,
        fontSize: "16px",
        color: "text.primary"
    }
};

const StatisticsListItem: React.FC<IProps> = (props) => {
    const { label, value, addition } = props;

    return (
        <ListItem>
            <Typography sx={sx.label}>{label}:</Typography>
            <Typography sx={sx.value}>{value} {!!addition && `(${addition})`}</Typography>
        </ListItem>
    );
};

export default StatisticsListItem;