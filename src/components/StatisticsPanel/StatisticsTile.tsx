import React from 'react';
import { Grid, Tooltip, Typography } from '@mui/material';
import { SxStyles } from '../../helpers';

interface IProps {
    label: string;
    value: string | number;
    hover?: string;
};

const sx: SxStyles = {
    root: {
        mb: 1
    },
    label: {
        fontSize: "12px",
        color: "grey"
    },
    value: {
        fontSize: "20px"
    }
};

const StatisticsTile: React.FC<IProps> = (props) => {
    const { hover } = props;

    if (hover) {
        return (
            <Tooltip title={<span style={{ fontSize: "14px" }}>{hover}</span>}>
                <div><StatisticsTileContent {...props} /></div>
            </Tooltip>
        );
    }

    return (<StatisticsTileContent {...props} />);
};

const StatisticsTileContent: React.FC<IProps> = (props) => {
    const { label, value } = props;

    return (
        <Grid container spacing={0} sx={sx.root}>
            <Grid item xs={12} sx={sx.value}>
                <Typography sx={sx.value}>{typeof value === "number" ? value.toLocaleString() : value}</Typography>
            </Grid>
            <Grid item xs={12} sx={sx.label}>
                <Typography sx={sx.label}>{label}</Typography>
            </Grid>
        </Grid>
    );
};

export default StatisticsTile;