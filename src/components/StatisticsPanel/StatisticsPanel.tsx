import React, { useState } from 'react';
import { Paper, Grid, List, ListItem, Collapse } from '@mui/material';
import StatisticsTile from './StatisticsTile';
import StatisticsListItem from './StatisticsListItem';
import { ExpandMore, ExpandLess } from '@mui/icons-material';
import NoMobile from '../../common/components/MobileHelpers/NoMobile';
import MobileOnly from '../../common/components/MobileHelpers/MobileOnly';
import { SxStyles } from '../../helpers';

type breakpointNumber = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;

interface IProps {
    mainLabel: string | any;
    tiles: Array<{ label: string, value: string, hover?: string } | any>;
    xs: breakpointNumber;
    sm: breakpointNumber;
    md: breakpointNumber;
    lg?: breakpointNumber;
    xl?: breakpointNumber;
};

const sx: SxStyles = {
    root: {
        width: "100%",
        height: "90px"
    },
    grid: {
        width: "100%",
        height: "90px",
        display: "flex",
        alignItems: "center"
    },
    mainLabel: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "70%",
        fontSize: "20px",
        borderRight: "1px solid lightgrey"
    },
    mainLabelMobile: {
        height: "70px",
        fontSize: "18px",
        color: "text.primary"
    },
    list: {
        bgcolor: "background.paper"
    }
};

const StatisticsPanel: React.FC<IProps> = (props) => {
    const { mainLabel, tiles, xs, sm, md, lg, xl } = props;
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(!open);
    };

    return (
        <>
            <NoMobile>
                <Paper sx={sx.root} elevation={3}>
                    <Grid container sx={sx.grid}>
                        <Grid item xs={3} md={2} sx={sx.mainLabel}>
                            {mainLabel}
                        </Grid>
                        {tiles.map((tile, index) => (
                            <Grid key={index} xs={xs} sm={sm} md={md} lg={lg} xl={xl} item>
                                {!!tile.label && <StatisticsTile label={tile.label} value={tile.value} hover={tile.hover} />}
                            </Grid>
                        ))}
                    </Grid>
                </Paper>
            </NoMobile>
            <MobileOnly>
                <List component="div" sx={sx.list}>
                    <ListItem button sx={sx.mainLabelMobile} onClick={handleClick}>
                        {mainLabel} {open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open} unmountOnExit>
                        <List>
                            {tiles.map((tile, index) => (
                                !!tile.label && <StatisticsListItem key={index} label={tile.label} value={tile.value} addition={tile.hover} />
                            ))}
                        </List>
                    </Collapse>
                </List>
            </MobileOnly>
        </>
    );
};

export default StatisticsPanel;