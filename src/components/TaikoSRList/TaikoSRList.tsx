import React, { useEffect, useState } from "react";
import { Paper, useMediaQuery, useTheme, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import { useFetch } from "../../common/hooks/useAPICall";
import BaseTableFilters from "../../common/components/BaseTableFilters/BaseTableFilters";
import BaseTable from "../../common/components/BaseTable/BaseTable";
import ToolBar from "../../common/components/ToolBar/ToolBar";
import { isDefined, SxStyles } from "../../helpers";
import Link from "../../common/components/Link/Link";

interface IProps { };

const StyledTaikoSRList = styled("div")(({ theme }) => ({
    backgroundColor: theme.palette.background.default
}));

const sx: SxStyles = {
    header: {
        height: {
            xs: 300,
            md: 120
        },
        textAlign: "left",
        padding: "10px",
        fontWeight: "bold",
        fontSize: "14px",
        mb: 1
    }
};

const TaikoSRList: React.FC<IProps> = (props) => {
    const isSmallScreen = useMediaQuery(useTheme().breakpoints.down("md"));
    const [searchValue, setSearchValue] = useState<string>("");
    const [showModStarRatings, setShowModStarRatings] = useState<boolean>(false);
    const [sortingField, setSortingField] = useState<string>("sr_diff");
    const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");

    const { response, loading } = useFetch<any>(`rankings/taiko-sr-list?search=${searchValue}&includeMods=${showModStarRatings}&sort=${sortingField}&order=${sortOrder}`);

    const [parsedRows, setParsedRows] = useState<any>();

    useEffect(() => {
        if (response) {
            const newRows = response.map((row: any, index: number) => {
                return {
                    key: index,
                    values: {
                        ...row,
                        map_name: `${row.artist} - ${row.title} [${row.diff_name}]`
                    }
                };
            });

            setParsedRows(newRows);
        };
    }, [response]);

    if (loading && !response) return <Typography>Loading data...</Typography>;

    const headers: { key: string, value: string, sortAllowed?: boolean }[] = [
        { key: "map_name", value: "Map" },
        showModStarRatings ? { key: "mods", value: "Mods" } : null,
        { key: "old_sr", value: isSmallScreen ? "Old" : "Old SR", sortAllowed: true },
        { key: "new_sr", value: isSmallScreen ? "New" : "New SR", sortAllowed: true },
        { key: "sr_diff", value: isSmallScreen ? "Diff" : "Difference", sortAllowed: true }
    ].filter(isDefined);

    const handleSearch = (newSearchValue?: string, _?: any, __?: any, ___?: any, ____?: any, newShowModStarRatings?: boolean) => {
        if (newSearchValue !== undefined) setSearchValue(newSearchValue);
        if (newShowModStarRatings !== undefined) setShowModStarRatings(newShowModStarRatings);
    };

    const handleRowClick = (row: any) => {
        window.open(`https://osu.ppy.sh/b/${row.map_id}`);
    };

    const handleSortChange = (newSortingField: string) => {
        if (newSortingField === sortingField)
            setSortOrder(o => o === "asc" ? "desc" : "asc");
        setSortingField(newSortingField);
    };

    return (
        <StyledTaikoSRList>
            <ToolBar shortTitle={"Taiko SR Comparison"} longTitle={`Taiko SR Comparison`} />
            <Paper sx={sx.header}>
                This page enables you to compare the current and previous (before September 2020) star rating algorithms for the Taiko gamemode.<br />
                Requested by <Link url="https://osu.ppy.sh/u/1893718" text="mangomizer" />
            </Paper>
            <BaseTableFilters update={handleSearch} searchDelay={500} searchLabel="Search beatmap (title)" noRoundValuesOption noHideOutdatedPlayersOption noHideUnrankedPlayersOption />
            <BaseTable
                headers={headers}
                rows={parsedRows}
                handleClick={handleRowClick}
                sortBy={sortingField}
                sortingHandler={handleSortChange}
                sortOrder={sortOrder}
                defaultSort="asc"
                defaultSortField="sr_diff"
                loading={loading}
            />
        </StyledTaikoSRList>
    );
};

export default TaikoSRList;