import { Algorithm } from "common/interfaces/interfaces";

export interface IGetOReworksAction {
    type: "GET_REWORKS",
    payload: Algorithm[]
};