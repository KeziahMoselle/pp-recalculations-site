import { Algorithm } from "common/interfaces/interfaces";
import { OAuth } from "common/interfaces/OAuth";
import { apiGetRequest } from "helpers";
import { IAppThunkAction } from "store";
import { IGetOReworksAction } from "./actions";

export type KnownAction = IGetOReworksAction;

export const ReworksActions = {
    getReworks: (user: OAuth): IAppThunkAction<KnownAction> => async (dispatch, getState) => {
        try {
            const { response } = await apiGetRequest<Algorithm[]>("reworks/list", undefined, user.token);
            dispatch({ type: "GET_REWORKS", payload: response });
        } catch (error) {
            console.error(error);
            dispatch({ type: "GET_REWORKS", payload: [] });
        }
    }
};