import { Algorithm } from "common/interfaces/interfaces";
import { Action, Reducer } from "redux";
import { KnownAction } from ".";

const defaultReworks: Algorithm[] = [];

export const ReworksReducer: Reducer<Algorithm[]> = (state: Algorithm[] = defaultReworks, incomingAction: Action) => {
    const action = incomingAction as KnownAction;

    switch (action.type) {
        case "GET_REWORKS":
            return [...action.payload];
        default:
            return state;
    };
};