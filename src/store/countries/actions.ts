import { Country } from "common/interfaces/interfaces";

export interface IGetCountriesAction {
    type: "GET_COUNTRIES",
    payload: Country[]
};