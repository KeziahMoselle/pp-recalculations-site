import { Country } from "common/interfaces/interfaces";
import { apiGetRequest } from "helpers";
import { IAppThunkAction } from "store";
import { IGetCountriesAction } from "./actions";

export type KnownAction = IGetCountriesAction;

export const CountriesActions = {
    getCountries: (): IAppThunkAction<KnownAction> => async (dispatch, getState) => {
        try {
            const { response } = await apiGetRequest<Country[]>("countries");
            dispatch({ type: "GET_COUNTRIES", payload: response });
        } catch (error) {
            console.error(error);
            dispatch({ type: "GET_COUNTRIES", payload: [] });
        }
    }
};