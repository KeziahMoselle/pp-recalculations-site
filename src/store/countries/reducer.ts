import { Country } from "common/interfaces/interfaces";
import { Action, Reducer } from "redux";
import { KnownAction } from ".";

const defaultCountries: Country[] = [];

export const CountriesReducer: Reducer<Country[]> = (state: Country[] = defaultCountries, incomingAction: Action) => {
    const action = incomingAction as KnownAction;

    switch (action.type) {
        case "GET_COUNTRIES":
            return [...action.payload];
        default:
            return state;
    };
};