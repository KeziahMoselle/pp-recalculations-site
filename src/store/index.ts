import { Algorithm, Country, Settings } from "common/interfaces/interfaces";
import { OAuth } from "common/interfaces/OAuth";
import { CountriesReducer } from "./countries/reducer";
import { OAuthReducer } from "./oauth/reducer";
import { ReworksReducer } from "./reworks/reducer";
import { SettingsReducer } from "./settings/reducer";

export const reducers = {
    countries: CountriesReducer,
    oauth: OAuthReducer,
    reworks: ReworksReducer,
    settings: SettingsReducer
};

export interface IAppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => IApplicationState): void;
};

export interface IApplicationState {
    countries: Country[],
    oauth: OAuth,
    reworks: Algorithm[],
    settings: Settings
};