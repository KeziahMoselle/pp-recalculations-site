import { OsuApiV2User } from "common/interfaces/User";
import { Action, Reducer } from "redux";
import { KnownAction } from ".";
import { OAuth } from "../../common/interfaces/OAuth";

const defaultOAuth = {} as OAuth;

export const notLoggedInOAuth: OAuth = {
    user: {
        id: -1,
        username: "",
    } as OsuApiV2User,
    is_hyper_admin: false,
    rework_permissions: [],
    token: ""
};

export const OAuthReducer: Reducer<OAuth> = (state: OAuth = defaultOAuth, incomingAction: Action) => {
    const action = incomingAction as KnownAction;

    switch (action.type) {
        case "GET_OAUTH":
            return { ...action.payload };
        case "SET_OAUTH":
            return { ...action.payload };
        case "DELETE_OAUTH":
            return { ...action.payload };
        default:
            return state;
    };
};