import { OAuth } from "../../common/interfaces/OAuth";

export interface IGetOAuthAction {
    type: "GET_OAUTH",
    payload: OAuth
};

export interface ISetOAuthAction {
    type: "SET_OAUTH",
    payload: OAuth
};

export interface IDeleteOAuthAction {
    type: "DELETE_OAUTH",
    payload: OAuth
};