import { IAppThunkAction } from "..";
import { IDeleteOAuthAction, IGetOAuthAction, ISetOAuthAction } from "./actions";
import localforage from "localforage";
import { OAuth } from "../../common/interfaces/OAuth";
import { apiGetRequest, apiPatchRequest } from "../../helpers";
import { notLoggedInOAuth } from "./reducer";
import { OsuApiV2User } from "common/interfaces/User";

export type KnownAction = IGetOAuthAction | ISetOAuthAction | IDeleteOAuthAction;

//eslint-disable-next-line
const testOAuth: OAuth = {
    user: {
        id: 2330619,
        username: "Mr HeliX",
    } as OsuApiV2User,
    is_hyper_admin: false,
    rework_permissions: [],
    token: ""
};

export const OAuthActions = {
    getOAuth: (): IAppThunkAction<KnownAction> => async (dispatch, getState) => {
        try {
            const oauth = await localforage.getItem<OAuth>("oauth");
            if (oauth?.user?.id) {
                const { response: user } = await apiGetRequest<OAuth>("oauth/auth", undefined, oauth.token);
                dispatch({ type: "GET_OAUTH", payload: user });
            }
            else
                OAuthActions.setOAuth(null)(dispatch, getState);
        } catch (error) {
            console.error(error);
        }
    },
    setOAuth: (oauth: OAuth | null): IAppThunkAction<KnownAction> => async (dispatch) => {
        try {
            await localforage.setItem("oauth", oauth);
            dispatch({
                type: "SET_OAUTH",
                payload: oauth ?? notLoggedInOAuth
                // payload: testOAuth
            });
        } catch (error) {
            console.error(error);
        }
    },
    deleteOAuth: (oauth: OAuth): IAppThunkAction<KnownAction> => async (dispatch, getState) => {
        OAuthActions.setOAuth(null)(dispatch, getState);
        await apiPatchRequest("oauth/logout", undefined, oauth.token);
    }
};