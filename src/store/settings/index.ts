import { Settings } from "common/interfaces/interfaces";
import localforage from "localforage";
import { IAppThunkAction } from "store";
import { IGetSettingsAction, IUpdateSettingsAction } from "./actions";
import { defaultSettings } from "./reducer";

export type KnownAction = IGetSettingsAction | IUpdateSettingsAction;

export const SettingsActions = {
    getSettings: (): IAppThunkAction<KnownAction> => async (dispatch) => {
        const existingSettings = await localforage.getItem<Settings>("pp_settings");
        dispatch({ type: "GET_SETTINGS", payload: existingSettings ?? defaultSettings });
    },
    updateSettings: (newSettings: Settings): IAppThunkAction<KnownAction> => async (dispatch) => {
        await localforage.setItem("pp_settings", newSettings);
        dispatch({ type: "UPDATE_SETTINGS", payload: newSettings });
    }
};