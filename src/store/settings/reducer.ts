import { Settings } from "common/interfaces/interfaces";
import { Action, Reducer } from "redux";
import { KnownAction } from ".";

export const defaultSettings: Settings = {
    dateTimeFormat: "ymd",
    useGradientForPPDifference: true,
    highlightNewTop100Scores: true
};

export const SettingsReducer: Reducer<Settings> = (state: Settings = defaultSettings, incomingAction: Action) => {
    const action = incomingAction as KnownAction;

    switch (action.type) {
        case "GET_SETTINGS":
            return { ...action.payload };
        case "UPDATE_SETTINGS":
            return { ...action.payload };
        default:
            return state;
    };
};