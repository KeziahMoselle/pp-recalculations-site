import { Settings } from "common/interfaces/interfaces";

export interface IGetSettingsAction {
    type: "GET_SETTINGS",
    payload: Settings
};

export interface IUpdateSettingsAction {
    type: "UPDATE_SETTINGS",
    payload: Settings
};