import config from "../config"
import { useState, useEffect } from "react";

const cache: any = {};

export const useFetch = <T>(endpoint: string, skip?: boolean): { response: T | undefined, loading: boolean } => {
    const [response, setResponse] = useState<T>();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const fetchData = async() => {
            setIsLoading(true);
            const res = await fetch(`${config.api}/${endpoint}`, {
                method: 'GET',
                headers: new Headers({
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                })
            });
            const json = await res.json();
            setResponse(json);
            cache[endpoint] = json;
            setIsLoading(false);
        }

        if (!skip)
            fetchData();
    }, [endpoint, skip])

    return { response, loading: isLoading };
}