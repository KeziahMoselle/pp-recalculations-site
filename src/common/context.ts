import { createContext } from "react";
import { Algorithm } from "./interfaces/interfaces";

export const AlgorithmContext = createContext<{ algorithm?: Algorithm, setAlgorithm: (a: Algorithm) => void }>({ setAlgorithm: () => {} });