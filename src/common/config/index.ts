import devConfig from './dev';
import prodConfig from './prod';

export interface IConfig {
    env: string,
    api: string,
    osu_redirect_uri: string,
    osu_client_id: number
}

let config: IConfig;
const hostName = window.location.hostname;

if (hostName === 'localhost')
    config = devConfig;
else
    config = prodConfig;

export default config;