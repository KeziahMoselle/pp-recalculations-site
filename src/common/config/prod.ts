import { IConfig } from ".";

export default {
    env: "prod",
    api: "https://pp-api.huismetbenen.nl",
    osu_redirect_uri: "https://huismetbenen.nl/oauth",
    osu_client_id: 11450
} as IConfig;