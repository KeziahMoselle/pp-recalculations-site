import { IConfig } from ".";

export default {
    env: "dev",
    api: "http://localhost:3333",
    osu_redirect_uri: "http://localhost:3000/oauth",
    osu_client_id: 5591
} as IConfig;