import React, { Dispatch, SetStateAction, SyntheticEvent } from "react";
import { Snackbar } from "@mui/material";
import MuiAlert from "@mui/lab/Alert";

interface IProps {
    open: boolean,
    onClose: Dispatch<SetStateAction<boolean>>,
    text?: string
};

const ForbiddenSnackbar: React.FC<IProps> = (props) => {
    const { open, onClose, text } = props;

    const handleClose = (_: SyntheticEvent, reason?: string) => {
        if (reason === "clickaway") return;
        onClose(false);
    };

    return (
        <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
            <MuiAlert elevation={6} variant="filled" onClose={handleClose} severity="error">
                {text ?? "Forbidden"}
            </MuiAlert>
        </Snackbar>
    );
};

export default ForbiddenSnackbar;