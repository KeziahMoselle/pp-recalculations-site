import React from "react";
import { SxStyles } from "helpers";
import { Typography } from "@mui/material";

interface IProps { };

const styles: SxStyles = {
    error: {
        color: "white"
    }
};

const ForbiddenError: React.FC<IProps> = (props) => {
    return <Typography variant="body1" sx={styles.error}>You're not allowed to view this data</Typography>;
};

export default ForbiddenError;