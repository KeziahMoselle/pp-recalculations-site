import React, { useEffect, useState } from "react";
import PlayerDetails from "../../../components/PlayerDetails/PlayerDetails";
import Rankings from "../../../components/Rankings/Rankings";
import { AlgorithmContext } from "../../context";
import ReactGA from "react-ga";
import TaikoSRList from "../../../components/TaikoSRList/TaikoSRList";
import { Algorithm } from "../../interfaces/interfaces";
import { defaultAlgorithm } from "../../../helpers";
import { Redirect, Route, Switch, useHistory, useLocation } from "react-router";
import { styled } from "@mui/material/styles";
import { Chart, registerables } from "chart.js";
import { useDispatch, useSelector } from "react-redux";
import { OAuthActions } from "store/oauth";
import OAuth from "../OAuth/OAuth";
import { IApplicationState } from "store";
import { ReworksActions } from "store/reworks";
import { CountriesActions } from "store/countries";
import { PayPalScriptProvider } from "@paypal/react-paypal-js";
import Confetti from "../Confetti/Confetti";
import SettingsComponent from "components/Settings/Settings";
import { SettingsActions } from "store/settings";

interface IProps { }

const StyledBaseContainer = styled("div")(({ theme }) => ({
    backgroundColor: theme.palette.background.default,
    minHeight: "100vh"
}));

const BaseContainer: React.FC<IProps> = (props) => {
    const { oauth, reworks } = useSelector((state: IApplicationState) => state);
    const [algorithm, setAlgorithm] = useState<Algorithm>();
    const location = useLocation();
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        if (!algorithm && reworks?.length) {
            const a = location.pathname === "/oauth"
                ? location.search?.split("state=")?.[1]?.split("%2F")?.pop()
                : location.pathname.split("/").pop();

            if (!a) return;
            const rework = reworks?.find(r => r.code === a);
            if (rework?.code) setAlgorithm(rework);
            else setAlgorithm(defaultAlgorithm);
        }
    }, [location, algorithm, history, reworks]);

    useEffect(() => {
        ReactGA.pageview(location.pathname);
    }, [location]);

    useEffect(() => {
        Chart.register(...registerables);
    });

    useEffect(() => {
        dispatch(OAuthActions.getOAuth());
        dispatch(CountriesActions.getCountries());
        dispatch(SettingsActions.getSettings());
    }, [dispatch]);

    useEffect(() => {
        if (oauth?.user?.id)
            dispatch(ReworksActions.getReworks(oauth));
    }, [dispatch, oauth]);

    return (
        <AlgorithmContext.Provider value={{ algorithm, setAlgorithm }}>
            <Confetti />
            <StyledBaseContainer>
                <PayPalScriptProvider options={{ "client-id": "sb", currency: "USD" }}>
                    <Switch>
                        <Route path="/rankings" component={Rankings} />
                        <Route path="/player" component={PlayerDetails} />
                        <Route path="/taiko-sr" component={TaikoSRList} />
                        <Route path="/oauth" component={OAuth} />
                        <Route path="/settings" component={SettingsComponent} />
                        <Route path="/"><Redirect to="/rankings"></Redirect></Route>
                        <Route><Redirect to="/" /></Route>
                    </Switch>
                </PayPalScriptProvider>
            </StyledBaseContainer>
        </AlgorithmContext.Provider>
    );
};

export default BaseContainer;