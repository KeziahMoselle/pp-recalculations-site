import React, { MouseEvent, useState } from "react";
import { AppBar, Button, MenuItem, Toolbar, Typography } from "@mui/material";
import NoMobile from "../MobileHelpers/NoMobile";
import MobileOnly from "../MobileHelpers/MobileOnly";
import Sidebar from "../Sidebar/Sidebar";
import { BasicStyles, SxStyles } from "../../../helpers";
import { useSelector, useDispatch } from "react-redux";
import { IApplicationState } from "store";
import MenuBelowButton from "../MenuBelowButton/MenuBelowButton";
import { OAuthActions } from "store/oauth";
import config from "../../config";
import { useLocation } from "react-use";
import { useHistory } from "react-router-dom";

interface IProps {
    shortTitle: string;
    longTitle: string;
};

const sx: SxStyles = {
    root: {
        height: "100%",
        bgcolor: "primary.main"
    },
    toolbar: {
        display: "flex",
        justifyContent: "space-between",
        height: "100%"
    },
    button: {
        color: "white"
    }
};

const styles: BasicStyles = {
    toolbarStart: {
        display: "flex",
        alignItems: "center"
    }
};

const ToolBar: React.FC<IProps> = (props) => {
    const { shortTitle, longTitle } = props;
    const { oauth } = useSelector((state: IApplicationState) => state);
    const dispatch = useDispatch();
    const location = useLocation();
    const history = useHistory();
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

    const handleUserMenuToggle = (event: MouseEvent<HTMLButtonElement> | null) => {
        setAnchorEl(event?.currentTarget ?? null);
    };

    const authorizeOsu = () => {
        window.location.href = `https://osu.ppy.sh/oauth/authorize?client_id=${config.osu_client_id}&redirect_uri=${config.osu_redirect_uri}&response_type=code&scope=public&state=pp;${location.pathname}`;
    };

    const navigateToSettings = () => {
        if (!location.pathname?.includes("/settings"))
            history.push("/settings");
    };

    const logout = () => {
        dispatch(OAuthActions.deleteOAuth(oauth));
    };

    return (
        <AppBar sx={sx.root} position="sticky">
            <Toolbar sx={sx.toolbar}>
                <div style={styles.toolbarStart}>
                    <Sidebar />
                    <Typography variant="h6">
                        <NoMobile>{longTitle}</NoMobile>
                        <MobileOnly>{shortTitle}</MobileOnly>
                    </Typography>
                </div>
                <div>
                    <Button variant="text" onClick={navigateToSettings}>
                        <Typography variant="button" sx={sx.button}>Settings</Typography>
                    </Button>
                    {!oauth?.user?.id && <Button variant="text" disabled>
                        <Typography variant="button" sx={sx.button}>Loading...</Typography>
                    </Button>}
                    {oauth?.user?.id < 0 && <Button variant="text" onClick={authorizeOsu}>
                        <Typography variant="button" sx={sx.button}>Login</Typography>
                    </Button>}
                    {oauth?.user?.id > 0 && (
                        <>
                            <Button onClick={handleUserMenuToggle}>
                                <Typography variant="button" sx={sx.button}>{oauth.user.username}</Typography>
                            </Button>
                            <MenuBelowButton anchorEl={anchorEl} onClose={handleUserMenuToggle}>
                                <MenuItem onClick={logout}>Logout</MenuItem>
                            </MenuBelowButton>
                        </>
                    )}
                </div>
            </Toolbar>
        </AppBar>
    );
};

export default ToolBar;