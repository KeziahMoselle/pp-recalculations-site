import React, { useState, MouseEvent } from 'react';
import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody, TableFooter, TablePagination, TableSortLabel } from '@mui/material';
import { SxProps } from "@mui/system";
import MobileOnly from '../MobileHelpers/MobileOnly';
import NoMobile from '../MobileHelpers/NoMobile';
import _ from 'lodash';
import LoadingIcon from '../LoadingIcon/LoadingIcon';
import { SxStyles } from '../../../helpers';
import ContextMenu from '../ContextMenu/ContextMenu';
import BetterTooltip from "../BetterTooltip/BetterTooltip";

interface IProps {
    headers: { key: string, value: string, isBoldColumn?: boolean, defaultSort?: "asc" | "desc", maxWidth?: string, sortAllowed?: boolean, tooltip?: string }[];
    higherHeaders?: { key: string, value: string }[];
    rows: { key: string | number, values: any, isGreenBackground?: boolean, isOrangeBackground?: boolean, isOutdated?: boolean }[];
    defaultSort: 'asc' | 'desc';
    defaultSortField: string;
    containerStyle?: SxProps;
    tableStyle?: any;
    handleClick?: (row: any, mouseButton?: number) => void;
    sortingHandler?: (newField: any) => void;
    sortBy?: string;
    sortOrder?: "asc" | "desc";
    loading?: boolean;
    contextMenuItems?: { label: string, onClick: (row: any) => void }[]
}

const styles: SxStyles = {
    headerCell: {
        fontWeight: "bold"
    },
    boldCell: {
        fontWeight: "bold"
    },
    greenRow: {
        backgroundColor: 'rgb(57, 109, 38)',
        ":hover": {
            backgroundColor: 'rgb(88, 162, 60) !important'
        }
    },
    redRow: {
        backgroundColor: 'rgb(107, 64, 64)',
        ":hover": {
            backgroundColor: 'rgb(117, 70, 70) !important'
        }
    },
    orangeRow: {
        backgroundColor: "rgb(158, 94, 35)",
        ":hover": {
            backgroundColor: "rgb(194, 133, 76) !important",
        }
    }
};

const BaseTable: React.FC<IProps> = (props) => {
    const { containerStyle, tableStyle } = props;
    return (
        <TableContainer sx={containerStyle} component={Paper}>
            <MobileOnly>
                <Table size='small' stickyHeader className={tableStyle}>
                    <BaseTableContent {...props} />
                </Table>
            </MobileOnly>
            <NoMobile>
                <Table size='small' stickyHeader>
                    <BaseTableContent {...props} />
                </Table>
            </NoMobile>
        </TableContainer>
    )
}

const BaseTableContent: React.FC<IProps> = (props) => {
    const { headers, higherHeaders, rows, handleClick, defaultSort, defaultSortField, sortingHandler, sortBy, sortOrder, loading, contextMenuItems } = props;
    const [currentPage, setCurrentPage] = useState(0);
    const [rowsPerPage] = useState(50);
    const [order, setOrder] = useState<'asc' | 'desc'>(defaultSort);
    const [orderBy, setOrderBy] = useState(defaultSortField);
    const [contextMenuOpen, setContextMenuOpen] = useState<any>();

    let mouseDownEvent: MouseEvent;

    const createSortHandler = (property: string) => (event: MouseEvent<unknown>) => {
        if (sortingHandler) sortingHandler(property);
        else {
            const defaultSort = headers.find(h => h.key === property)?.defaultSort || "desc";
            const newOrder = orderBy !== property ? defaultSort : (order === "asc" ? "desc" : "asc");
            setOrder(newOrder);
            setOrderBy(property);
        }
    };

    const handlePageChange = (_: any, newPage: number) => {
        setCurrentPage(newPage);
    };

    const handleRowClick = (e: MouseEvent, row: any) => {
        if (!!handleClick)
            handleClick(row, e.button);
    };

    const openContextMenu = (event: MouseEvent, row: any) => {
        event.preventDefault();
        setContextMenuOpen({
            mouseX: event.clientX - 2,
            mouseY: event.clientY - 4,
            row
        });
    };

    const getSortOrder = () => {
        return sortOrder || order;
    };

    const getSortingField = () => {
        return sortBy || orderBy;
    };

    const sortRows = (rows: any[]): any => {
        if (sortingHandler) return rows;

        rows.sort((a: any, b: any) => {
            let valueA = a.values[orderBy];
            let valueB = b.values[orderBy];
            if (!isNaN(valueA.value)) valueA = valueA.value;
            if (!isNaN(valueB.value)) valueB = valueB.value;

            const o = order === "desc" ? -1 : 1;
            if (valueA === valueB && !!a.values.rank)
                return b.values.rank < a.values.rank ? 1 : -1;
            return valueB < valueA ? o : o * -1;
        });

        return rows;
    };

    const getCellStyling = (width: string | undefined, index: number, hasTooltip?: boolean): any => {
        const style: any = {
            width,
            paddingLeft: 5,
            paddingRight: 5,
            textDecoration: "underline",
            textDecorationStyle: "dotted",
            textUnderlineOffset: "4px"
        };
        if (index === 0) delete style.paddingLeft;
        if (index === headers.length - 1) delete style.paddingRight;

        if (!hasTooltip) {
            delete style.textDecoration;
            delete style.textDecorationStyle;
            delete style.textUnderlineOffset;
        }
        return style;
    };

    const getRowStyling = (row: any) => {
        if (row.isGreenBackground) return styles.greenRow;
        if (row.isOrangeBackground) return styles.orangeRow;
        if (row.isOutdated) return styles.redRow;
        return {};
    };

    const onMouseDown = (event: MouseEvent) => {
        mouseDownEvent = event;
    };

    const onMouseUp = (event: MouseEvent, clickFunction: () => any) => {
        if (Math.abs(event.clientX - mouseDownEvent?.clientX) <= 10)
            clickFunction();
    };

    return (
        <>
            <TableHead>
                {higherHeaders && (
                    <TableRow>
                        {higherHeaders.map(header => (
                            <TableCell sx={styles.headerCell} key={header.key}>
                                {header.value}
                            </TableCell>
                        ))}
                    </TableRow>
                )}
                <TableRow>
                    {headers.map((header, i) =>
                        <TableCell sx={styles.headerCell} key={header.key} style={getCellStyling(header.maxWidth, i, !!header.tooltip)}>
                            {header.sortAllowed
                                ? (
                                    <BetterTooltip title={[header.tooltip ?? ""]}>
                                        <TableSortLabel style={!!header.tooltip ? { textDecoration: "underline dotted", textUnderlineOffset: "4px" } : {}} active={getSortingField() === header.key} direction={getSortingField() === header.key ? getSortOrder() : 'desc'} onClick={createSortHandler(header.key)}>
                                            {header.value}
                                        </TableSortLabel>
                                    </BetterTooltip>
                                )
                                : (
                                    <BetterTooltip title={[header.tooltip ?? ""]}>
                                        {header.value}
                                    </BetterTooltip>
                                )}
                        </TableCell>
                    )}
                </TableRow>
            </TableHead>
            <TableBody>
                {loading ? <LoadingIcon /> : (
                    (rowsPerPage > 0
                        ? sortRows(rows).slice(currentPage * rowsPerPage, currentPage * rowsPerPage + rowsPerPage)
                        : sortRows(rows)
                    )
                        .map((row: any) => {
                            return <TableRow sx={getRowStyling(row)} key={row.key} hover={true} onMouseUp={e => onMouseUp(e, () => handleRowClick(e, row.values))} onMouseDown={onMouseDown} onContextMenu={e => openContextMenu(e, row.values)}>
                                {headers.map((header, i) => {
                                    let cellValue = _.get(row.values, header.key);
                                    if (!isNaN(cellValue)) cellValue = cellValue.toLocaleString();
                                    return <TableCell sx={header.isBoldColumn ? styles.boldCell : undefined} key={`${row.key}-${i}`} style={getCellStyling(header.maxWidth, i)}>{cellValue?.element ?? cellValue}</TableCell>
                                })}
                            </TableRow>
                        })
                )}
            </TableBody>
            <TableFooter>
                <TableRow>
                    <TablePagination
                        rowsPerPageOptions={[50]}
                        colSpan={headers.length}
                        count={rows.length}
                        rowsPerPage={rowsPerPage}
                        page={currentPage}
                        onPageChange={handlePageChange}
                    />
                </TableRow>
            </TableFooter>
            {contextMenuItems && <ContextMenu items={contextMenuItems} open={contextMenuOpen} onClose={() => setContextMenuOpen(false)} />}
        </>
    );
};

export default BaseTable;