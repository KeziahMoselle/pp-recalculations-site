import React, { MouseEvent, useContext, useEffect, useState } from 'react';
import { AppBar, Tabs, Tab } from '@mui/material';
import { AlgorithmContext } from '../../context';
import { useHistory, useLocation } from 'react-router';
import { SxStyles } from '../../../helpers';
import ContextMenu from '../ContextMenu/ContextMenu';

interface IProps {
    parentRoute: string;
    tabs: { id: string, label: string }[];
    defaultValue?: string;
}

const sx: SxStyles = {
    root: {
        bgcolor: "background.paper",
        color: "text.primary"
    }
};

function LinkTab(props: any) {
    const { path, linkToTab, ...others } = props;
    return (
        <Tab
            component="a"
            onClick={(event: any) => {
                event.preventDefault();
                linkToTab(path);
            }}
            {...others}
        />
    );
};

const LinkTabs: React.FC<IProps> = (props) => {
    const { parentRoute, tabs, defaultValue } = props;
    const [tabValue, setTabValue] = useState<number>(0);
    const history = useHistory();
    const location = useLocation();
    const { algorithm } = useContext(AlgorithmContext);
    const [contextMenuOpen, setContextMenuOpen] = useState<any>();

    useEffect(() => {
        const tab = location.pathname.split("/")[2];
        const activeTab = tabs[tabValue];
        if (tab !== activeTab?.label) {
            const newTab = tabs.findIndex(t => t.id === tab);
            if (newTab > -1) setTabValue(newTab);
        }
    }, [algorithm, location, tabValue, tabs]);

    const tabIndex = tabs.findIndex(tab => tab.id === defaultValue);
    if (tabIndex !== tabValue && tabIndex > -1)
        setTabValue(tabIndex);

    const handleChange = (event: any, newValue: number) => {
        setTabValue(newValue);
    };

    const linkToTab = (tab: string, newTab?: boolean) => {
        const link = `${parentRoute}/${tab}/${algorithm?.code}`;
        if (newTab)
            window.open(link);
        else
            history.push(link);
    };

    const openContextMenu = (event: MouseEvent, tab: string) => {
        event.preventDefault();
        setContextMenuOpen({
            mouseX: event.clientX - 2,
            mouseY: event.clientY - 4,
            tab
        });
    };

    return (
        <>
            <AppBar position="static">
                <Tabs sx={sx} variant="scrollable" value={tabValue} onChange={handleChange} aria-label="link tabs" indicatorColor='primary' scrollButtons='auto'>
                    {tabs.map(tab => (
                        <LinkTab key={tab.id} label={tab.label} path={tab.id} linkToTab={linkToTab} onContextMenu={(e: MouseEvent) => openContextMenu(e, tab.id)} />
                    ))}
                </Tabs>
            </AppBar>

            <ContextMenu items={[{ label: "Open in new tab", onClick: () => linkToTab(contextMenuOpen.tab, true) }]} open={contextMenuOpen} onClose={() => setContextMenuOpen(false)} />
        </>
    );
};

export default LinkTabs;