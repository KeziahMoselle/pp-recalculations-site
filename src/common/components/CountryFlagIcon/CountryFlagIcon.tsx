import React from 'react';
import { styled } from "@mui/material/styles";

interface IProps {
    country: string;
};

const StyledImage = styled("img")(({ theme }) => ({
    width: "23pt",
    height: "18pt",
    marginRight: theme.spacing(1)
}));

const CountryFlagIcon: React.FC<IProps> = (props) => {
    const { country } = props;
    let svg;
    try {
        svg = require(`./flags/${country.toLowerCase()}.svg`);
    }
    catch (error) {
        svg = "";
    }

    return (
        <StyledImage src={svg.default} alt={country === "Unknown" ? "?" : country} />
    );
};

export default CountryFlagIcon;