import React, { MouseEvent, useContext, useState } from "react";
import { IconButton, Drawer, List, ListItem, ListItemText, Divider, Typography, ListItemSecondaryAction } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useHistory, useLocation } from "react-router";
import { AlgorithmContext } from "../../context";
import { Algorithm, ReworkTypeCode } from "../../interfaces/interfaces";
import { IApplicationState } from "store";
import { useSelector } from "react-redux";
import { getGamemodeFromNumber, getReworkTypeNameByCode, SxStyles } from "helpers";
import ContextMenu from "../ContextMenu/ContextMenu";

interface IProps { };

const styles: SxStyles = {
    divider: {
        width: "calc(100% - 16)",
        ml: 2
    }
};

const Sidebar: React.FC<IProps> = (props) => {
    const [sidebarOpen, setSidebarOpen] = useState<boolean>(false);
    const history = useHistory();
    const location = useLocation();
    const { reworks } = useSelector((state: IApplicationState) => state);
    const [contextMenuOpen, setContextMenuOpen] = useState<any>();

    const { algorithm, setAlgorithm } = useContext(AlgorithmContext);

    const toggleSidebar = () => {
        setSidebarOpen(!sidebarOpen);
    };

    const navigateToRankingsPage = (a: string) => {
        if (algorithm?.code !== a || location.pathname.includes("/player/") || location.pathname.includes("/settings")) {
            const algorithmDetails = reworks?.find(r => r.code === a);
            history.push(`/rankings/players/${a}`);
            if (algorithmDetails)
                setAlgorithm(algorithmDetails);
            else
                setAlgorithm({ code: a, name: "Gaming" } as Algorithm);
        }
        setSidebarOpen(false);
    };

    const navigateToPage = (path: string, newTab?: boolean) => {
        if (newTab) window.open(path);
        else {
            history.push(path);
            setAlgorithm({ code: "", name: "Gaming" } as Algorithm);
        }
    };

    const openContextMenu = (event: MouseEvent, path: string) => {
        event.preventDefault();
        setContextMenuOpen({
            mouseX: event.clientX - 2,
            mouseY: event.clientY - 4,
            path
        });
    };

    const getReworksByType = (types: ReworkTypeCode | ReworkTypeCode[]): Algorithm[] => {
        return (reworks ?? [])?.filter(r => Array.isArray(types) ? types.includes(r.rework_type_code) : r.rework_type_code === types);
    };

    const { LIVE, MASTER, REWORK_PUBLIC_ACTIVE, REWORK_PUBLIC_INACTIVE, REWORK_PRIVATE_ACTIVE, REWORK_PRIVATE_INACTIVE, HISTORIC } = ReworkTypeCode;

    return (
        <>
            <IconButton edge="start" color="inherit" aria-label="menu" onClick={toggleSidebar}>
                <MenuIcon />
            </IconButton>
            <Drawer open={sidebarOpen} onClose={toggleSidebar}>
                <List>
                    {[LIVE, MASTER, REWORK_PUBLIC_ACTIVE, REWORK_PRIVATE_ACTIVE, [REWORK_PUBLIC_INACTIVE, REWORK_PRIVATE_INACTIVE], HISTORIC].map((reworkTypes, i) => {
                        let filteredReworks = getReworksByType(reworkTypes);
                        if (!filteredReworks.length) return null;

                        filteredReworks.sort((a, b) => a.gamemode - b.gamemode);

                        return (
                            <React.Fragment key={i}>
                                <ListItem sx={{ height: 15, mt: 2 }}>
                                    <Typography variant="caption" sx={{ color: "gray" }}>{getReworkTypeNameByCode(Array.isArray(reworkTypes) ? reworkTypes[0] : reworkTypes)}</Typography>
                                </ListItem>
                                <Divider sx={styles.divider} />
                                {filteredReworks.map(r => (
                                    <ListItem key={r.id} button onClick={() => navigateToRankingsPage(r.code)} onContextMenu={(e: MouseEvent) => openContextMenu(e, `/rankings/players/${r.code}`)}>
                                        <ListItemText primary={r.name} />
                                        <ListItemSecondaryAction sx={{ color: "gray", fontSize: "small" }}>{getGamemodeFromNumber(r.gamemode)}</ListItemSecondaryAction>
                                    </ListItem>
                                ))}
                            </React.Fragment>
                        )
                    })}

                    <ListItem sx={{ height: 15, mt: 2 }}>
                        <Typography variant="caption" sx={{ color: "gray" }}>Other data</Typography>
                    </ListItem>
                    <Divider sx={styles.divider} />

                    <ListItem button onClick={() => navigateToPage("/taiko-sr")} onContextMenu={(e: MouseEvent) => openContextMenu(e, "/taiko-sr")}>
                        <ListItemText primary="Taiko SR Comparison" />
                    </ListItem>
                </List>
            </Drawer>

            <ContextMenu items={[{ label: "Open in new tab", onClick: () => navigateToPage(contextMenuOpen.path, true) }]} open={contextMenuOpen} onClose={() => setContextMenuOpen(false)} />
        </>
    );
};

export default Sidebar;