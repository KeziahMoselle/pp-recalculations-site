import React from 'react';
import { styled } from "@mui/material/styles";
import CountryFlagIcon from '../CountryFlagIcon/CountryFlagIcon';

interface IProps {
    name: string;
    country: string;
};

const StyledDonatorName = styled("div")(({ theme }) => ({
    display: "flex",
    alignItems: "center"
}));

const DonatorName: React.FC<IProps> = (props) => {
    const { name, country } = props;

    return (
        <StyledDonatorName>
            <CountryFlagIcon country={country} />
            <span>{name}</span>
        </StyledDonatorName>
    );
};

export default DonatorName;