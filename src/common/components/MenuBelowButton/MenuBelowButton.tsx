import React, { MouseEvent } from "react";
import { Menu } from "@mui/material";

interface IProps {
    anchorEl: HTMLElement | null,
    onClose: (event: MouseEvent<HTMLButtonElement> | null) => void
};

const MenuBelowButton: React.FC<IProps> = (props) => {
    const { children, anchorEl, onClose } = props;

    return (
        <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            anchorOrigin={{
                vertical: "bottom",
                horizontal: "center"
            }}
            transformOrigin={{
                vertical: "top",
                horizontal: "center"
            }}
            onClose={() => onClose(null)}
        >
            {children}
        </Menu>
    );
};

export default MenuBelowButton;