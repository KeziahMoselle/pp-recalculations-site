import React, { useState, } from 'react';
import { FormControl, InputLabel, Select, MenuItem, Checkbox, FormControlLabel, Grid, IconButton } from '@mui/material';
import { styled } from "@mui/material/styles";
import Searchbar from '../Searchbar/Searchbar';
import { SxStyles } from '../../../helpers';
import { Clear } from "@mui/icons-material"

interface IProps {
    update: (searchValue: string, countryFilter: string, roundValues: boolean, onlyUpdatedPlayers: boolean, hideUnrankedPlayers: boolean, showModStarRatings?: boolean) => void;
    countries?: { code: string, name: string }[];
    searchDelay?: number;
    searchLabel?: string;
    initialCountry?: string;
    noRoundValuesOption?: boolean;
    noHideOutdatedPlayersOption?: boolean;
    noHideUnrankedPlayersOption?: boolean;
    noShowModStarRatingsOption?: boolean;
};

const StyledBaseTableFilters = styled("div")(({ theme }) => ({
    display: "flex",
    [theme.breakpoints.up("md")]: {
        width: "98%",
        textAlign: "left",
        marginLeft: theme.spacing(2),
    },
    [theme.breakpoints.down("sm")]: {
        width: "90%",
        paddingLeft: "5%"
    },
    alignItems: "center"
}));

const styles: SxStyles = {
    dialogAppBar: {
        display: "flex",
        justifyContent: "space-between",
        position: "relative"
    },
    selectField: {
        width: 300,
        mr: 1
    }
};

const BaseTableFilters: React.FC<IProps> = (props) => {
    const { update, countries, searchDelay, searchLabel, initialCountry, noRoundValuesOption, noHideOutdatedPlayersOption, noHideUnrankedPlayersOption, noShowModStarRatingsOption } = props;
    const [searchValue, setSearchValue] = useState('');
    const [countryFilter, setCountryFilter] = useState<string>(initialCountry ?? "");
    const [roundValues, setRoundValues] = useState<boolean>(true);
    const [onlyUpdatedPlayers, setOnlyUpdatedPlayers] = useState<boolean>(false);
    const [hideUnrankedPlayers, setHideUnrankedPlayers] = useState<boolean>(true);
    const [showModStarRatings, setShowModStarRatings] = useState<boolean>(false);

    const countriesForSelect = [
        { code: "", name: "No filter" },
        ...(countries ?? []).sort((a, b) => a.name < b.name ? -1 : 1)
    ];

    interface HandleChangeParams {
        newSearchValue?: string,
        newCountryFilter?: string,
        newRoundValues?: boolean,
        newOnlyUpdatedPlayers?: boolean,
        newHideUnrankedPlayers?: boolean,
        newShowModStarRatings?: boolean
    };

    const handleChange = ({ newSearchValue, newCountryFilter, newRoundValues, newOnlyUpdatedPlayers, newHideUnrankedPlayers, newShowModStarRatings }: HandleChangeParams) => {
        update(
            newSearchValue ?? searchValue,
            newCountryFilter ?? countryFilter,
            newRoundValues ?? roundValues,
            newOnlyUpdatedPlayers ?? onlyUpdatedPlayers,
            newHideUnrankedPlayers ?? hideUnrankedPlayers,
            newShowModStarRatings ?? showModStarRatings
        );
    }

    const handleSearch = (value: string) => {
        setSearchValue(value);
        handleChange({ newSearchValue: value ?? "" });
    };

    const handleCountryFilterChange = (event: any) => {
        const newFilter = event.target.value as string;
        setCountryFilter(newFilter);
        handleChange({ newCountryFilter: newFilter });
    };

    const handleRoundValuesChange = (event: any) => {
        setRoundValues(event.target.checked);
        handleChange({ newRoundValues: event.target.checked });
    };

    const handleOnlyUpdatedPlayers = (event: any) => {
        setOnlyUpdatedPlayers(event.target.checked);
        handleChange({ newOnlyUpdatedPlayers: event.target.checked });
    };

    const handleHideUnrankedPlayers = (event: any) => {
        setHideUnrankedPlayers(event.target.checked);
        handleChange({ newHideUnrankedPlayers: event.target.checked });
    };

    const handleShowModStarRatings = (event: any) => {
        setShowModStarRatings(event.target.checked);
        handleChange({ newShowModStarRatings: event.target.checked });
    };

    return (
        <StyledBaseTableFilters>
            <Grid container sx={{ alignItems: "center" }}>
                <Grid container justifyContent="flex-start" item xs={12} sm={6} md={4} lg={3} xl={2}>
                    <Searchbar label={searchLabel ?? "Search player"} value={searchValue} update={handleSearch} delay={searchDelay} />
                </Grid>
                {countries?.length && <Grid container justifyContent="flex-start" item xs={12} sm={6} md={3} xl={2}>
                    <FormControl sx={styles.selectField}>
                        <InputLabel variant="standard">Filter on country</InputLabel>
                        <Select
                            variant="standard" value={countryFilter} onChange={handleCountryFilterChange}
                            sx={{ textAlign: "left", "& .MuiSelect-icon": { display: countryFilter?.length ? "none" : "" } }}
                            endAdornment={countryFilter?.length > 0 && <IconButton sx={{ width: 25, height: 25 }} onClick={() => handleCountryFilterChange({ target: { value: "" } })}><Clear fontSize="small" /></IconButton>}
                        >
                            {countriesForSelect.map(country => (
                                <MenuItem key={country.code} value={country.code}>{country.name}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Grid>}
                <Grid container justifyContent="flex-start" item xs={12} md={4}>
                    {!noRoundValuesOption && (
                        <FormControlLabel
                            control={<Checkbox checked={roundValues} onChange={handleRoundValuesChange} color="primary" />}
                            label="Round values"
                            sx={{ color: "white" }}
                        />
                    )}
                    {!noHideOutdatedPlayersOption && (
                        <FormControlLabel
                            control={<Checkbox checked={onlyUpdatedPlayers} onChange={handleOnlyUpdatedPlayers} color="primary" />}
                            label="Hide outdated players"
                            sx={{ color: "white" }}
                        />
                    )}
                    {!noHideUnrankedPlayersOption && (
                        <FormControlLabel
                            control={<Checkbox checked={hideUnrankedPlayers} onChange={handleHideUnrankedPlayers} color="primary" />}
                            label="Hide unranked players"
                            sx={{ color: "white" }}
                        />
                    )}
                    {!noShowModStarRatingsOption && (
                        <FormControlLabel
                            control={<Checkbox checked={showModStarRatings} onChange={handleShowModStarRatings} color="primary" />}
                            label="Show mod star ratings"
                            sx={{ color: "white" }}
                        />
                    )}
                </Grid>
            </Grid>
        </StyledBaseTableFilters>
    );
};

export default BaseTableFilters;