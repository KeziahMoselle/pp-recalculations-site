import React, { ChangeEvent, SyntheticEvent, useContext, useEffect, useState } from "react";
import { TextField, Button, useTheme, useMediaQuery, Snackbar, Typography, Grid, Hidden } from "@mui/material";
import { styled } from "@mui/material/styles";
import MuiAlert from "@mui/lab/Alert";
import { AlgorithmContext } from "../../context";
import { apiPatchRequest, SxStyles } from "../../../helpers";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";
import BetterTooltip from "../BetterTooltip/BetterTooltip";
import { QueueResponse } from "common/interfaces/interfaces";

interface IProps {
    onQueueUpdate: (newQueue: QueueResponse) => void,
    activeRequestCount: number
};

const StyledRequestRecalculation = styled("div")(({ theme }) => ({
    [theme.breakpoints.up('md')]: {
        width: "100%",
    },
    [theme.breakpoints.down('sm')]: {
        width: "90%",
    }
}));

const StyledRequestRecalculationInfo = styled("div")(({ theme }) => ({
    [theme.breakpoints.up("md")]: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "flex-start",
        marginLeft: theme.spacing(2)
    },
    [theme.breakpoints.down("sm")]: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "flex-start",
        marginLeft: theme.spacing(2)
    }
}));

const sx: SxStyles = {
    button: {
        ml: 1,
        mt: 1,
        height: "75%"
    },
    disabledQueueText: {
        color: "#da4949",
        mt: 1,
        ml: 1
    }
};

const RequestRecalculation: React.FC<IProps> = (props) => {
    const { onQueueUpdate, activeRequestCount } = props;
    const { oauth } = useSelector((state: IApplicationState) => state);
    const [userId, setUserId] = useState<number>();
    const [snackBarOpen, setSnackBarOpen] = useState<boolean>(false);
    const [snackBarMessage, setSnackBarMessage] = useState<string>();
    const [errorSnackBarOpen, setErrorSnackBarOpen] = useState<boolean>(false);
    const [errorText, setErrorText] = useState<string>("");
    const theme = useTheme();
    const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));
    const { algorithm } = useContext(AlgorithmContext);

    const [queueDisabled, setQueueDisabled] = useState<boolean>(false);

    const requestsCapped = activeRequestCount >= 10;

    useEffect(() => {
        setQueueDisabled(!algorithm?.queue_enabled);
    }, [algorithm]);

    const userIdChanged = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if (!isNaN(+event.target.value))
            setUserId(+event.target.value);
    };

    const addToQueue = async () => {
        if (queueDisabled) return;
        if (!userId || isNaN(userId)) setErrorSnackBarOpen(true);
        else {
            const { response, error } = await apiPatchRequest<QueueResponse>(`queue/add-to-queue`, { user_id: userId, rework: algorithm?.id }, oauth?.token);
            if (error) {
                setErrorText(error);
                setErrorSnackBarOpen(true);
            }
            else {
                setSnackBarMessage(`User ${userId} was added to the queue!`);
                setSnackBarOpen(true);
                onQueueUpdate(response);
            }
        }
    };

    const addUserToQueue = async () => {
        if (oauth?.user?.id < 0 || queueDisabled) return;
        const { response, error } = await apiPatchRequest<QueueResponse>(`queue/add-to-queue`, { user_id: oauth.user?.id, rework: algorithm?.id }, oauth.token);
        if (error) {
            setErrorText(error);
            setErrorSnackBarOpen(true);
        }
        else {
            setSnackBarMessage(`${oauth.user?.username} was added to the queue!`);
            setSnackBarOpen(true);
            onQueueUpdate(response);
        }
    };

    const handleClose = (_: SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') return;
        setErrorSnackBarOpen(false);
        setSnackBarOpen(false);
    };

    return (
        <StyledRequestRecalculation>
            <StyledRequestRecalculationInfo>
                <Typography variant="body1" style={{ color: "white" }} align="left">You can request a recalculation here. Your top 100 scores will always be recalculated; if you were a top 10k player on the first day of the month, this will be extended to your top 500 scores.</Typography>
                <Typography variant="body1" style={{ color: "white", marginTop: isSmallScreen ? 16 : 0 }} align="left">Add yourself to the queue by clicking "Recalculate me", or enter a user id and click "add to queue" to add that player to the queue</Typography>
                <Grid container spacing={1}>
                    <Hidden smUp>
                        <Grid item xs={12} sx={{ mb: 1, mt: 2, display: "flex" }}>
                            <BetterTooltip title={oauth?.user?.id < 0 ? ["You must be logged in to use this feature"] : []}>
                                <Button sx={{ ...sx.button, ml: 0 }} variant="contained" color="primary" disabled={oauth?.user?.id < 0 || queueDisabled || requestsCapped} onClick={addUserToQueue}>Recalculate me</Button>
                            </BetterTooltip>
                        </Grid>
                    </Hidden>

                    <Grid item xs={12} md={6} sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                        <TextField variant="standard" label="User id" value={userId} onChange={userIdChanged} disabled={queueDisabled} />
                        <Button sx={sx.button} variant="contained" color="primary" onClick={addToQueue} disabled={queueDisabled || requestsCapped}>{isSmallScreen ? "Recalculate" : "Add to queue"}</Button>
                        {!isSmallScreen && (
                            <BetterTooltip title={oauth?.user?.id < 0 ? ["You must be logged in to use this feature"] : []}>
                                <Button sx={sx.button} variant="contained" color="primary" disabled={oauth?.user?.id < 0 || queueDisabled || requestsCapped} onClick={addUserToQueue}>Recalculate me</Button>
                            </BetterTooltip>
                        )}
                    </Grid>

                    <Grid item xs={12} sx={{ display: "flex" }}>
                        {queueDisabled && <Typography variant="body2" sx={sx.disabledQueueText}>This queue is currently disabled</Typography>}
                        {!queueDisabled && requestsCapped && (
                            <Typography variant="body2" sx={sx.disabledQueueText}>Please wait for your active requests to be processed</Typography>
                        )}
                    </Grid>
                </Grid>
            </StyledRequestRecalculationInfo>
            {/* <PayPalButtons
                fundingSource={FUNDING.PAYPAL}
                createOrder={(data, actions) => {
                    return actions.order.create({
                        purchase_units: [
                            {
                                amount: {
                                    value: "3.00"
                                }
                            }
                        ]
                    })
                }}
                onApprove={(data, actions) => {
                    if (!actions.order) return Promise.resolve();
                    return actions.order.capture().then((details) => {
                        const name = details.payer.name.given_name;
                        console.log(`Transaction completed by ${name}`);
                    });
                }}
            /> */}
            <Snackbar open={snackBarOpen} autoHideDuration={2000} onClose={handleClose}>
                <MuiAlert elevation={6} variant="filled" onClose={handleClose} severity="success">
                    {snackBarMessage}
                </MuiAlert>
            </Snackbar>
            <Snackbar open={errorSnackBarOpen} autoHideDuration={2000} onClose={handleClose}>
                <MuiAlert elevation={6} variant="filled" onClose={handleClose} severity="error">
                    {errorText}
                </MuiAlert>
            </Snackbar>
        </StyledRequestRecalculation>
    );
};

export default RequestRecalculation;