import React from "react";
import { styled } from "@mui/material/styles";

interface IProps {
    url: string,
    text: string,
    color?: string
};

const StyledLink = styled("a")(({ theme }) => ({
    color: theme.palette.text.primary
}));

const Link: React.FC<IProps> = (props) => {
    const { url, text, color } = props;

    const style = color ? { color } : {};

    return (
        <StyledLink href={url} style={style}>{text}</StyledLink>
    );
};

export default Link;