import React from 'react';
import { CircularProgress } from '@mui/material';
import { styled } from "@mui/material/styles";

interface IProps { }

const StyledLoadingIcon = styled("div")(({ theme }) => ({
    paddingTop: theme.spacing(2),
    width: "100%",
    display: "flex",
    justifyContent: "center",
}));

const LoadingIcon: React.FC<IProps> = (props) => {
    return (
        <StyledLoadingIcon>
            <CircularProgress />
        </StyledLoadingIcon>
    );
};

export default LoadingIcon;