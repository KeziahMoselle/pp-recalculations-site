import React from "react";
import { Tooltip } from "@mui/material";

interface IProps {
    title: string[];
};

const TooltipText: React.FC<IProps> = (props) => {
    const { title } = props;

    return (
        <div>
            {title.map((line, index) => <div key={index}>{line}</div>)}
        </div>
    );
};

const BetterTooltip: React.FC<IProps> = (props) => {
    const { children, title } = props;
    if (!title.length || !title[0].length) return <div>{children}</div>;

    return (
        <Tooltip title={<span style={{ fontSize: 13 }}><TooltipText {...props} /></span>} enterTouchDelay={0} leaveTouchDelay={2000}>
            <div>{children}</div>
        </Tooltip>
    );
};

export default BetterTooltip;