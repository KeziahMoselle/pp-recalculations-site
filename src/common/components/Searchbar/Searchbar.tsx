import React, { useState, ChangeEvent, useEffect } from 'react';
import { IconButton, TextField } from '@mui/material';
import { SxStyles } from '../../../helpers';
import { Clear } from '@mui/icons-material';

interface IProps {
    label: string;
    value: string;
    styling?: any;
    delay?: number;
    update: (searchValue: string, ignoreTimeout?: boolean) => void;
};

const styles: SxStyles = {
    root: {
        width: {
            xs: "80%",
            md: "400px"
        },
        maxHeight: "48px",
        mr: 1
    }
};

const Searchbar: React.FC<IProps> = (props) => {
    const { styling, label, value, delay, update } = props;
    const [searchValue, setSearchValue] = useState(value);
    const [searchTimeout, setSearchTimeout] = useState<NodeJS.Timeout>();

    useEffect(() => setSearchValue(value), [value]);

    const searchValueChanged = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const newValue = event.target.value;
        setSearchValue(newValue);
        if (searchTimeout) clearTimeout(searchTimeout);
        setSearchTimeout(setTimeout(() => {
            update(event.target.value.toLowerCase());
            setSearchTimeout(undefined);
        }, delay ?? 0));
    };

    const clearValue = () => {
        setSearchValue("");
        if (searchTimeout) clearTimeout(searchTimeout);
        update("");
    };

    return (
        <TextField
            sx={{ ...styles.root, ...(styling ?? {}) }}
            variant="standard" label={label} value={searchValue}
            onChange={searchValueChanged}
            InputProps={{ endAdornment: searchValue?.length > 0 && <IconButton sx={{ width: 25, height: 25 }} onClick={clearValue}><Clear fontSize="small" /></IconButton> }} />
    );
};

export default Searchbar;