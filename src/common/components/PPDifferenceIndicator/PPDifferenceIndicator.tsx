import React from "react";
import { roundValue } from "helpers";
import * as Rainbow from "rainbowvis.js";
import { useSelector } from "react-redux";
import { IApplicationState } from "store";

interface IProps {
    value: number
};

const PPDifferenceIndicator: React.FC<IProps> = (props) => {
    const { value } = props;
    const roundedValue = roundValue(value, 1);
    const { settings } = useSelector((state: IApplicationState) => state);

    const colorGain = "#23fa1b";
    const colorLoss = "#d62929";
    const colorNoChange = "#f0f08d"

    const rainbowGain = new Rainbow();
    const rainbowLoss = new Rainbow();

    rainbowGain.setSpectrum("#d1d1d1", colorGain);
    rainbowLoss.setSpectrum("#d1d1d1", colorLoss);

    const getColor = () => {
        if (roundedValue < 0) return settings.useGradientForPPDifference ? `#${rainbowLoss.colourAt(Math.min(value * -1, 100))}` : colorLoss;
        if (roundedValue > 0) return settings.useGradientForPPDifference ? `#${rainbowGain.colourAt(Math.min(value, 100))}` : colorGain;
        return colorNoChange;
    };

    return <span style={{ color: getColor() }}>{roundedValue === 0 ? "-" : roundedValue.toLocaleString()}</span>;
};

export default PPDifferenceIndicator;





