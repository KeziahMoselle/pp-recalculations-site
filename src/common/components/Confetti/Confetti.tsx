import moment from "moment";
import needle from "needle";
import React, { useCallback, useEffect, useRef, useState } from "react";
import ReactCanvasConfetti from "react-canvas-confetti";

interface IProps { };

const canvasStyles = {
    position: "fixed",
    pointerEvents: "none",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0
};

function getAnimationSettings(color: string) {
    return {
        particleCount: 1,
        startVelocity: 0,
        ticks: 200,
        spread: 360,
        origin: {
            x: Math.random(),
            y: Math.random() * 0.999 - 0.2
        },
        colors: [color]
    };
}

const Confetti: React.FC<IProps> = (props) => {
    const now = moment();
    const [birthdays, setBirthdays] = useState<{ day: number, month: number, color: string }[]>();

    const currentBirthday = !!birthdays ? birthdays?.find(b => now.date() === b.day && now.month() === b.month - 1) : null;

    const refAnimationInstance = useRef<any>(null);
    const nextTickAnimation = useCallback(() => {
        if (refAnimationInstance.current && currentBirthday?.color) {
            refAnimationInstance.current(getAnimationSettings(currentBirthday?.color ?? ""));
            refAnimationInstance.current(getAnimationSettings(currentBirthday?.color ?? ""));
        }
    }, [currentBirthday]);

    const [intervalId] = useState<any>(setInterval(nextTickAnimation, 400));

    const getInstance = useCallback((instance) => {
        refAnimationInstance.current = instance;
    }, []);

    useEffect(() => {
        return () => {
            clearInterval(intervalId);
        };
    }, [intervalId]);

    useEffect(() => {
        const fetchBirthdays = async () => {
            needle.get(`https://api.huismetbenen.nl/dates`, { json: true }, (_, response) => {
                if (response?.body && Array.isArray(response.body))
                    setBirthdays(response.body);
                else setBirthdays([]);
            });
        };

        if (!birthdays) fetchBirthdays();
    }, [birthdays]);

    if (!currentBirthday || !birthdays) return null;

    return (
        <>
            <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles as any} />
        </>
    );
};

export default Confetti;