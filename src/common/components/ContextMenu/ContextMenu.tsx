import React from "react";
import { Menu, MenuItem } from "@mui/material";

interface IProps {
    items: { label: string, onClick: (row?: any) => void }[],
    open: { mouseX: number, mouseY: number, row?: any } | null,
    onClose: () => void
};

const ContextMenu: React.FC<IProps> = (props) => {
    const { items, open, onClose } = props;

    const handleItemClick = (item: { label: string, onClick: (row?: any) => void }) => {
        item.onClick(open?.row);
        onClose();
    };

    return (
        <Menu
            open={!!open}
            onClose={onClose}
            anchorReference="anchorPosition"
            anchorPosition={!!open ? { top: open.mouseY, left: open.mouseX } : undefined}
        >
            {items.map(item => (
                <MenuItem key={item.label} onClick={() => handleItemClick(item)}>
                    {item.label}
                </MenuItem>
            ))}
        </Menu>
    );
};

export default ContextMenu;