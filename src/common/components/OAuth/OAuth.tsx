import "./OAuth.css";
import React, { useEffect, useRef, useState } from "react";
import { SxStyles } from "helpers";
import { useHistory, useLocation } from "react-router";
import { useDispatch } from "react-redux";
import config from "../../config";
import { OAuthActions } from "store/oauth";
import { styled } from "@mui/material/styles";
import { Paper, Typography } from "@mui/material";
import huismetbenen from "../../images/huismetbenen.png";

interface IProps { };

const styles: SxStyles = {
    paper: {
        width: {
            xs: "90%",
            md: "50%"
        },
        p: 2
    }
};

const StyledContainer = styled("div")(({ theme }) => ({
    display: "flex",
    justifyContent: "center"
}));

const OAuth: React.FC<IProps> = (props) => {
    const location = useLocation();
    const history = useHistory();
    const dispatch = useDispatch();

    const imagesRef = useRef<any>(null);
    const [loadingIconCount, setLoadingIconCount] = useState<number>(0);
    const [seed, setSeed] = useState<number>();

    useEffect(() => {
        if (imagesRef.current) {
            setLoadingIconCount(Math.round(imagesRef.current.offsetWidth / 40 + 1));
        }
    }, [imagesRef]);

    useEffect(() => {
        if (!seed)
            setSeed(Math.ceil(Math.random() * 10));
    }, [seed]);

    useEffect(() => {
        const params = location.search?.split("?")[1]?.split("&").reduce((total: any, current) => {
            const [key, value] = current.split("=");
            total[key] = value;
            return total;
        }, {});

        const { code, state } = params;

        const handleOAuthResponse = async () => {
            if (!code?.length) {
                history.push("/");
                return;
            }

            const response = await fetch(`${config.api}/oauth`, {
                method: "POST",
                headers: new Headers({
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }),
                body: JSON.stringify({ code })
            });

            const oauth_response = await response.json();
            dispatch(OAuthActions.setOAuth(oauth_response));
            const path = state.split("%3B")[1];
            const parsedPath = path?.replaceAll("%2F", "/");
            history.replace(parsedPath?.length ? parsedPath : "/rankings");
        };

        handleOAuthResponse();
    }, [history, location, dispatch]);

    const headers: string[] = [
        "Verifying data...",
        "Stealing your pp...",
        "Deploying the StanR rework...",
        "Buffing all scores on Britney Spears - Toxic...",
        "Making fun of angry people on Twitter...",
        "Sending angry e-mail to peppy about pp changes...",
        "Reverting game back to score rankings...",
        "Attempting to keep servers from burning down...",
        "Nerfing everyone who still says WYSI...",
        "Wasting more time on these loading messages...",
        "Baiting Reddit with the Microflow 2 rework..."
    ];

    return (
        <StyledContainer>
            <Paper sx={styles.paper}>
                <Typography variant="body1">{seed && (headers[seed] ?? headers[0])}</Typography>
                <div className="axis" style={{ width: "100%" }}>
                    <div className="blocker" />
                    <div className="imageContainer">
                        <div className="images" ref={imagesRef} >
                            {Array.from(Array(loadingIconCount)).map((_, i) => (
                                <img key={i} className="img" src={huismetbenen} alt="" style={{ left: (i - 1) * 40 }} />
                            ))}
                        </div>
                    </div>
                    <div className="blocker" />
                </div>
            </Paper>
        </StyledContainer>
    );
};

export default OAuth;