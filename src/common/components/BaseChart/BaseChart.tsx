import { Chart, ChartType, ChartOptions } from "chart.js";
import _ from "lodash";
import React, { useRef, useState, useEffect } from "react";

interface IProps {
    type: ChartType,
    data: any;
    options?: ChartOptions;
    onClick?: (event: any, chart: Chart) => void;
};

const BaseChart: React.FC<IProps> = (props) => {
    const { type, data, options, onClick } = props;
    const chartRef = useRef<HTMLCanvasElement>(null);
    const [chart, setChart] = useState<Chart>();

    // Create initial chart
    useEffect(() => {
        if (chartRef?.current) {
            const newChart = new Chart(chartRef.current, { type, data: { datasets: [] }, options: {} });
            setChart(newChart);
        }
    }, [chartRef, type]);

    // Data changed
    useEffect(() => {
        if (chart && !_.isEqual(data, chart.data)) {
            chart.data.labels = data.labels;
            chart.data.datasets = data.datasets;
            if (options)
                chart.options = options;
            if (onClick)
                chart.options.onClick = (event) => onClick(event, chart);
            chart.update();
        }
    }, [chart, data, options, onClick]);

    return <canvas ref={chartRef} />;
};

export default BaseChart;