import React from 'react';
import { Hidden } from '@mui/material';

interface IProps {};

const MobileOnly: React.FC<IProps> = (props) => {
    const { children } = props;
    return (
        <Hidden only={['md', 'lg', 'xl']}>
            {children}
        </Hidden>
    );
};

export default MobileOnly;