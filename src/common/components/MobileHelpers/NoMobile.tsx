import React from 'react';
import { Hidden } from '@mui/material';

interface IProps {};

const NoMobile: React.FC<IProps> = (props) => {
    const { children } = props;
    return (
        <Hidden only={['xs', 'sm']}>
            {children}
        </Hidden>
    );
};

export default NoMobile;