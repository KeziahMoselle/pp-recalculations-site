export interface OsuScore {
    score_id: number,
    beatmap_id: number,
    user_id: number,
    score: number,
    maxcombo: number,
    rank: string,
    count50: number,
    count100: number,
    count300: number,
    countmiss: number,
    countgeki: number,
    countkatu: number,
    perfect: number,
    enabled_mods: number,
    date: string,
    pp: number,
    replay: number,
    hidden: number,
    country_acronym: string
};

export interface CalculationResult {
    map_name: string,
    accuracy: number,
    combo: number,
    great: number,
    good: number,
    meh: number,
    miss: number,
    mods: string,
    aim_pp: number,
    tap_pp: number,
    accuracy_pp: number,
    od: number,
    ar: number,
    max_combo: number,
    pp: number
};

export interface OsuBeatmap {
    beatmapset_id: string,
    beatmap_id: string,
    approved: string,
    total_length: string,
    hit_length: string,
    version: string,
    file_md5: string,
    diff_size: string,
    diff_overall: string,
    diff_approach: string,
    diff_drain: string,
    mode: string,
    count_normal: string,
    count_slider: string,
    count_spinner: string,
    submit_date: string,
    approved_date: string,
    last_update: string,
    artist: string,
    title: string,
    creator: string,
    creator_id: string,
    bpm: string,
    source: string,
    tags: string,
    genre_id: string,
    language_id: string,
    favourite_count: string,
    rating: string,
    download_unavailable: string,
    audio_unavailable: string,
    playcount: string,
    passcount: string,
    max_combo: string,
    diff_aim: string,
    diff_speed: string,
    difficultyrating: string
};

export type RecalculatedPlayerData = DatabaseScore[];

export interface DatabaseScore {
    _id: string;
    score_id: number,
    beatmap_id: number,
    live_pp: number,
    master_pp: number,
    title: string,
    artist: string,
    diff_name: string,
    creator_name: string,
    local_pp: number,
    aim_pp?: number,
    tap_pp?: number,
    acc_pp?: number,
    fl_pp?: number,
    visual_pp?: number,
    cognition_pp?: number,
    reading_pp?: number,
    mods: string,
    accuracy: number,
    max_combo: number,
    perfect?: number,
    great: number,
    good: number,
    ok?: number,
    meh: number,
    miss: number,
    score_date: string,
    score_rank: string,
    old_rank: number,
    new_rank: number,
};

export interface Algorithm {
    id: number,
    code: string,
    name: string,
    rework_type_code: ReworkTypeCode,
    algorithm_version: number,
    url: string,
    branch: string,
    commit: string,
    gamemode: number,
    is_private: boolean,
    queue_enabled: boolean
    accurate_rank_limit: number,
    has_extra_fl_calc: boolean,
    banner_text: string | null,
    compare_with_master: boolean,
    pp_skills: string,
    parsed_pp_skills: string[]
};

export enum ReworkTypeCode {
    LIVE = "LIVE",
    MASTER = "MASTER",
    REWORK_PUBLIC_ACTIVE = "REWORK_PUBLIC_ACTIVE",
    REWORK_PUBLIC_INACTIVE = "REWORK_PUBLIC_INACTIVE",
    REWORK_PRIVATE_ACTIVE = "REWORK_PRIVATE_ACTIVE",
    REWORK_PRIVATE_INACTIVE = "REWORK_PRIVATE_INACTIVE",
    HISTORIC = "HISTORIC"
};

export interface RankingsDataTopPlayers {
    new: number[],
    old: number[],
    diff: number[],
    avgChange: number
};

export interface RankingsDataTopScores {
    new: number[],
    old: number[],
    diff: number[],
    avgChange: number
};

export interface Player {
    user_id: number,
    name: string,
    country: string,
    new_pp_excl_bonus: number,
    new_pp_incl_bonus: number,
    old_pp: number,
    weighted_aim_pp: number,
    weighted_tap_pp: number,
    weighted_acc_pp: number,
    weighted_fl_pp: number,
    bonus_pp: number,
    pp_change: number,
    global_rank: number,
    country_rank: number,
    pp_version: number,
    rework: number
};

export interface Country {
    code: string,
    name: string
};

export type DateFormat = "ymd" | "dmy" | "mdy" | null;

export interface Settings {
    dateTimeFormat: DateFormat,
    useGradientForPPDifference: boolean,
    highlightNewTop100Scores: boolean
};

export interface QueueResponse {
    queue: QueuedPlayer[],
    oldestTime: number | null
};

export interface QueuedPlayer {
    user_id: number,
    rework_id: number,
    date_added: number,
    formatted_date: string,
    name: string | null,
    last_updated: string,
    requested_by_you: boolean
};