export interface OsuApiV1User {
    user_id: string,
    username: string,
    join_date: string,
    count300: string,
    count100: string,
    count50: string,
    playcount: string,
    ranked_score: string,
    total_score: string,
    pp_rank: string,
    level: string,
    pp_raw: string,
    accuracy: string,
    count_rank_ss: string,
    count_rank_ssh: string,
    count_rank_s: string,
    count_rank_sh: string,
    count_rank_a: string,
    country: string,
    total_seconds_played: string,
    pp_country_rank: string,
    events: any[]
};

export interface OsuApiV2User {
    avatar_url: string;
    country_code: string;
    default_group: string;
    id: number;
    is_active: boolean;
    is_bot: boolean;
    is_online: boolean;
    is_supporter: boolean;
    last_visit: string;
    pm_friends_only: boolean;
    profile_colour: string;
    username: string;
    cover_url: string;
    discord: string;
    has_supported: boolean;
    interests: any;
    join_date: string;
    kudosu: {
        total: number;
        available: number;
    };
    location: any;
    max_blocks: number;
    max_friends: number;
    occupation: any;
    playmode: string;
    playstyle: string[];
    post_count: number;
    profile_order: string[];
    skype: any;
    title: any;
    twitter: string;
    website: string;
    country: {
        code: string;
        name: string;
    };
    cover: {
        custom_url: string;
        url: string;
        id: any;
    };
    account_history: any[];
    active_tournament_banner: any[];
    badges: {
        awarded_at: string;
        description: string;
        image_url: string;
        url: string;
    }[];
    favourite_beatmapset_count: number;
    follower_count: number;
    graveyard_beatmapset_count: number;
    groups: {
        id: number;
        identifier: string;
        name: string;
        short_name: string;
        description: string;
        colour: string;
    }[];
    loved_beatmapset_count: number;
    monthly_playcounts: {
        start_date: string;
        count: number;
    }[];
    page: {
        html: string;
        raw: string;
    };
    previous_usernames: string[];
    ranked_and_approved_beatmapset_count: number;
    replays_watched_count: {
        start_date: string;
        count: number;
    }[];
    scores_first_count: number;
    statistics: {
        level: {
            current: number;
            progress: number;
        };
        global_rank: number;
        pp: number;
        ranked_score: number;
        hit_accuracy: number;
        play_count: number;
        play_time: number;
        total_score: number;
        total_hits: number;
        maximum_combo: number;
        replays_watched_by_others: number;
        is_ranked: boolean;
        grade_counts: {
            ss: number;
            ssh: number;
            s: number;
            sh: number;
            a: number;
        };
        country_rank: number;
        rank: {
            country: number;
        };
    };
    support_level: number;
    unranked_beatmapset_count: number;
    user_achievements: {
        achieved_at: string;
        achievement_id: number;
    }[];
    rank_history: {
        mode: string;
        data: number[];
    };
};

export interface OsuApiV2Score {
    id: number,
    user_id: number,
    accuracy: number,
    mods: string[],
    score: number,
    max_combo: number,
    perfect: boolean,
    rank_global: number,
    statistics: {
        count_50: number,
        count_100: number,
        count_300: number,
        count_geki: number,
        count_katu: number,
        count_miss: number
    },
    pp: number,
    rank: string,
    created_at: string,
    mode: string,
    mode_int: number,
    replay: boolean,
    beatmap: {
        id: number,
        beatmapset_id: number,
        mode: string,
        mode_int: number,
        convert: any,
        difficulty_rating: number,
        version: string,
        total_length: number,
        hit_length: number,
        cs: number,
        drain: number,
        accuracy: number,
        ar: number,
        playcount: number,
        passcount: number,
        count_circles: number,
        count_sliders: number,
        count_spinners: number,
        count_total: number,
        last_updated: string,
        ranked: number,
        status: string,
        url: string,
        deleted_at: string
    },
    beatmapset: {
        artist: string,
        artist_unicode: string,
        covers: {
            cover: string,
            "cover@2x": string,
            card: string,
            "card@2x": string,
            list: string,
            "list@2x": string,
            slimcover: string,
            "slimcover@2x": string
        },
        creator: string,
        favourite_count: number,
        hype: any,
        id: number,
        nsfw: boolean,
        play_count: number,
        preview_url: string,
        source: string,
        status: string,
        title: string,
        title_unicode: string,
        track_id: number | null,
        user_id: number,
        video: boolean
    },
    user: {
        id: number,
        username: string,
        profile_colour: any,
        avatar_url: string,
        country_code: string,
        default_group: string,
        is_active: boolean,
        is_bot: boolean,
        is_online: boolean,
        is_supporter: boolean,
        last_visit: string,
        pm_friends_only: boolean,
        country: {
            code: string,
            name: string
        }
    }
}