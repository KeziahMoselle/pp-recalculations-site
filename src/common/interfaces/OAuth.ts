import { OsuApiV2User } from "./User"

export enum ReworkPermissionEnum {
    NO_PERMISSIONS = 0,
    ADMIN = 1,
    READER = 2
};

export interface ReworkPermission {
    rework_id: number,
    rework_code: string,
    permission: number
};

export interface UserReworkPermission {
    uid: number,
    user_id: number,
    username: string | null,
    rework_id: number,
    rework_permission_id: number,
    role: string
};

export interface OAuth {
    user: OsuApiV2User,
    token: string,
    is_hyper_admin: boolean,
    rework_permissions: ReworkPermission[]
};

export interface HyperAdmin {
    user_id: string,
    username?: string
};

export interface UsersWithAccess {
    users: UserReworkPermission[],
    hyperAdmins: HyperAdmin[]
};