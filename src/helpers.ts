import { CSSProperties } from "react";
import config from "./common/config";
import { Algorithm, DateFormat, ReworkTypeCode } from "./common/interfaces/interfaces";
import { SxProps } from "@mui/system";
import { Theme } from "@mui/material/styles";
import moment, { Moment } from "moment-timezone";

export const isDefined = <T>(item: T | undefined | null): item is T => item !== null && item !== undefined;

export interface SxStyles { [key: string]: SxProps<Theme> };
export interface BasicStyles { [key: string]: CSSProperties };

export interface ApiResponse<T> {
    response: T,
    status: number,
    error?: string
};

const apiRequest = async <T>(endpoint: string, method: "GET" | "POST" | "PATCH" | "DELETE", body: object = {}, token?: string, rework?: string): Promise<{ response: T, status: number, error?: string }> => {
    const response = await fetch(`${config.api}/${endpoint}`, {
        method,
        headers: new Headers({
            "Accept": "application/json",
            "Authorization": token ? `Bearer ${token}` : "",
            "Content-Type": "application/json",
            "X-Rework": rework ?? ""
        }),
        body: method !== "GET" ? JSON.stringify(body) : undefined
    });

    const parsedResponse = await response.json();
    if (parsedResponse.error)
        return { response: parsedResponse, status: response.status, error: parsedResponse.error };
    return { response: parsedResponse, status: response.status };
};

export const apiGetRequest = async <T>(endpoint: string, body?: object, token?: string, rework?: string): Promise<ApiResponse<T>> => {
    return await apiRequest<T>(endpoint, "GET", body, token, rework);
};

export const apiPatchRequest = async <T>(endpoint: string, body?: object, token?: string, rework?: string): Promise<ApiResponse<T>> => {
    return await apiRequest<T>(endpoint, "PATCH", body, token, rework);
};

export const apiPostRequest = async <T>(endpoint: string, body?: object, token?: string, rework?: string): Promise<ApiResponse<T>> => {
    return await apiRequest<T>(endpoint, "POST", body, token, rework);
};

export const apiDeleteRequest = async <T>(endpoint: string, body?: object, token?: string): Promise<ApiResponse<T>> => {
    return await apiRequest<T>(endpoint, "DELETE", body, token);
};

export const enforceModOrder = (modsInput: string): string => {
    if (modsInput === "" || modsInput === "NM" || modsInput === "nomod" || modsInput === "None")
        return modsInput;
    const correctOrder = ["NF", "EZ", "HD", "HR", "HT", "DT", "NC", "FL", "SO", "SD", "PF", "TD", "1K", "2K", "3K", "4K", "5K", "6K", "7K", "8K", "9K", "FI", "MR", "LastMod"]; // TD = TouchDevice
    const splitMods = modsInput.replace("TouchDevice", "TD").match(/.{1,2}/g) ?? [];
    const result: string[] = [];
    splitMods.forEach(mod => {
        const index = correctOrder.findIndex(m => m === mod);
        if (index >= 0)
            result[index] = mod;
        else
            console.error(`Unknown mod: ${mod}`);
    });

    return result.join('');
};

export const defaultAlgorithm: Algorithm = {
    id: 1,
    code: "live",
    name: "Live (osu!)",
    rework_type_code: ReworkTypeCode.LIVE,
    url: "https://github.com/ppy/osu",
    branch: "master",
    commit: "",
    gamemode: 0,
    is_private: false,
    queue_enabled: true,
    algorithm_version: 1,
    accurate_rank_limit: 25000,
    banner_text: "",
    has_extra_fl_calc: false,
    compare_with_master: false,
    pp_skills: "",
    parsed_pp_skills: []
};

export const roundValue = (value: number, decimals: number = 0): number => {
    return Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
};

export const getGamemodeFromNumber = (gamemode: number): string => {
    switch (gamemode) {
        case 0: return "osu";
        case 1: return "taiko";
        case 2: return "catch";
        case 3: return "mania";
        default: return "osu";
    };
};

export const getReworkTypeNameByCode = (code: ReworkTypeCode): string => {
    switch (code) {
        case "LIVE":
            return "Live data";
        case "MASTER":
            return "All confirmed changes";
        case "REWORK_PUBLIC_ACTIVE":
            return "Pending reworks";
        case "REWORK_PUBLIC_INACTIVE":
            return "Inactive reworks";
        case "REWORK_PRIVATE_ACTIVE":
            return "Private reworks";
        case "REWORK_PRIVATE_INACTIVE":
            return "Private reworks (inactive)";
        case "HISTORIC":
            return "Historic data";
        default:
            return "Other data";
    };
};

export const convertDateFormat = (format: DateFormat): string => {
    switch (format) {
        case "ymd": return "YYYY-MM-DD";
        case "dmy": return "DD-MM-YYYY";
        case "mdy": return "MM-DD-YYYY";
        default: return "YYYY-MM-DD";
    };
};

export const parseDateToUtcDate = (dateInput: string): Moment => {
    const [date, time] = dateInput.split("T");
    const timeWithoutZone = time.split(".")[0];
    const fullDate = `${date} ${timeWithoutZone}`;
    return moment.utc(fullDate);
};

export const totalPPReworks = ["xexxar_total_pp", "logistic_total_pp"];