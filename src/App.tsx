import React from 'react';
import './App.css';
import { ThemeProvider } from '@mui/material';
import { createTheme } from "@mui/material/styles";
import BaseContainer from './common/components/BaseContainer/BaseContainer';
import ReactGA from "react-ga";
import { BrowserRouter } from 'react-router-dom';
import { composeWithDevTools } from "redux-devtools-extension";
import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { reducers } from 'store';
import { Provider } from "react-redux";

const trackingId = "UA-149685557-5";
ReactGA.initialize(trackingId);

const createStoreWithMiddleware = composeWithDevTools(applyMiddleware(thunk))(createStore);
const store = createStoreWithMiddleware(combineReducers(reducers));

function App() {
    const theme = createTheme({
        palette: {
            mode: "dark",
            primary: {
                main: "#a35994"
            },
            secondary: {
                main: "#f50057",
                light: "#ff4081",
                dark: "#c51162",
                contrastText: "#fff"
            },
            "info": {
                "light": "#64b5f6",
                "main": "#2196f3",
                "dark": "#1976d2",
                "contrastText": "#fff"
            },
            "grey": {
                "50": "#fafafa",
                "100": "#f5f5f5",
                "200": "#eeeeee",
                "300": "#e0e0e0",
                "400": "#bdbdbd",
                "500": "#9e9e9e",
                "600": "#757575",
                "700": "#616161",
                "800": "#424242",
                "900": "#212121",
                "A100": "#d5d5d5",
                "A200": "#aaaaaa",
                "A400": "#303030",
                "A700": "#616161"
            },
            "background": {
                "paper": "#424242",
                "default": "#303030"
            },
            text: {
                primary: "#ffffff"
            },
        },
        components: {
            MuiPaper: {
                styleOverrides: {
                    root: {
                        backgroundImage: "linear-gradient(rgba(255, 255, 255, 0), rgba(255, 255, 255, 0))"
                    }
                }
            },
            MuiCardHeader: {
                styleOverrides: {
                    root: {
                        textAlign: "left"
                    },
                    title: {
                        fontSize: 22
                    },
                    subheader: {
                        fontSize: 12
                    }
                }
            },
            MuiListItemText: {
                styleOverrides: {
                    primary: {
                        color: "white"
                    }
                }
            },
            MuiTypography: {
                styleOverrides: {
                    root: {
                        color: "white"
                    }
                }
            }
        }
    });

    return (
        <div className="App">
            <ThemeProvider theme={theme}>
                <Provider store={store}>
                    <BrowserRouter>
                        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
                        <BaseContainer />
                    </BrowserRouter>
                </Provider>
            </ThemeProvider>
        </div>
    );
};

export default App;
